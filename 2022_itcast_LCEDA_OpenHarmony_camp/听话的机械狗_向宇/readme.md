﻿
> # 功能描述
> 
> 
> 四足机器人：目前已经支持语音控制前进、后退、左转、右转、扭身子、握手、跳跃等；支持AP模式，用终端连接热点，访问网页进行控制；还支持使用HC05和蓝牙调试器进行控制以及超声波避障。

# 成品展示

## 视频展示
1.[【开源一只基于Hi3861的OpenHarmony四足机器人-哔哩哔哩】](https://b23.tv/ULsH4aZ)

2.自己的方案：[【立创训练营--听话的狗子-哔哩哔哩】](https://b23.tv/mH67eUn)

3.官方作业：[【立创训练营--仿生机器狗（丐版）-哔哩哔哩】](https://b23.tv/wLG3YG1)

其他的学习Demo：
1.[【Hi3861+oled播放动画-哔哩哔哩】](https://b23.tv/JxyAsvD)

2.[OpenHarmony学习笔记——Hi3861+ASR-01的语音识别助手](http://t.csdn.cn/R9JPK)

3.[【OpenHarmony南向开发日常--基于Hi3861与onenet的远程温湿度检测-哔哩哔哩】](https://b23.tv/gcHOA3Y)
# 硬件介绍
## 电源部分

由于整个项目需要驱动9个舵机，需要比较大的电流，所以供电选用两节18（直径）65（长度）0（圆柱形）锂电池，电源部分主要使用12V-5V，以及5V-3.3V的LDO（这里有个优化点，作者当时图简单，使用的都是LDO，实测发热还是比较严重，后面有打算换成DCDC进行供电）。

### 手册参考电路
UZ1084:（商城料号：C84897）
![在这里插入图片描述](https://img-blog.csdnimg.cn/8b7ec8289b16473888f77a041260cc7f.png)
### 实际使用电路
![在这里插入图片描述](https://img-blog.csdnimg.cn/fe445dc1d6084354b58fe2356b85fe6f.png)
## 主控电路

主控使用的是传智教育的HI3861模组（商城料号C2923578），该模组内置最小系统电路，简化了该部分电路设计，只需要额外增加一个复位电路以及在电源部分添加一个100nF的旁路电容C7（大电容滤除低频噪声，小电容滤除高频噪声）即可。image.png


## 下载电路

上一期鸿蒙训练营，没有添加CH340需要外部接线，给我人接麻了，疼定思痛，加上了CH340G模块，由于使用的是Typec接口，所以在CC1和CC2要下来俩个电阻，方便电脑识别如果不加有可能会识别不到。另外，Hi3861是3.3V电压基准，为了保证统一电平，这里CH340G采用3.3V供电（上一期笔者有用5V供电烧录失败的经历）所以对应的V3端口也要接3.3V，而非5V供电时的那样。
![在这里插入图片描述](https://img-blog.csdnimg.cn/c7a92edbe6e24e3392e266cbfee9f6cd.png)

## 舵机驱动模块

由于PCA9685商城没有现货，在某宝查了一下价格，买个芯片和买个模块要花差不多的米，本着提高成功率的态度，买了模块。
## 语音识别模块

语音识别使用的是鹿小班语音识别模块，官方简介：
ASR-01是一颗专用于语音处理的人工智能芯片，可广泛应用于家电、家居、照明、玩具等产品领域，实现语音交互及控制。
ASR-ONE内置自主研发的脑神经网络处理器BNPU，支持200条命令词以内的本地语音识别，内置CPU核和高性能低功耗Audio Codec模块，集成多路UART、IIC、PWM、GPIO等外围控制接口，可以开发各类高性价比单芯片智能语音产品方案。
![在这里插入图片描述](https://img-blog.csdnimg.cn/5d7596a52f244fe2a0060ec10f21d935.jpeg)

想要购买的戳这——[「LU-ASR01鹿小班智能语音识别模块 离线识别 自定义词条远超LD3320」](https://m.tb.cn/h.fCqIxAN?tk=n9vq2q4f15E)他们近期出了一款PRO版本，据说是支持声纹识别的，笔者还没买来尝鲜，有兴趣的同学自行了解吧。

## 其他注意事项

可能是由于这个模组BootLoader的影响，在上电和烧录时，如果其它引脚接入了模块就会造成乱码和无法复位的问题，所以在设计电路的时候，对于IO的使用尽量用排针排母引出使用而不是在电路中焊死，例如串口1的TX、RX如果上电之前已经接了CH340之类的就有可能出现无法启动，无法复位，无法烧录的问题。

# 软件简介

## 主体框架

程序主体还是用的传智官方的示例，在此基础上进行了亿点点修改，主要是增加了一个串口任务和动作逻辑。

```javascript

static void UART_Task(void)
{
    IotUartAttribute uart_attr = {
        //baud_rate: 9600
        .baudRate = 9600,

        //data_bits: 8bits
        .dataBits = 8,
        .stopBits = 1,
        .parity = 0,
        .rxBlock = 0,
        .txBlock = 0,
    };
      //Initialize uart driver
    IoTUartInit(HI_UART_IDX_1, &uart_attr);
    while (1)
    {
        printf("=======================================\r\n");
        printf("*************SIZU_example**************\r\n");
        printf("=======================================\r\n");

        // //通过串口1发送数据
        // IoTUartWrite(HI_UART_IDX_1, (unsigned char *)data, strlen(data));

        //通过串口1接收数据
        IoTUartRead(HI_UART_IDX_1, uart_buff_ptr, UART_BUFF_SIZE);
        SIZU_Uart_Cmd((char *)uart_buff_ptr);
        printf("Uart1 read data:%s\r\n", uart_buff_ptr);
        usleep(500000);
    }

}

static void start(void) {
    osThreadAttr_t attr;
    //设置GPIO_2引脚复用功能为PWM
    IoTIoSetFunc(IOT_IO_NAME_5, IOT_IO_FUNC_5_UART1_RXD);
    IoTIoSetFunc(IOT_IO_NAME_6, IOT_IO_FUNC_6_UART1_TXD);
    IoTIoSetFunc(IOT_IO_NAME_10, IOT_IO_FUNC_10_I2C0_SDA);
    IoTIoSetFunc(IOT_IO_NAME_9, IOT_IO_FUNC_9_I2C0_SCL);
    
    IoTI2cInit(0, 400000);
    dog_init();
    genki_services_start();
    init_service();

    attr.name = "UART_Task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = UART_TASK_STACK_SIZE;
    attr.priority = UART_TASK_PRIO;

    if (osThreadNew((osThreadFunc_t)UART_Task, NULL, &attr) == NULL)
    {
        printf("[ADCExample] Falied to create UART_Task!\n");
    }
}

APP_FEATURE_INIT(start);
```

动作逻辑主要参考的B站——[【金属舵机+3D打印四足机器人-哔哩哔哩】](https://b23.tv/8mzZyAx)

```
//  --------                 --------
// |  D9    |               |  D7    |
// | joint9 |               | joint7 |
//  ----- --------     --------- -----
//       |  D8    |   |  D6    |
//       | joint8 |   | joint6 |
//        --------     --------
//       |  D2    |  |   D4    |
//       | joint2 |  |  joint4 |
//  ----- --------    --------- -----
// |  D3    |               |  D5    |
// | joint3 |               | joint5 |
//  --------                 --------
//                Front

```

然后是语音识别，参考的笔者上期鸿蒙做的笔记——[OpenHarmony学习笔记——Hi3861+ASR-01的语音识别助手](http://t.csdn.cn/ngtkW)，由于功能比较简单，也就没有去弄数据包，一个简单的开头和一个数字，然后使用atoi函数获取数字，进行判断，最后执行操作即可。

```javascript
// An highlighted block
enum{
    Get_Down,//趴下
	Hand_shake,//握手
    Go_Forward,//前进
    Go_Backward,//后退
    Go_Left,//左转
    Go_Right,//右转
    Twist_Body,//扭身子
    Stretch_Oneself,//伸懒腰
    WAIT//d
};

//检测串口指令
void SIZU_Uart_Cmd(char *str)      
{
    char  *Str;
    unsigned char ID=255;
	
    Str=&str[1];//定位到指令的数字部分“U1”
    ID=atoi(Str);
	
	if(strstr((const char *)str,"G")!=NULL)			//如果字符串str中包含有“G”
	{
		switch(ID)
		{
			case Get_Down:	//趴下  G0
                sithome();
                printf("Get_Down\r\n");
				break;
			case Hand_shake:	// 握手G1
                wink(10);
                printf("Handshake\r\n");
				break;
			case Go_Forward:	// 前进G2
                forward(5);
                printf("Go_Forward\r\n");
				break;
			case Go_Backward:	// 后退G3
                 backward(5);
                 printf("Go_Backward\r\n");
				break;
            case Go_Left:	// 左转G4
                leftturn(5);
                 printf("Go_Left\r\n");
				break;
             case Go_Right:	// 右转G5
                 rightturn(5);
                 printf("Go_Right\r\n");
				break;
            case Twist_Body:	// 扭身子
                 twist();
                 printf("LED_Add\r\n");
				break;
            case Stretch_Oneself:	//伸懒腰
                 printf("LED_Reduce\r\n");
                 stand3();
				break;
			default:
				printf("%s ERROR",str);
                standhome();
				break;
		}
	}
    memset(uart_buff,0,sizeof(uart_buff));
}
```

## 语音识别代码

语音识别使用的是天问的鹿小班模块，内置图形化编程模块，YYDS，会拖动模块就可以了，想玩语音识别的，强烈推荐此款。
![在这里插入图片描述](https://img-blog.csdnimg.cn/e6edce51431c4e579192a0bd3244dbc8.png)


## 超声波测距流程

这里笔者使用的是GPIO模式，利用两个GPIO口进行控制和捕获，进而计算出距离，详细介绍请参考笔者的博客——[OpenHarmony南向学习笔记——Hi3861+HC-SR04超声波检测](http://t.csdn.cn/sccJD)

### 通信流程

根据厂商资料可以知道，该模块的通信流程如下：
1.主控芯片与TRIG连接的IO配置为输出模式，与ECHO连接的IO配置为输入模式；
2.MCU给TRIG引脚输出一个大于10us的高电平脉冲；
3.模块通过ECHO脚返回一个高电平脉冲信号；
4.主控记录ECHO脚高电平脉冲时间T并代入公式计算。

### 代码

```javascript
// An highlighted block

#define Echo  8   //Echo   //GPIO8
#define Trig  7   //Trig   //GPIO7
#define GPIO_FUNC 0

float GetDistance  (void) {
    static unsigned long start_time = 0, time = 0;
    float distance = 0.0;
    IotGpioValue value = IOT_GPIO_VALUE0;
    unsigned int flag = 0;
            /*
=============== GPIO通信模式流程 1初始化GPIO========================================
            */
    IoTIoSetFunc(Echo, GPIO_FUNC);//设置Echo连接IO为普通GPIO模式，无复用
    IoTGpioSetDir(Echo, IOT_GPIO_DIR_IN);//设置Echo连接IO为输入模式
    IoTGpioSetDir(Trig, IOT_GPIO_DIR_OUT);//设置Trig连接IO为输出模式
            /*
=============== GPIO通信模式流程 2输出起始信号========================================
            */
    IoTGpioSetOutputVal(Trig, IOT_GPIO_VALUE1);//拉高Trig
    IoTUdelay(20);//20us
    IoTGpioSetOutputVal(Trig, IOT_GPIO_VALUE0);//拉低Trig
           /*
=============== GPIO通信模式流程 3检测Echo脚输出的高电平时间========================================
            */
    while (1) {
        IoTGpioGetInputVal(Echo, &value);//读取Echo脚的电平状态
        if ( value == IOT_GPIO_VALUE1 && flag == 0) {//如果为高
            start_time = IoTGetUs();//获取此时时间
            flag = 1;
        }
        if (value == IOT_GPIO_VALUE0 && flag == 1) {//高电平结束变成低电平
            time = IoTGetUs() - start_time;//计算高电平维持时间
            start_time = 0;
            break;
        }
    }
               /*
=============== GPIO通信模式流程 4代入公式计算========================================
            */
    distance = time * 0.034 / 2;
    // printf("distance is %f\r\n",distance);
    return distance;
}

```

## 网页服务器

这里的网页部分也是在传智的Demo上修改的，由于笔者之前没有学过HTML和JavaScript的知识，在B站大学找了个速成教程，学了一下大概明白了原理，这里把视频链接贴给大家想要学习的自己去看吧——[【ESP32创建HTTP服务器，Arduino开发，不会C语言、HTML也能轻松上手，超简单！-哔哩哔哩】](https://b23.tv/s4qbCpc)。看完UP的视频就可以大致看明白源代码了，进而就可以进行修改了，笔者修改后的界面如下，左侧是后端代码，右侧是服务器端的代码。
![image.png](//image.lceda.cn/pullimage/80xueJxr8T8SAJ1JvNpV8Si1W3hTAekSkWwEOqcw.png)
终端打开效果如下，经过验证，所有功能正常。
![Screenshot_20220724_121915_com.huawei.browser.jpg](//image.lceda.cn/pullimage/9KODyjtg9PCnOjNo0tBLcLf7UPYxD0rByvgyX43o.jpeg)
HTML的代码：

```javascript
<!DOCTYPE html>
<html lang=\"en\">
<head><meta charset=\"UTF-8\">
<title>听话的狗子</title><style> body {user-select: none;text-align: center;}
table {margin: 0 auto}  td div {width: 150px;height: 150px;border: 1px solid darkgrey;display: flex;align-items: center;justify-content: center;color:deepskyblue;}
#s {background-color: red;color: white;}  td div:active {background: yellow;color: white;}
button {margin: 0.5rem;width: 9rem;height: 3rem;font-size: 1.2rem;border-radius: 1rem;}
button:hover {border-radius: 1rem;background-color: green;color: yellow;}
</style></head><body><h1>网页控制</h1>
<button id=\"c\">安装模式</button><button id=\"a\">第一组动作</button><button id=\"b\">第二组动作</button><br>
<br>

<button id=\"d\">前进</button><br>
<br data-tomark-pass>
<button id=\"e\">左转</button>
<button id=\"f\">右转</button><br data-tomark-pass><br data-tomark-pass>
<button id=\"g\">后退</button><br data-tomark-pass><br data-tomark-pass>
<button id=\"h\">趴下</button><button id=\"i\">握手</button><button id=\"j\">伸懒腰</button><button id=\"k\">扭身子</button>
<br data-tomark-pass><br data-tomark-pass>
<script>
function u(p) {return `${p}`;}
function g(i) {return document.getElementById(i);}
function fe(e, f){e.addEventListener('click', function (e) {/* do something*/f();});}
function fg(u, f) {let x = new XMLHttpRequest();
x.onreadystatechange = function () {if (this.readyState == 4 && this.status == 200) {f(x);}};
x.open(\"GET\", u, true);x.send();}
function fp(u, d, f, p) {let x = new XMLHttpRequest();
 if (p) {x.upload.addEventListener('progress', function (e) {p(e);});}
 x.onreadystatechange = function () {if (this.readyState == 4 && this.status == 200) {f(x);}};
 x.open('POST', u, true);x.timeout = 45000;x.send(d);}
 fe(g('a'), () => {fg(u('/dog/first'), () => {});});
 fe(g('b'), () => {fp(u('/dog/second'),  () => {});});
 fe(g('c'), () => {fp(u('/dog/init'), () => {});});
 fe(g('d'), () => {fp(u('/dog/forward'), () => {});});
 fe(g('e'), () => {fp(u('/dog/left'), () => {});});
 fe(g('f'), () => {fp(u('/dog/right'),  () => {});});
 fe(g('g'), () => {fp(u('/dog/backward'), () => {});});
 fe(g('h'), () => {fp(u('/dog/fall'),  () => {});});
 fe(g('i'), () => {fp(u('/dog/handshake'), () => {});});
 fe(g('j'), () => {fp(u('/dog/stretch'),  () => {});});
 fe(g('k'), () => {fp(u('/dog/twist'), () => {});});
 </script></body></html>

```


## 蓝牙APP

由于笔者没有学习过APP的制作，这里用的是蓝牙调试器来制作的上位机，这个调试器功能还是蛮强的，有兴趣的可以去试试。
![在这里插入图片描述](https://img-blog.csdnimg.cn/d3148e5e131343ee82f09fe8aeabced1.png)

***顺便问一下，哪位同学有好的APP制作教程啊，就做做简单上位机的那种。***

# 机械结构

笔者也是第一次自己绘制3D结构件，还不太OK啊，跟着画了个锤子，然后觉着自己画的结构实在不太靠谱，于是去海鲜市场捞了一下，找到了这个，有相同烦恼的同学可以去看看，38米——[四足机器人3D打印件（打印件不是整机，电子件、螺丝等自备）](https://m.tb.cn/h.fCqtqEX?tk=fEZs2q4TzAm)。
![在这里插入图片描述](https://img-blog.csdnimg.cn/07073204a4e54d019f633d3764673a45.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/9881116d090d443e972ba984dba9dc08.png)

# 学习笔记
这里有亿点点鸿蒙学习笔记，放在这欢迎大家交流学习，有错误之处欢迎私信教我修改，不然误人子弟了。

[OpenHarmony学习笔记——南向开发环境搭建](https://blog.csdn.net/qq_41954556/article/details/123374184?spm=1001.2014.3001.5502)

[OpenHarmony学习笔记——编辑器访问Linux服务器进行编译](https://blog.csdn.net/qq_41954556/article/details/123593264?spm=1001.2014.3001.5502)

[OpenHarmony学习笔记——点亮你的LED](https://blog.csdn.net/qq_41954556/article/details/123593028?spm=1001.2014.3001.5502)

[OpenHarmony学习笔记——多线程的创建](https://blog.csdn.net/qq_41954556/article/details/123683779?spm=1001.2014.3001.5502)

[OpenHarmony学习笔记——I2C驱动0.96OLED屏幕](https://blog.csdn.net/qq_41954556/article/details/123761411?spm=1001.2014.3001.5502)

[OpenHarmony学习笔记——Hi3861使用DHT11获取温湿度](https://blog.csdn.net/qq_41954556/article/details/123799852?spm=1001.2014.3001.5502)

[OpenHarmony学习笔记——Hi3861接入OneNET](https://blog.csdn.net/qq_41954556/article/details/123843107?spm=1001.2014.3001.5502)

[手把手教你OneNET数据可视化](https://blog.csdn.net/qq_41954556/article/details/123878277?spm=1001.2014.3001.5502)

[OpenHarmony学习笔记——Hi386+ASR-01的语音识别助手](https://blog.csdn.net/qq_41954556/article/details/123905578?spm=1001.2014.3001.5502)

[Hi3861网络通信——UDP收发](http://t.csdn.cn/ysXwJ)

[OpenHarmony南向学习笔记——Hi3861+HC-SR04超声波检测](http://t.csdn.cn/sccJD)

[\[立创&传智&黑马程序员&CSDN\]训练营——仿生机械狗\]](http://t.csdn.cn/0m96R)
# 总结
文中如有不妥之处欢迎批评指出，工程硬件资料放在立创开源平台链接——[听话的狗子](https://oshwhub.com/45683968445xy/fang-sheng-ji-qi-gou)。


