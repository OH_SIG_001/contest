# Speexdsp测试说明

## 已测试功能

重采样、预处理、回声消除、抖动缓冲。

## 生成库与测试文件

该库基于OpenHarmony3.2Beta1版本进行编译测试。

1.新建子系统

本项目在系统源码third_party目录下创建一个目录作为子系统目录,子系统命名为speexdsp。

在子系统目录下创建ohos.build文件，内容如下：
  
```
{
  "subsystem": "speexdsp",
  "parts": {
    "speexdsp": {
      "module_list": [
        "//third_party/speexdsp/libspeexdsp:testdenoise",
        "//third_party/speexdsp/libspeexdsp:testecho",
        "//third_party/speexdsp/libspeexdsp:testjitter",
        "//third_party/speexdsp/libspeexdsp:testresample",
        "//third_party/speexdsp/libspeexdsp:testresample2"
      ],
      "inner_kits": [
              ],
      "system_kits": [
      ],
      "test_list": [
      ]
    }
  }
}
```

2.将子系统添加到subsystem_config.json

打开//build/subsystem_config.json文件,在最后面加上。

```
  "speexdsp": {
    "path": "third_party/speexdsp",
    "name": "speexdsp"
  }
```

3.将子系统添加到编译中

打开文件//vendor/hihope/rk3568/config.json并在最后添加：

```
   {
      "subsystem": "speexdsp",
      "components": [
        {
          "component": "speexdsp",
          "features": []
        }
      ]
    }
```

4.编译出动态链接库和测试用的可执行文件

编译执行语句如下为：

```
./build.sh --product-name rk3568 --ccache --build-target=speexdsp --target-cpu arm64
```

生成的文件在//out/rk3568目录下。


## 测试逻辑

### 通过原生库示例程序测试

在speexdsp原生库的libspeexdsp目录下有原生的测试示例程序，在rk3568开发板上运行均成功，运行结果均符合示例程序要求，且运行效果与pc端运行效果一致。speexdsp原生库的libspeexdsp目录下有原生的测试源文件testresample.c、testresample2.c、testecho.c、testdenoise.c、testjitter.c。分别测试的是如下四个功能：

### 重采样测试

- 运行testresample.c进行重采样测试
  - 输入需重采样音频（该程序指定输入采样率为96000Hz）
  - 输出指定采样频率音频(该程序指定输出采样率为44100Hz)

### 回声消除测试

- 运行testecho.c进行回声消除测试
  - 输入两份音频
     - 包含底噪音的音频
     - 只包含底噪音的音频
  - 输出底噪音已消除的音频

- 回声消除概念理解，请参考[speexdsp分析文档](./speexdsp_analysis.md)



### 预处理测试

- 运行testdenoise.c进行预处理中的噪声消除测试
  - 输入需降噪的音频（采样率为8000Hz）
  - 输出降噪后的音频

### 抖动缓冲测试

- 运行testjitter.c进行抖动缓冲测试
  - 需要接收来自udp/rtp的网络语音数据，原生测试程序没有做到真正意义上的测试，从程序上告诉用户如何使用接口。

## 测试步骤

1.通过与ohos版本匹配的hdc_std工具,将编译生成的库以及测试用的可执行文件推送到开发板上

```
hdc_std shell               
mount -o remount,rw /	    ## 重新加载系统为可读写
mkdir speexdsp              ## 创建speexdsp目录存放测试用例
exit                        ## 退出开发板系统
```

2.将压缩包push到开发板

```
hdc_std file speexdsp.tar /speexdsp
```

3.解压压缩包并将库文件拷贝到对应的目录

```
hdc_std shell              ## 进入开发板系统
cd speexdsp
tar -xvf speexdsp.tar
cp libspeexdsp_share.z.so /system/lib64/
```

4.执行测试程序

①执行testresample可执行文件

通过分析testresample.c源码可知：执行测试程序时输入一份采样率为96000Hz并且为单声道的音频时，经过重采样输出的音频采样率为44100Hz。

这里测试testresample时，将一份录好的96000Hz单声道音频文件input.pcm和空白的单声道音频文件output.pcm拷贝至开发板speexdsp目录。

执行语句如下

```
chmod 777 testresample                  ## 添加可执行权限
./testresample < input.pcm > output.pcm
```

**测试结果**：经过重采样输出的output.pcm音频采样率为44100Hz。

②执行testresample2可执行文件

通过分析testresample2.c源码可知，执行测试程序时需要指定一份空白的单声道音频文件output.pcm。

测试testresample2时，需要把空白的单声道音频文件output.pcm拷贝至开发板speexdsp目录。

执行语句如下：

```
./testresample2 > output.pcm
```

**测试结果**：输出一份不为空的output.pcm音频文件，并且在终端输出文本如下：

```
1000 0 1024 22 -> 1024 0                  
1100 0 1024 24 -> 1024 15
1200 0 1024 26 -> 926 26
1300 926 1122 31 -> 1048 31
1400 950 1098 33 -> 1032 33
...
...
...
127600 1024 1024 2723 -> 1024 2723
...
...
127900 1024 1024 2729 -> 1024 2729
128000 1024 1024 2731 -> 1024 2730
```
以最后一行"128000 1024 1024 2731 -> 1024 2730“”为例,其中128000为采样率（Hz）;1024为一个编码单元采样点数（帧）;1024为输入音频理论帧长;2731为输出音频理论帧长。"->"符号后的1024为经过重采样处理输入音频实际帧长，2730为输出音频实际帧长。

③执行testecho可执行文件

测试testecho时，需要输入两份音频文件，同时需要指定一份输出的音频文件。

输入的两份音频一份为mic1.pcm（麦克风收录的说话人语音信号+在房间多径反射的语音），另一份为mic2.pcm(麦克风收录的房间多径反射的语音)。
- mic1.pcm正常房间环境下收录说话人说话声音即可，mic2.pcm在正常环境收录时说话人不说话即可。

同时需要指定一份output.pcm输出文件。

执行语句如下：
```
./testecho mic1.pcm mic2.pcm output.pcm
```

**测试结果**：对比输入的mic1.pcm和输出output.pcm的波形图和声谱图，回声已经被消除。

④执行testdenoise可执行文件

通过分析testdenoise.c源码，执行测试程序时需要指定一份输入的不为空的8000Hz的input.pcm音频，并且需要指定一份空的输出的output.pcm音频。

执行语句如下：

```
./testdenoise < input.pcm > output.pcm 
```

**测试结果**：对比输入的input.pcm和输出的outpu.pcm的波形图和声谱图，噪声已经被消除。

⑤执行testjitter可执行文件

通过分析testjitter.c源码，测试需要接收来自udp/rtp的网络语音数据，原生测试程序没有做到真正意义上的测试，从程序上看是告诉用户如何使用接口。

执行语句如下
```
./testjitter
```

**测试结果**:终端打印出语句
```
Frozen sender: Jitter 0
```

### 测试接口

  1. 所有导出接口：

     参考[speexdsp_api.txt](./speexdsp_api.txt)

  2. 测试接口：

     参考[speexdsp_tested_api.txt](./speexdsp_tested_api.txt)
