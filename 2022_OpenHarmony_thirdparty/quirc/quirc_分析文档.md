# quirc核心库分析

## 一、 库实现方式

* 编程语言：C
* 原平台：linux



## 二、 依赖分析

* 除C标准库外，无其他第三方库依赖



## 三、 license以及版权

Copyright (C) 2010-2012 Daniel Beer <[dlbeer@gmail.com](mailto:dlbeer@gmail.com)>



ISC License

**===========**



Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.



## 四、 最新一次版本

* 2021年10月， 版本号1.0



## 五、 功能点分析

* 支持读入png、jpeg格式的图像文件并解析其中包含的二维码信息

* 支持分析图片中二维码数量

* 支持一次性解析多个二维码

* 支持翻转、旋转二维码图片

  



## 六、 代码规模

| decode.c | identify.c | quirc.c | quirc.h | quirc_internal.h | version_db.c | 总行数 |
| -------- | ---------- | ------- | ------- | ---------------- | ------------ | ------ |
| 933      | 1148       | 130     | 178     | 115              | 421          | 2925   |

