import router from '@ohos.router';
import { NoteArray } from '../model/noteModel'
import { startRemoteAbilities } from '../utils/functionUtil'
import { Item, itemData } from '../model/ItemData'
import featureAbility from '@ohos.ability.featureAbility'
import wantConstant from '@ohos.ability.wantConstant'

@Component
@CustomDialog
export struct LightDialog {
  dialogController: CustomDialogController;
  @State select: number = 0
  private lights: string[] = ['客厅灯', '卧室灯', '餐厅灯', '阳台灯']
  @Link selectLight: string
  @State selected: string = '客厅灯'

  build() {
    Column() {
      Row() {
        Image($r("app.media.icon_not_ok"))
          .width(24)
          .height(24)
          .onClick(() => {
            this.dialogController.close()
          })
        Blank()
          .flexGrow(1)
        Image($r("app.media.ic_public_ok_filled"))
          .width(24)
          .height(24)
          .onClick(() => {
            this.dialogController.close()
            this.selectLight = this.selected
          })
      }
      .width('100%')
      .height(48)
      .padding({ left: 12, right: 12 })
      .alignItems(VerticalAlign.Center)

      TextPicker({ range: this.lights, selected: this.select })
        .height('50%')
        .onChange((value: string, index: number) => {
          this.selected = value
        })
    }
  }
}

@Component
@CustomDialog
export struct deleteDialog {
  dialogController: CustomDialogController;
  @Prop index: number
  @State confirmed: boolean = true

  build() {
    Column() {
      Row() {
        Text('此笔记将从本地、云空间及所有同步设备删除。是否删除？').fontSize(16)
      }
      .margin({ top: 25 })
      .width('100%')
      .justifyContent(FlexAlign.Center)

      Row() {
        Toggle({ type: ToggleType.Checkbox, isOn: this.confirmed })
          .size({ width: 20, height: 20 })
          .selectedColor($r('app.color.checked_blue'))
          .onChange((isOn: boolean) => {
            this.confirmed = isOn
          })
        Text('我已阅读并了解').fontSize(16)
      }
      .justifyContent(FlexAlign.Start)
      .onClick(() => {
        this.confirmed = !this.confirmed
      })

      Flex({ justifyContent: FlexAlign.SpaceEvenly }) {
        Text('取消')
          .fontSize(16)
          .backgroundColor(0xffffff)
          .fontColor($r('app.color.background_blue'))
          .onClick(() => {
            this.dialogController.close()
          })
        Divider()
          .height(5)
          .vertical(true)
          .color('#D1D1D1')
          .strokeWidth(2)

        Button('删除', { type: ButtonType.Normal })
          .enabled(this.confirmed)
          .fontSize(16)
          .backgroundColor(0xffffff)
          .opacity(this.confirmed ? 1 : 0.4)
          .fontColor(Color.Red)
          .onClick(() => {
            this.dialogController.close()
            NoteArray.splice(this.index, 1)
            router.replace({
              url: 'pages/FamilyNote',
            })
          })
      }.margin({ bottom: 15 })
    }
    .alignItems(HorizontalAlign.Start)
    .justifyContent(FlexAlign.SpaceBetween)
    .width('70%')
    .height(180)
    .borderRadius(25)
    .backgroundColor(Color.White)
  }
}

@Component
export struct shareItem {
  @State imgSrc: Resource = $r('app.media.icon')
  @State text: string = ''
  @State bundleName: string = ''

  build() {
    Column() {
      Image(this.imgSrc)
        .width(64)
        .height(64)
      Text(this.text)
        .fontSize(14)
        .margin({ top: 10 })
    }
    .width(90)
    .padding({ top: 10 })
    .margin({ left: 88 })
    .onClick(() => {
      featureAbility.startAbility(
        {
          want:
          {
            action: "android.intent.action.MAIN",
            entities: ["android.intent.category.LAUNCHER"],
            flags: wantConstant.Flags.FLAG_NOT_OHOS_COMPONENT,
            bundleName: this.bundleName,
          },
        }
      ).then((data) => {
        console.info("==========================>startAbility=======================>");
      });
    })
  }
}

@CustomDialog
export struct shareDialog {
  private swiperController: SwiperController = new SwiperController()
  dialogController: CustomDialogController;
  data: Item[][] = []

  aboutToAppear(): void {
    // 初始化data数组
    for (let i = 0;i <= (itemData.length / 8 | 0); i++) {
      this.data.push([])
    }

    // 将Item项循环放进data数组并做分页操作
    for (let i = 0; i <= (itemData.length / 8 | 0); i++) {
      if (i == (itemData.length / 8 | 0)) {
        for (let j = 0;j < itemData.length % 8; j++) {
          this.data[i].push(itemData[i * 8+j]);
        }
      } else {
        for (let j = 0;j < 8; j++) {
          this.data[i].push(itemData[i * 8+j]);
        }
      }
    }
  }

  build() {
    Column() {
      Row() {
        Image($r('app.media.ic_public_arrow_down_0'))
          .width(32)
          .height(32)
          .onClick(() => {
            this.dialogController.close()
          })
      }
      .justifyContent(FlexAlign.Center)

      Swiper(this.swiperController) {
        ForEach(this.data, item1 => {
          Flex({ direction: FlexDirection.Row, justifyContent: FlexAlign.Start, wrap: FlexWrap.Wrap }) {
            ForEach(item1, item2 => {
              shareItem({ imgSrc: item2.imgSrc, text: item2.text })
            })
          }
          .padding({ bottom: 50 })
        })
      }
    }
    .width('100%')
    .backgroundColor('white')
    .borderRadius(35)
  }
}

@Component
export struct noteItem {
  @State index: number = 0
  @State title: string = ''
  @State date: string = ''
  @State backgroundColor: ResourceColor = $r('app.color.background_white')
  callback: (event?: ClickEvent) => void

  build() {
    Button({ type: ButtonType.Normal }) {
      Column() {
        Text(this.title)
          .fontSize(20)
          .textOverflow({ overflow: TextOverflow.Ellipsis })
          .maxLines(1)
        Text(this.date)
          .fontColor($r('app.color.placeholderColor_grey'))
          .fontSize(14)
          .margin({ top: 5 })
      }
      .width('98%')
      .alignItems(HorizontalAlign.Start)
      .justifyContent(FlexAlign.Center)
      .padding({ left: 10 })
    }
    .height(80)
    .borderRadius(20)
    .backgroundColor(this.backgroundColor)
    .onClick(this.callback)
  }
}

@Component
export struct clickItem {
  @State imgSrc: Resource = $r('app.media.icon')
  @State text: string = ''
  callback: (event?: ClickEvent) => void

  build() {
    Column() {
      Image(this.imgSrc)
        .width(24)
        .height(24)
      Text(this.text)
        .margin({ top: 5 })
        .fontSize(12)
    }
    .onClick(this.callback)
  }
}

@Component
export struct cardItem {
  text: string | Resource
  url: string

  build() {
    Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center }) {
      Text(this.text)
        .fontSize(25)
        .fontColor(Color.White)
    }
    .margin({ left: 30 })
    .height('42%')
    .width('20%')
    .borderRadius(30)
    .backgroundColor($r('app.color.background_blue'))
    .onClick(() => {
      router.push({
        url: this.url,
      })
    })
  }
}

@Component
export struct TextRow {
  @State text: string = ''
  @State value: string = ''

  build() {
    Row() {
      Column() {
        Text(this.text)
          .fontSize(40)
          .fontColor(Color.White)
      }.width('48%')

      Column() {
        Text(this.value)
          .fontSize(40)
          .fontColor(Color.White)
      }.width('48%')
    }
  }
}

/**
 * 设备列表弹窗组件
 */
@CustomDialog
@Component
export struct DeviceListDialog {
  @Prop videoIndex: number
  // 获取组网设备列表数据
  @Consume deviceList: any[]
  // 被选择的设备id
  private selectedDevices: any[] = [];
  // 用于控制弹窗的显示和关闭
  controller: CustomDialogController;

  build() {
    Column() {
      Text("选择设备")
        .fontWeight(FontWeight.Bold)
        .fontSize(20)
        .margin({ top: 20, bottom: 10 })

      List() {
        ForEach(this.deviceList, item => {
          ListItem() {
            Row() {
              Text(item.deviceName)
                .fontSize(18)
                .layoutWeight(1)

              Toggle({ type: ToggleType.Checkbox })
                .onChange((isOn: boolean) => {
                  if (isOn) {
                    // 添加被选择的设备deviceId
                    this.selectedDevices.push(item.deviceId)
                  } else {
                    // 移除未被选择的设备deviceId
                    for (var i = 0; i < this.selectedDevices.length; i++) {
                      if (this.selectedDevices[i] === item.deviceId) {
                        this.selectedDevices.splice(i, 1);
                      }
                    }
                  }
                })
                .width(20)
                .height(20)
            }
            .padding({ left: 30, right: 30 })
          }
        }, item => item.deviceId.toString())
      }
      .height("30%")
      .align(Alignment.TopStart)

      Row() {
        Button("取消")
          .layoutWeight(1)
          .backgroundColor(Color.White)
          .fontColor(Color.Blue)
          .height(60)
          .onClick(() => {
            // 关闭弹窗
            this.controller.close();
          })

        Button("确定")
          .layoutWeight(1)
          .backgroundColor(Color.White)
          .fontColor(Color.Blue)
          .height(60)
          .onClick(() => {
            // 关闭弹窗
            this.controller.close();
            startRemoteAbilities(this.selectedDevices, this.videoIndex)
          })
      }
    }
  }
}

@Component
export struct DeviceTitle {
  deviceTitle: Resource = $r("app.string.smartCurtain")

  build() {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
      Image($r("app.media.ic_public_back"))
        .width(24)
        .height(24)
        .onClick(() => {
          router.back()
        })
      Text(this.deviceTitle)
        .margin({ left: 16 })
        .fontSize(18)
        .width(193)
        .flexGrow(1)
      Image($r("app.media.icon_about"))
        .margin({ left: 39 })
        .width(24)
        .height(24)
        .onClick(() => {
        })
    }
    .height(60)
  }
}

@Component
export struct DeviceLogo {
  @State deviceLogo: Resource = $r("app.media.icon_curtain")

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Image(this.deviceLogo)
        .width(200)
        .height(200)
    }
    .margin({ top: 8 })
  }
}

@Component
export struct DeviceState {
  @State deviceSwitchText: Resource = $r("app.string.closed")
  deviceSwitchTextColor
  deviceState: Resource | string
  deviceStateColor: Resource
  currentState: Resource
  currentStateColor: Resource
  isAlert: boolean
  switchImg: Resource
  batteryImg: Resource
  electricQuantity: string
  callback: (event?: ClickEvent) => void

  build() {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
      Text(this.deviceSwitchText)
        .fontSize(18)
        .fontWeight(13.5)
        .fontColor(this.deviceSwitchTextColor)
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Text(this.deviceState)
          .fontSize(24)
          .fontColor($r("app.color.background_green"))
          .opacity(0.9)
          .fontColor(this.deviceStateColor)
        Text(this.currentState)
          .fontSize(12)
          .opacity(0.6)
          .fontColor(this.currentStateColor)
      }
      .width(114)
      .layoutWeight(1)

      if (this.isAlert) {
        Image(this.switchImg)
          .width(56)
          .height(56)
          .onClick(this.callback)
      }
      else {
        Image(this.batteryImg)
          .width(24)
          .height(24)
        Text(this.electricQuantity)
          .width(30)
          .height(24)
          .fontSize(14)
          .margin({ left: 1 })
      }
    }
    .backgroundColor($r("app.color.background_white"))
    .height(88)
    .padding({ left: 16, right: 16 })
    .borderRadius(16)
  }
}

@Component
export struct componentsRow {
  text: Resource | string
  textSize: number
  @State textColor: Resource = $r("app.color.background_black")
  textSub: string
  textSubSize: number
  textSubColor: Resource
  @State image: Resource = $r("app.media.icon_close_door")
  frameWidth: string

  build() {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.SpaceBetween }) {
      Text(this.text)
        .fontSize(this.textSize)
        .fontColor(this.textColor)
      if (this.textSub) {
        Text(this.textSub)
          .fontSize(this.textSubSize)
          .fontColor(this.textSubColor)
      } else {
        Image(this.image)
          .width(24)
          .height(24)
      }
    }
    .height(64)
    .width(this.frameWidth)
    .backgroundColor($r("app.color.background_white"))
    .padding({ left: 16, right: 16 })
    .borderRadius(16)
  }
}

@Component
export struct Stow {
  build() {
    Flex({ direction: FlexDirection.Column }) {
      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Text($r("app.string.stow"))
          .fontSize(16)
          .opacity(0.6)
        Image($r("app.media.icon_tucked"))
          .width(25)
          .height(25)
      }.margin({ top: 8 }).height(35)

      Text($r("app.string.equipmentServices")).fontSize(18).margin({ top: 16 })
    }
  }
}

@Component
export struct ProgressBar {
  text: Resource | string
  textSize: number
  @Link textProgress: number
  textProgressSize: number
  progressWidth: string
  isSendCommand: boolean
  deviceId: string

  build() {
    Row() {
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Start, justifyContent: FlexAlign.Center }) {
        Text(this.text)
          .fontSize(this.textSize)
        Text(this.textProgress.toFixed(0) + '%')
          .fontSize(this.textProgressSize)
          .fontColor($r("app.color.background_blue"))
      }

      Row() {
        Slider({
          value: this.textProgress,
          min: 0,
          max: 100,
          step: 1.8,
        })
          .blockColor(Color.Blue)
          .trackColor($r("app.color.background_grey"))
          .selectedColor(Color.Blue)
          .showSteps(true)
          .showTips(true)
          .onChange((value: number, mode: SliderChangeMode) => {
            this.textProgress = value
          })
      }
      .width(this.progressWidth)
    }
    .height(64)
    .width('100%')
    .backgroundColor($r("app.color.background_white"))
    .padding({ left: 16, right: 16 })
    .borderRadius(16)
    .margin({ top: 12 })
  }
}

@Component
export struct componentsRowColumn {
  text: Resource
  textSize: number
  textColumn: Resource
  textColumnSize: number
  isSwitchImge: boolean
  isSendCommand: boolean
  deviceId: string

  build() {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.SpaceBetween }) {
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Start, justifyContent: FlexAlign.Center }) {
        Text(this.text)
          .fontSize(this.textSize)
        Text(this.textColumn)
          .fontSize(this.textColumnSize)
          .opacity(0.6)
      }

      if (this.isSwitchImge) {
        Image($r("app.media.img_switch_on"))
          .width(40)
          .height(24)
      } else {
        Image($r("app.media.icon_notice"))
          .width(25)
          .height(25)
      }
    }
    .height(64)
    .width('100%')
    .backgroundColor($r("app.color.background_white"))
    .padding({ left: 16, right: 16 })
    .borderRadius(16)
    .margin({ top: 12 })
  }
}