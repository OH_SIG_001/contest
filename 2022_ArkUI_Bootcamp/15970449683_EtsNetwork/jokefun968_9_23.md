APP绝大多数都是需要网络请求来加载数据的，本项目案例利用鸿蒙ArkUI开发框架中的@ohos.net.http模块提供的网络请求相关API，
实现了查询输入城市的天气预报、查询新闻列表及新闻详情浏览两个案例应用，
同时本应用利用了ETS声明式开发范式的语法特点，实现了页面请求传参、组件循环渲染、网页加载、页面路由跳转等基础功能，
对于学习鸿蒙ETS的网络应用开发有一定的借鉴价值。

开发工具版本：DevEco Studio 3.0 Beta4 Build Version 3.0.0.991
项目配置：Compile SDK: 8     Language: eTS


（1）天气预报查询案例
第一步，准备好数据接口
天气预报查询接口地址：http://apis.juhe.cn/simpleWeather/query?key=XXX&city=XXXX
天气预报查询接口调用案例：http://apis.juhe.cn/simpleWeather/query?key=397c9db4cb0621ad0313123dab416668&city=南昌
返回的JSON格式数据：
{"reason":"查询成功!","result":{"city":"南昌","realtime":{"temperature":"25","humidity":"56","info":"多云","wid":"01","direct":"北风","power":"4级","aqi":"43"},
"future":[{"date":"2022-09-23","temperature":"22\/30℃","weather":"多云","wid":{"day":"01","night":"01"},"direct":"北风"},{"date":"2022-09-24","temperature":"22\/30℃","weather":"多云",
"wid":{"day":"01","night":"01"},"direct":"东北风转北风"},{"date":"2022-09-25","temperature":"23\/31℃","weather":"多云转晴","wid":{"day":"01","night":"00"},"direct":"东北风转北风"},
{"date":"2022-09-26","temperature":"23\/31℃","weather":"晴","wid":{"day":"00","night":"00"},"direct":"东北风转北风"},{"date":"2022-09-27","temperature":"24\/33℃","weather":"晴",
"wid":{"day":"00","night":"00"},"direct":"北风"}]},"error_code":0}

第二步，根据接口返回的数据格式生成数据模型类
在工程项目下src/main/ets/MainAbility下新建model文件夹，并根据上面的JSON返回格式创建天气预报返回数据的数据模型文件weatherModel.ets文件，

export class WeatherModel {
  reason: string //返回说明
  error_code: number //返回码，0为查询成功
  result: {
    city: string
    realtime: RealtimeWeatherData // 实时天气
    future: Array<WeatherData> // 往后天气情况
  }
}

export class RealtimeWeatherData {
  info: string = '多云' // 天气情况
  temperature: string = '10' // 当前温度
  direct: string = '东风' //风向
}

export class WeatherData {
  date: string // 日期
  temperature: string // 温度
  direct: string // 风向
  weather: string // 天气情况
}

export function getTest() {//生成future的初始化数据函数
  return [
    {
      "date": "2021-11-15",
      "direct": "东北风转北风",
      "temperature": "-1/12℃",
      "weather": "多云转晴",
      "wid": {
        "day": "01",
        "night": "00"
      }
    },
    {
      "date": "2021-11-16",
      "direct": "西南风",
      "temperature": "0/13℃",
      "weather": "晴转多云",
      "wid": {
        "day": "00",
        "night": "01"
      }
    },
    {
      "date": "2021-11-17",
      "direct": "西南风",
      "temperature": "0/13℃",
      "weather": "晴",
      "wid": {
        "day": "00",
        "night": "00"
      }
    },
    {
      "date": "2021-11-18",
      "direct": "东北风",
      "temperature": "2/12℃",
      "weather": "晴",
      "wid": {
        "day": "00",
        "night": "00"
      }
    },
    {
      "date": "2021-11-19",
      "direct": "东北风转东南风",
      "temperature": "2/10℃",
      "weather": "多云",
      "wid": {
        "day": "01",
        "night": "01"
      }
    }
  ]
}


只有字段、类型及结构一一对应，ETS程序才能正常解析出JSON数据。
建议使用https://www.bejson.com/jsonviewernew/ JSON在线视图查看器对JSON数据结构看清晰来编写核对。

第三步，调用http模块的API接口发送网络请求并返回数据
  先说明下，this.input_city表示的是用户在界面数据的城市名称，如'南昌'，通过以下代码完成了查询城市的录入和获取

     Row() {
        Text('城市名称：')
          .fontSize(15).width('25%').textAlign(TextAlign.Center)

        TextInput({ placeholder: "城市中文名称"})
          .layoutWeight(1)
          .height(45)
          .type(InputType.Normal)
          .fontColor(Color.Brown)
          .enterKeyType(EnterKeyType.Next)
          .caretColor(Color.Red)
          .placeholderColor(Color.Green)
          .placeholderFont({
            size: 15,
            style: FontStyle.Italic,
            weight: FontWeight.Bold
          }).width('45%')
          .onChange((value) => {
            this.input_city = value;
          })

        Button() {
          Text('获取天气数据')
            .width('30%')
            .fontSize(15)
            .fontColor(Color.White)
            .textAlign(TextAlign.Center)
        }
        .padding(20)
        .onClick(() => {
          if(this.input_city ==''){
            prompt.showToast({
              message: "请输入城市名称"
            })
          }else{
            this.loadingHint = '加载中...'
            this.getRequest()
          }
        })

       }.width("100%")

  直接上http请求的调用代码getRequest()，我们这里采用的是GET请求方法
// 请求方式：GET
  getRequest() {
    // 每一个httpRequest对应一个http请求任务，不可复用
    let httpRequest = http.createHttp()
    let url = 'http://apis.juhe.cn/simpleWeather/query?key=397c9db4cb0621ad0313123dab416668&city='+this.input_city
    httpRequest.request(url, (err, data) => {
      if (!err) {
        if (data.responseCode == 200) {
          console.info('=====data.result=====' + data.result)
          // 解析数据
          var weatherModel: WeatherModel = JSON.parse(data.result.toString())
          // 判断接口返回码，0成功
          if (weatherModel.error_code == 0) {
            // 设置数据
            this.realtime = weatherModel.result.realtime
            this.future = weatherModel.result.future
            this.city =weatherModel.result.city
            this.isRequestSucceed = true
          } else {
            // 接口异常，弹出提示
            prompt.showToast({ message: weatherModel.reason })
          }
        } else {
          // 请求失败，弹出提示
          prompt.showToast({ message: '网络异常' })
        }
      } else {
        // 请求失败，弹出提示
        prompt.showToast({ message: err.message })
      }
    })
  }

重点是这句var weatherModel: WeatherModel = JSON.parse(data.result.toString()) data表示http请求返回的数据，data.result就是真实的返回的json数据内容。
前面也要引入建好的数据模型import {WeatherModel, RealtimeWeatherData, WeatherData, getTest} from '../model/weatherModel';
解析后将值付给提前定义好的变量，再渲染出来展示到界面上
  @State realtime: RealtimeWeatherData = new RealtimeWeatherData()
  @State future: Array<WeatherData> = getTest()

          ForEach(this.future, (item: WeatherData) => {
            Row() {
              this.WeatherText(item.date)
              this.WeatherText(item.temperature)
              this.WeatherText(item.weather)
              this.WeatherText(item.direct)
            }
          }, item => item.date)



（2）新闻列表浏览案例
第一步，准备好新闻列表数据接口
新闻列表查询接口地址：http://120.26.195.225:8080/ServletDemo/NewsQuery ,这是我自己编写的一个java后台数据接口，大家也可以到网上找找其他新闻接口
返回的JSON格式数据：
{"result":[
{"image":"http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85","path":"https://www.163.com/dy/article/G1OBC8LO0514BCL4.html","passtime":"2021-02-02 10:00:51","title":"被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"},
{"image":"http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85","path":"https://www.163.com/dy/article/G1OBC8LO0514BCL4.html","passtime":"2021-02-02 10:00:51","title":"被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"},
{"image":"http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85","path":"https://www.163.com/dy/article/G1OBC8LO0514BCL4.html","passtime":"2021-02-02 10:00:51","title":"被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"},
{"image":"http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85","path":"https://www.163.com/dy/article/G1OBC8LO0514BCL4.html","passtime":"2021-02-02 10:00:51","title":"被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"},
{"image":"http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85","path":"https://www.163.com/dy/article/G1OBC8LO0514BCL4.html","passtime":"2021-02-02 10:00:51","title":"被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"},
{"image":"http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85","path":"https://www.163.com/dy/article/G1OBC8LO0514BCL4.html","passtime":"2021-02-02 10:00:51","title":"被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"},
{"image":"http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85","path":"https://www.163.com/dy/article/G1OBC8LO0514BCL4.html","passtime":"2021-02-02 10:00:51","title":"被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"},
{"image":"http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85","path":"https://www.163.com/dy/article/G1OBC8LO0514BCL4.html","passtime":"2021-02-02 10:00:51","title":"被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"},
{"image":"http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85","path":"https://www.163.com/dy/article/G1OBC8LO0514BCL4.html","passtime":"2021-02-02 10:00:51","title":"被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"},
{"image":"http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85","path":"https://www.163.com/dy/article/G1OBC8LO0514BCL4.html","passtime":"2021-02-02 10:00:51","title":"被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"}],
"code":200,"message":"获取新闻成功"}

第二步，根据接口返回的数据格式生成数据模型类
在工程项目下src/main/ets/MainAbility/model文件夹下，根据上面的JSON返回格式创建新闻列表返回数据的数据模型文件NewsModel.ets文件，
export class NewsModel {
  message: string
  code: number //返回码，200为成功
  result: Array<NewsData>// 新闻列表
}

export class NewsData {
  image: string
  path: string
  passtime: string
  title: string
}

export function getTest() {//本函数生成的result初始化数据
  return [
    {
      "image": "http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85",
      "path": "https://www.163.com/dy/article/G1OBC8LO0514BCL4.html",
      "passtime": "2021-02-02 10:00:51",
      "title": "被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"
    },
    {
      "image": "http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85",
      "path": "https://www.163.com/dy/article/G1OBC8LO0514BCL4.html",
      "passtime": "2021-02-02 10:00:51",
      "title": "被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"
    },
    {
      "image": "http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85",
      "path": "https://www.163.com/dy/article/G1OBC8LO0514BCL4.html",
      "passtime": "2021-02-02 10:00:51",
      "title": "被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"
    },
    {
      "image": "http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85",
      "path": "https://www.163.com/dy/article/G1OBC8LO0514BCL4.html",
      "passtime": "2021-02-02 10:00:51",
      "title": "被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"
    },
    {
      "image": "http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85",
      "path": "https://www.163.com/dy/article/G1OBC8LO0514BCL4.html",
      "passtime": "2021-02-02 10:00:51",
      "title": "被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"
    },
    {
      "image": "http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85",
      "path": "https://www.163.com/dy/article/G1OBC8LO0514BCL4.html",
      "passtime": "2021-02-02 10:00:51",
      "title": "被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"
    },
    {
      "image": "http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85",
      "path": "https://www.163.com/dy/article/G1OBC8LO0514BCL4.html",
      "passtime": "2021-02-02 10:00:51",
      "title": "被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"
    },
    {
      "image": "http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85",
      "path": "https://www.163.com/dy/article/G1OBC8LO0514BCL4.html",
      "passtime": "2021-02-02 10:00:51",
      "title": "被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"
    },
    {
      "image": "http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85",
      "path": "https://www.163.com/dy/article/G1OBC8LO0514BCL4.html",
      "passtime": "2021-02-02 10:00:51",
      "title": "被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"
    },
    {
      "image": "http://dingyue.ws.126.net/2021/0201/b63f2e50j00qntwfh0020c000hs00npg.jpg?imageView&thumbnail=140y88&quality=85",
      "path": "https://www.163.com/dy/article/G1OBC8LO0514BCL4.html",
      "passtime": "2021-02-02 10:00:51",
      "title": "被指偷拿半卷卫生纸 63岁女洗碗工服药自杀 酒店回应"
    }
  ]
}

第三步，调用http模块的API接口发送网络请求并返回数据
直接上代码，我们这里采用的是GET请求方法
// 请求方式：GET
  private httpRequest() {
    let httpRequest = http.createHttp();
    let url = 'http://120.26.195.225:8080/ServletDemo/NewsQuery'
    httpRequest.request(url, (err, data) => {
      if (!err) {
        if (data.responseCode == 200) {
          console.info('=====data.result=====' + data.result)
          // 解析数据
          var newsModel: NewsModel = JSON.parse(data.result.toString())
          // 判断接口返回码，0成功
          if (newsModel.code == 200) {
            // 设置数据
            this.isRequestSucceed = true
            this.newsresult = newsModel.result
          } else {
            // 接口异常，弹出提示
            prompt.showToast({ message: newsModel.message })
          }
        } else {
          // 请求失败，弹出提示
          prompt.showToast({ message: '网络异常' })
        }
      } else {
        // 请求失败，弹出提示
        prompt.showToast({ message: err.message })
      }
    })

  }

与天气预报的处理逻辑类似，http接口返回的数据通过NewsModel解析出来后渲染到界面展示。
import {NewsModel, NewsData, getTest} from '../model/NewsModel';

            ForEach(this.newsresult,
              (item:NewsData) => {
                ListItem() {
                 Row() {
                   Image(item.image).width(80).height(80)
//                   this.NewsText(item.title)

                   Text(item.title)
                     .fontSize(20)
                     .layoutWeight(1)
                     .textAlign(TextAlign.Start)
                     .margin({ top: 5, bottom: 5,left:5,right:5 })
                     .onClick(()=>{
                       router.push({
                         url:"pages/webShow",
                         params:{
                           web_url:item.path
                         }
                       })
                     })

//                   this.NewsText(item.path)
                   this.NewsText(item.passtime)
                   }

                }
              })

（3）config.json配置文件更改
"deviceConfig"下面增加配置如下

  "deviceConfig": {
    "default": {
      "network": {
        "cleartextTraffic": true
      }
    }
  },


"abilities"前面增加以下配置，加载网络资源时，需要添加 ohos.permission.INTERNET 权限

    "reqPermissions": [
      {
        "name": "ohos.permission.INTERNET"
      }
    ],

 "pages"下面把新建的文件配置进去
 "pages": [
          "pages/index",
          "pages/weatherhttpdemo",
          "pages/newshttpdemo",
          "pages/webShow"
        ]

（4）页面传参
新闻列表案例newshttpdemo.ets中使用了页面传参，ForEach渲染数据时给Text增加了onClick事件，router路由跳转增加了params参数web_url
                   Text(item.title)
                     .fontSize(20)
                     .layoutWeight(1)
                     .textAlign(TextAlign.Start)                     
                     .margin({ top: 5, bottom: 5,left:5,right:5 })
                     .onClick(()=>{
                       router.push({
                         url:"pages/webShow",
                         params:{
                           web_url:item.path
                         }
                       })
                     }

然后在webShow.ets中接收router参数
private web_url: string = router.getParams()['web_url']

说明下router参数获取的API版本
   // 3.1.5.5版本之前，取值方式为：router.getParams().key
  private value: string = router.getParams().value;
  // 从3.1.6.5版本起，取值方式为：router.getParams()['key']
  private value: string = router.getParams()['value'];

（5）web网页加载
加载web网页，首先需要添加 ohos.permission.INTERNET 权限
定义WebController组件，WebController 用来控制Web组件的各种行为，一个 WebController 对象只能控制一个 Web 组件，且必须在 Web 组件和 WebController 绑定后，才能调用 WebController 上的方法。
private webController: WebController = new WebController();

定义Web组件，使用 Web 组件时，需要传递一个 WebOptions 类型的参数， WebOptions 类型说明如下：src：待加载的网页资源地址、controller：页面控制器
      Web({
        src: this.web_url,
        controller: this.webController
      })
        .width("100%")
        .height("100%")

然后在页面加载的时候把通过参数传递过来的页面地址进行加载，这样不需要另外手动点击加载页面
  onPageShow(){//生命周期方法
    console.info('当页面组件被显示时，调用onPageShow函数')
    this.webController.loadUrl({
      url: this.web_url
    })
  }




