【项目名称】：基于openharmony的纯净水小游戏

【负责人】 ：姚婉彤

#### **描述**

本方案致力于目标06：清洁饮水和卫生设施

目前已经实现的功能有：页面布局、人物移动、获取道具、重新开始、死亡等。

#### **产品价值**

纯净水计划是用一个小游戏的形式展示我们入口每一滴水的来之不易，小水滴经过重重关卡才能到达终点，在此期间它要面临蒸发、污染等危险。

#### **学习过程**
#### **心得体会**

通过样例共建让我们从零开始学习新的编译器，我们也因此学会了此编译器要求我们运用的语言，我们从零开始学了JavaScript，css，hml，在打代码发现，有些js在查找文档时跟编译器不一样，那时候查找了几天无果，最后仔细阅读了官方文档，才发现解决思路，让我学习到打代码要耐心，面对新知识要认真，最后实现了几个功能，有些功能我们在目前存在的知识还尚且实现不了，但是通过这次活动，我们收获很多
 
#### **遇到的困难**
对纯净度的变化试了很多方法，不知道如何在画布上使文字进行动态变化，以及水滴移动时画面的移动，还有水滴移动的卡顿

#### **需要改进**
对水滴的纯净度变化，以及水滴的人物变化，还有关卡的连贯性，界面的优化程度，敌人的升级等等

#### **项目思路**

 

游戏开始->创建背景->创建水滴->水滴运动->避开障碍物->获取道具提高纯净度->到达终点

程序状态图：


![](image/d.png)

交互图：

![](image/a.png) 

 

 

界面图：
![](image/b.png) 

 

 

死亡后的界面

![](image/c.png) 


 

 

 

视频链接：https://www.zhihu.com/zvideo/1539975366810984448