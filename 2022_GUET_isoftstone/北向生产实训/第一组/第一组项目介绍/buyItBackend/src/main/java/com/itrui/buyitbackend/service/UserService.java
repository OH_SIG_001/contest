package com.itrui.buyitbackend.service;

import com.itrui.buyitbackend.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserService {

    /**
     * 用户登录
     * @param account
     * @return
     */
    public User userLogin(Integer account);

    /**
     * 用户注册
     * @param user
     * @return
     */
    public boolean userRegister(User user);

    /**
     * 修改密码（忘记密码）
     * @param user
     * @return
     */
    public boolean updataUserPasswprd(User user);

    /**
     * 获取全部用户
     * @return
     */
    public List<User> getAllUSer();


    /**
     * 通过账号查找用户
     * @param account
     * @return
     */
    public User getUerByAccount(Integer account);

    /**
     * 通过id查找用户
     * @param id
     * @return
     */
    public User getUserById(Integer id);
}
