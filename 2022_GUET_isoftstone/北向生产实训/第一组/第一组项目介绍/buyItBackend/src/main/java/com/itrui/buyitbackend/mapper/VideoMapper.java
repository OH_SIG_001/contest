package com.itrui.buyitbackend.mapper;

import com.itrui.buyitbackend.pojo.Video;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface VideoMapper {

    /**
     * 获取全部视频
     * @return
     */
    public List<Video> getALlVideos();


}
