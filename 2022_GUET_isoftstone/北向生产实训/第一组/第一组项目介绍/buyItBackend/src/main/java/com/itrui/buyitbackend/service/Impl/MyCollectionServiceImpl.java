package com.itrui.buyitbackend.service.Impl;

import com.itrui.buyitbackend.mapper.MyCollectionMapper;
import com.itrui.buyitbackend.pojo.MyCollection;
import com.itrui.buyitbackend.service.MyCollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyCollectionServiceImpl implements MyCollectionService {

    @Autowired
    private MyCollectionMapper myCollectionMapper;

    @Override
    public List<MyCollection> getAllCollection(Integer account) {
        return myCollectionMapper.getAllCollection(account);
    }

    @Override
    public boolean delCollection(Integer id) {
        return myCollectionMapper.delCollection(id) > 0;
    }

    @Override
    public boolean addCollection(MyCollection myCollection) {
        return myCollectionMapper.addCollection(myCollection) > 0;
    }

    @Override
    public int getCollectionNumInt(Integer account) {
        return myCollectionMapper.getCollectionNumInt(account);
    }
}
