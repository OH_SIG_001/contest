package com.itrui.buyitbackend.service.Impl;

import com.itrui.buyitbackend.mapper.OrderMapper;
import com.itrui.buyitbackend.pojo.Order;
import com.itrui.buyitbackend.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;


    @Override
    public List<Order> getALlOrderByUserAccount(Integer account) {
        return orderMapper.getALlOrderByUserAccount(account);
    }

    @Override
    public boolean updateOrderStatusById(Order order) {
        return orderMapper.updateOrderStatusById(order) > 0;
    }

    @Override
    public List<Order> getAllOrderByType(Integer account, String type) {
        return orderMapper.getAllOrderByType(account, type);
    }

    @Override
    public boolean addOrder(Order order) {
        return orderMapper.addOrder(order) > 0;
    }

    @Override
    public boolean delOrderById(Integer id) {
        return orderMapper.delOrderById(id) > 0;
    }

}
