package com.itrui.buyitbackend.mapper;

import com.itrui.buyitbackend.pojo.MyAddress;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MyAddressMapper {

    /**
     * 获取用户地址
     * @param account
     * @return
     */
    public List<MyAddress> getAllAddressByAccount(@Param("account") Integer account);

    /**
     * 删除地址
     * @param id
     * @return
     */
    public int delAddressById(@Param("id") Integer id);

    /**
     * 修改地址
     * @param myAddress
     * @return
     */
    public int updateAddress(MyAddress myAddress);

    /**
     * 添加地址信息
     * @param myAddress
     * @return
     */
    public int addAddress(MyAddress myAddress);

}
