package com.itrui.buyitbackend.mapper;

import com.itrui.buyitbackend.pojo.Car;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CarMapper {
    /**
     * 查找用户购物车
     * @param account
     * @return
     */
    public List<Car> getAllCar(@Param("account") Integer account);


    /**
     * 删除用户购物车
     * @param id
     * @return
     */
    public int delCar(@Param("id") Integer id);


    /**
     * 添加购物车c
     * @param car
     * @return
     */
    public int addCar(Car car);

    /**
     *
     * @param account
     * @return
     */
    public int getCarNum(@Param("account") Integer account);
}
