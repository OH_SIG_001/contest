package com.itrui.buyitbackend.controller;

import com.itrui.buyitbackend.common.Code;
import com.itrui.buyitbackend.common.Result;
import com.itrui.buyitbackend.mapper.OrderMapper;
import com.itrui.buyitbackend.pojo.Order;
import com.itrui.buyitbackend.pojo.Product;
import com.itrui.buyitbackend.service.OrderService;
import com.itrui.buyitbackend.util.SetProductPicHeader;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 获取用户订单
     *
     * @param account
     * @return
     */
    @GetMapping("/{account}")
    public Result getALlOrderByUserAccount(HttpServletRequest request, @PathVariable Integer account) {
        String url = request.getRequestURL().toString();
        List<Order> aLlOrderByUserAccount = orderService.getALlOrderByUserAccount(account);
        for (int i = 0; i < aLlOrderByUserAccount.size(); i++) {
            Product product = SetProductPicHeader.setProduct(aLlOrderByUserAccount.get(i).getOrderProduct(), url);
            aLlOrderByUserAccount.get(i).setOrderProduct(product);
        }
        return new Result(Code.GET_OK, aLlOrderByUserAccount, "查询成功");
    }

    ;

    /**
     * 支付成功后修改用户订单装状态
     *
     * @param order
     * @return
     */
    @PutMapping
    public Result updateOrderStatusById(@RequestBody Order order) {
        log.info("修改订单状态的信息："+order);
        boolean flag = orderService.updateOrderStatusById(order);
        return new Result(flag ? Code.UPDATE_OK : Code.UPDATE_ERR, flag ? "修改成功" : "修改失败");
    }

    ;

    /**
     * 分类查找
     *
     * @param account
     * @param type
     * @return
     */
    @GetMapping("/{account}/{type}")
    public Result getAllOrderByType(HttpServletRequest request, @PathVariable Integer account, @PathVariable String type) {
        log.info("订单查询账号："+account);
        log.info("订单查询类型："+type);

        String url = request.getRequestURL().toString();
        List<Order> allOrderByType = orderService.getAllOrderByType(account, type);

        for (int i = 0; i < allOrderByType.size(); i++) {
            Product product = SetProductPicHeader.setProduct(allOrderByType.get(i).getOrderProduct(), url);
            allOrderByType.get(i).setOrderProduct(product);

        }
        return new Result(Code.GET_OK, allOrderByType,"查询成功");
    }

    ;

    /**
     * 添加订单
     * @param order
     * @return
     */
    @PostMapping()
    public Result addOrder(@RequestBody Order order){
        log.info("订单添加信息："+order.toString());
        order.setOrderCreatetime(new Date());
        boolean flag = orderService.addOrder(order);
        return new Result(flag?Code.SAVE_OK:Code.SAVE_ERR,flag?"添加成功":"添加失败");

    };

    /**
     * 删除订单通过id
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result delOrderById(@PathVariable Integer id){
        log.info("删除的订单id："+id);
        boolean flag = orderService.delOrderById(id);
        return new Result(flag?Code.DELETE_OK:Code.DELETE_ERR,flag?"删除成功":"删除失败");
    };
}
