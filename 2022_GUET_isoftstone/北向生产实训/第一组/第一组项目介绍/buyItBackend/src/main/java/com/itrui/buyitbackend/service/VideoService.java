package com.itrui.buyitbackend.service;

import com.itrui.buyitbackend.pojo.Video;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface VideoService {
    /**
     * 获取全部视频
     * @return
     */
    public List<Video> getALlVideos();
}
