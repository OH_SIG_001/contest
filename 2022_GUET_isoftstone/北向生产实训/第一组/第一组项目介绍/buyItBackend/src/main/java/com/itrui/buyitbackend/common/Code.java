package com.itrui.buyitbackend.common;

public class Code {

    public static final Integer SAVE_OK = 2011;
    public static final Integer DELETE_OK = 2021;
    public static final Integer UPDATE_OK = 2031;
    public static final Integer GET_OK = 2041;

    public static final Integer SAVE_ERR = 2010;
    public static final Integer DELETE_ERR = 2020;
    public static final Integer UPDATE_ERR = 2030;
    public static final Integer GET_ERR = 2040;
}
