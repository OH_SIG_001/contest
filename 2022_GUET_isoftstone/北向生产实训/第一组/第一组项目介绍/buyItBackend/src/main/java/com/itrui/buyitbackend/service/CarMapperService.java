package com.itrui.buyitbackend.service;

import com.itrui.buyitbackend.pojo.Car;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface CarMapperService {
    /**
     * 查找用户购物车
     * @param account
     * @return
     */
    public List<Car> getAllCar(Integer account);

    /**
     * 删除用户购物车
     * @param id
     * @return
     */
    public boolean delCar(Integer id);


    /**
     * 添加购物车
     * @param car
     * @return
     */
    public boolean addCar(Car car);

    /**
     *
     * @param account
     * @return
     */
    public int getCarNum(Integer account);
}
