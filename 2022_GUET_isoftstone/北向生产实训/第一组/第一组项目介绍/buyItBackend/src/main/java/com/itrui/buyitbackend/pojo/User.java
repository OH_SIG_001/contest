package com.itrui.buyitbackend.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class User {

    private Integer     userId          ;  //   '用户id',
    private String      userName        ;  //  '用户名',
    private Integer     userAccount     ;  //  '用户账号',
    private String      userPassword        ;  //  '密码 ',
    private String      userPhone       ;  //  '联系方式',
    private String      userEmail       ;  //  '用户邮箱',
    private String      headPhoto       ;  //  '用户头像',
    private Date        userCreatetime  ;  //  '创建时间',
    private String      userAddress     ;  //  '收货地址',
    private String      userRole        ;  //  '角色',
    private Integer     userLevel       ;  //  '等级',
    private String      userIntroduce       ;  //  '用户简介',
    private Integer     userStatus      ;  //  '用户状态',
    private Float       money           ; //用户余额

}
