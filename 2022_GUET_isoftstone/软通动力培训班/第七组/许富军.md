# 心得体会
经过这次实训，我收获了很多，一方面学习到了许多以前没学过的专业知识与知识的应用，另一方面还提高了自我动手做项目的本事。


## 实训期待


期待实训可以锻炼我的阅读代码的能力和动手能力。


## 实训过程
第一天我们就开始建立环境，然后我较为顺利的完成了任务，就有信心完成接下来的培训任务。
第二天老师开始给我们复习C语言的一些基础知识，比如结构体、指针之类的。
第三天我们开始学习多任务管理和任务状态的相关知识。
第四天我们开始尝试烧录代码进开发板里，进行了呼吸灯、按键控制、OLED屏的显示等内容
第五天我们开始学习wifi协议，学习了TCP和UDP的相关内容
第六天我们学习TCP传输协议的多客户端连接和i2c总线的相关内容，并在开发板上进行了相关测试
第七天我们学习了使用开发板进行温湿度检测和马达开关控制
第八天我们学习使用马云网站（gitee网站）
第九天我们学习了如何将之前学习的马达、LED、温湿度检测进行联网，通过华为云进行LED和马达的控制。
第十天我们完成了小组和个人的答辩，圆满的完成了各项的培训任务。





## 未来展望
通过本次培训，我学习到了很多关于嵌入式开发、硬件开发的内容，学会了openharmony的使用，希望以后能够有更多的机会学习更多该方面的知识。






