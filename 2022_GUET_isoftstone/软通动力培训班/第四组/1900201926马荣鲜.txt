# 心得体会
两周的软通动力培训就要结束了，经过这两周的学习，我学到了很多，OpenHarmony开源有一个初步的认识。虽然第一天的环境搭建和软件安装差点把我给劝退了，但我还是坚持下来了，老师一步一步的引我们走往这个道路上。越往后面学得越难，我本就没这方面的天赋，但紧跟着老师的思路，我还是能勉强完成每一天的任务；期间遇到不懂的问题我会寻求老师和同学的帮助，
增强了自己的团队协作能力和沟通能力。这一次的培训，是我第一次参与开源项目，是一次有意义的经历，为我的未来积累了经验。


## 实训期待
希望能成为高校的一个课程，方便更多的同学参与其中。


记录你实训前对实训的期待
希望培训很有趣

## 实训过程
环境搭建->qemu模拟烧录->体验硬件烧录->GPIO->PWM->I2C->Wifi_Connect->UDP&TCP->IoT&mqtt. 实现物联网平台的smartfan_demo.


记录实训过程中的点点滴滴
培训中认识了新的同学，加强了自己团队协作和沟通能力。

## 未来展望
未来我估计不往这个方向走，有别的打算。






