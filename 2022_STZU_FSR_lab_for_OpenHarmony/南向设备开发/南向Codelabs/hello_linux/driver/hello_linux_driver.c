#include "hdf_device_desc.h"  // Header file that describes the APIs provided by the HDF to the driver.
#include "hdf_log.h"          // Header file that describes the log APIs provided by the HDF.
#include "gpio_if.h"    //添加GPIO驱动头文件
#define HDF_LOG_TAG hello_linux_driver   // Tag contained in logs. If no tag is not specified, the default HDF_TAG is used.

// The driver service struct definition
struct ITestDriverService {
    struct IDeviceIoService ioService;       // The first member of the service structure must be a member of type IDeviceIoService
};

// The driver service interface must be bound to the HDF for you to use the service capability.
static int32_t HdfHelloLinuxDriverBind(struct HdfDeviceObject *deviceObject) {
    HDF_LOGI("hello_linux driver bind success");
    return 0;
}

// Initialize the driver service.
static int32_t HdfHelloLinuxDriverInit(struct HdfDeviceObject *deviceObject) {
    HDF_LOGI("Hello Linux");
    GpioSetDir(41,GPIO_DIR_OUT);//引脚设置为输出
    GpioWrite(41,GPIO_VAL_HIGH);//输出高电平，红外灯亮
    return 0;
}

// Release the driver resources.
static void HdfHelloLinuxDriverRelease(struct HdfDeviceObject *deviceObject) {
    HDF_LOGI("hello_linux driver Release success");
    return;
}

// Define the object of the driver entry. The object must be a global variable of the HdfDriverEntry type (defined in hdf_device_desc.h).
struct HdfDriverEntry g_hello_linuxDriverEntry = {
    .moduleVersion = 1,
    .moduleName = "hello_linux_driver",
    .Bind = HdfHelloLinuxDriverBind,
    .Init = HdfHelloLinuxDriverInit,
    .Release = HdfHelloLinuxDriverRelease,
};

// Call HDF_INIT to register the driver entry with the HDF framework. When loading the driver, call the Bind function and then the Init function. If the Init function fails to be called, the HDF will call Release to release the driver resource and exit.
HDF_INIT(g_hello_linuxDriverEntry);
