
export default {
    data: {
        title: "",
        rain_y:[],
        font_size:16,
        columns:0,
        context:undefined,
        code_rain:undefined,
        color:["#FF72D9D2", "#FF386FDB", "#FFC93C6D", "#FFFF0000", "#FFB4814A"]
    },
    onInit() {
        //初始化字体大小
       this.font_size=16;
        //屏幕宽度/字体大小 = 列数 = 每行有多少滴雨
       this.columns = 1080/this.font_size;
        // y坐标全部初始化为1
        for(var i=0;i<this.columns;i++){
            this.rain_y[i]=1;
        }

    },
    onShow(){
        // 找到之前设定的画布组件
        this.code_rain = this.$element("code_rain");
        // 获取画布上下文
        this.context = this.code_rain.getContext("2d");
        // 绘制成黑色，(r,g,b,透明度)，透明度不能为1或者0，否则要么黑屏，要么数字重叠，无法体现出渐变效果。
        this.context.fillStyle="rgba(0,0,0,0.1)";
        // 绘制已填充的画布，每次都进行刷新覆盖
        this.context.fillRect(0,0,1080,2340);
        // 设置字体颜色，经典的绿色
        this.context.fillStyle="green";
        // 设置字体大小
        this.context.font=this.font_size+"px";
        var that = this;
        setInterval(()=>{
            // 绘制成黑色，(r,g,b,透明度)，透明度不能为1或者0，否则要么黑屏，要么数字重叠，无法体现出渐变效果。
            that.context.fillStyle="rgba(0,0,0,0.1)";
            // 绘制已填充的画布，每次都进行刷新覆盖
            that.context.fillRect(0,0,1080,2340);
            // 经典的绿色字体
            that.context.fillStyle=this.color[this.ranNum(0,4)];
            // 字体大小
            that.context.font=this.font_size+"px";
            //一行一行绘制
            for(var i=0;i<this.columns;i++){
                //绘制字体 (数字0或者1，数字的x坐标，数字的y坐标)
                //x坐标跟随列数索引增加
                that.context.fillText(Math.floor(Math.random()*2),i*that.font_size,that.rain_y[i]*that.font_size);
                //超出屏幕一半 就可以开始重新绘制了，加入random判断是为了不让所有数字都从同一起点出现，保证随机生成效果。
                if(that.rain_y[i]*that.font_size>(2340*0.5)&&Math.random()>0.85)
                //满足条件则从头来过
                that.rain_y[i]=0;
                //y坐标下移
                that.rain_y[i]++;
            }
        },50);
    },
    ranNum(minNum,maxNum){
        switch(arguments.length){
            case 1:
                return parseInt(Math.random()*minNum+1,10);
                break;
            case 2:
                return parseInt(Math.random()*(maxNum-minNum+1)+minNum,10);
                break;
            default:
                return 0;
                break;
        }
    },

}
