export default {
    data: {
        title: ""
    },
    onInit() {

        this.title = this.$t('strings.world');
    },
    onShow(){

    },
    test(){
        var matrix = this.$element("matrix");
        var context = matrix.getContext("2d");
        var color1 = context.createLinearGradient(1080/2,2340/2-10,1080/2,2340/2-150);
        color1.addColorStop(0, '#1E90FF');
				color1.addColorStop(.25, '#FF7F50');
				color1.addColorStop(.5, '#8A2BE2');
				color1.addColorStop(.75, '#4169E1');
				color1.addColorStop(1, '#00FFFF');

				var color2=context.createLinearGradient(0,0,1080,2340);
				color2.addColorStop(0, '#1E90FF');
				color2.addColorStop(.25, '#FFD700');
				color2.addColorStop(.5, '#8A2BE2');
				color2.addColorStop(.75, '#4169E1');
				color2.addColorStop(1, '#FF0000');

    }
}



