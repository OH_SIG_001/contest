package com.example.random;

import com.huawei.agconnect.AGConnectInstance;
import com.huawei.agconnect.AGConnectOptionsBuilder;
import ohos.aafwk.ability.AbilityPackage;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        in();
    }
    private void in(){
        //添加如下代码
        try {
            AGConnectOptionsBuilder builder = new AGConnectOptionsBuilder();
            ResourceManager resourceManager = getResourceManager();
            // agconnect-services.json 文件路径
            RawFileEntry rawFileEntry =  resourceManager.getRawFileEntry("resources/rawfile/agconnect-services.json");
            Resource resource = rawFileEntry.openRawFile();
            builder.setInputStream(resource );
            // 如果您的json文件中不存在client_id、client_secret和api_key参数，需通过以下接口设置
            builder.setClientId("865143442517281664");
            builder.setClientSecret("6CDCF90B48310A1A80DF1B818848133BF3AE1EA8A61BE09C628894B262E7FC93");
            builder.setApiKey("DAEDADSYJyrB0DwxGwAReXl8nyYuCugvsdM4iCRmgf+jZhm+vzyPf+ViiznkGCTSI51Tv9JzxVpQlTjJuddZnqu9VuQ3nymhk8M7MA==");
            AGConnectInstance.initialize(this, builder);
        } catch (Exception e) {
            e.printStackTrace();
        }
//TODO: 添加代码结束
    }
}
