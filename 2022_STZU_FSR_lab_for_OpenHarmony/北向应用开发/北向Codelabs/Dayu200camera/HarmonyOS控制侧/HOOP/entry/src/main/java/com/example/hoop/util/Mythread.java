package com.example.hoop.util;

import ohos.eventhandler.EventHandler;

import ohos.eventhandler.EventRunner;



public class Mythread {

    public static void inUI(Runnable runnable){
        //返回主线程
        EventRunner runner = EventRunner.getMainEventRunner();

        EventHandler eventHandler = new EventHandler(runner);

        eventHandler.postSyncTask(runnable);

    }

    public static void inBG(Runnable runnable){
        EventRunner runner = EventRunner.create(true);

        EventHandler eventHandler = new EventHandler(runner);
        //投递任务，我们的Socket函数应该投递于此。
        eventHandler.postTask(runnable, 0, EventHandler.Priority.IMMEDIATE);

    }

}