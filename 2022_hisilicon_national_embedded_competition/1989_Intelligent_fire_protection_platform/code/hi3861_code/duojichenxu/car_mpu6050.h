


#ifndef __CAR_MPU6050_H__
#define __CAR_MPU6050_H__

#include "car_config.h"

// // 3861接入端口设置 i2c设置
// hi_void Hi3861_Gpio_Init(hi_void);

// MPU6050 初始设置
hi_void Mpu6050_Init(hi_void);

// 返回整数数据 hi_u8 mpu6050_data[14] = {0} hi_u8 data_len = 14;
hi_void Mpu6050_Measure_By(hi_u8 *datas, hi_u8 len);

// 返回整数数据  hi_s16 datas[7] = {0};
hi_void Mpu6050_Measure_Sh(hi_s16 *datas);

// 返回浮点结果 hi_double *datas[7] = {0.0}; 
hi_void Mpu6050_Measure_Do(hi_double *datas);

// 加速度 在XY 平面的方向 和 大小 把字节数据代入获得
hi_void Mpu6050_ACC(hi_u8 *ds, hi_s32 *ac);


#endif