/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "sample_comm_nnie.h"
#include "sample_media_ai.h"
#include "ai_infer_process.h"
#include "gesture_detect.h"
#include "vgs_img.h"
#include "ive_img.h"
#include "misc_util.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

// #define HAND_FRM_WIDTH     416 //640  //  //640
// #define HAND_FRM_HEIGHT    416 //480  //  //384
#define DETECT_OBJ_MAX     25//32
//#define RET_NUM_MAX        4
#define DRAW_RETC_THICK    2    // Draw the width of the line

#define MODEL_FILE_HAND   "/userdata/data/nnie_model/detection/target.wk" 

static int biggestBoxIndex;
static IVE_IMAGE_S img;
static DetectObjInfo objs[DETECT_OBJ_MAX] = {0};
static RectBox boxs[DETECT_OBJ_MAX] = {0};
static RectBox objBoxs[DETECT_OBJ_MAX] = {0};
static RectBox remainingBoxs[DETECT_OBJ_MAX] = {0};
static RectBox cnnBoxs[DETECT_OBJ_MAX] = {0}; // Store the results of the classification network
// static RecogNumInfo numInfo[RET_NUM_MAX] = {0};
// static IVE_IMAGE_S imgIn;
// static IVE_IMAGE_S imgDst;
// static VIDEO_FRAME_INFO_S frmIn;
// static VIDEO_FRAME_INFO_S frmDst;
int uartFd = 0;
HI_S32 Yolo3HandDetectionLoad(uintptr_t* model)
{
    SAMPLE_SVP_NNIE_CFG_S *self = NULL;
    HI_S32 ret;
    ret = Yolo3Create(&self, MODEL_FILE_HAND);
    *model = ret < 0 ? 0 : (uintptr_t)self;
    SAMPLE_PRT("Yolo3FdLoad ret:%d\n", ret);
    
    /* uart open init */
    uartFd = UartOpenInit();
    if (uartFd < 0) printf("uart1 open failed\r\n");
    else printf("uart1 open successed\r\n");

    return ret;
}

HI_S32 Yolo3HandDetectionUnload(uintptr_t model)
{
    Yolo3Destory((SAMPLE_SVP_NNIE_CFG_S*)model);
    SAMPLE_PRT("Unload hand detect yolo3 model success\n");
    return 0;
}

HI_S32 HandDetect3Cal(uintptr_t model,IVE_IMAGE_S *srcYuv, DetectObjInfo resArr[])
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    int objNum;
    int ret = Yolo3CalImg(self, srcYuv, resArr, DETECT_OBJ_MAX, &objNum);
    if (ret < 0) {
        SAMPLE_PRT("Hand detect Yolo3CalImg FAIL, for cal FAIL, ret:%d\n", ret);
        return ret;
    }
    return objNum;
}


/* Get the maximum hand */
static HI_S32 GetBiggestHandIndex(RectBox boxs[], int detectNum)
{
    HI_S32 handIndex = 0;
    HI_S32 biggestBoxIndex = handIndex;
    HI_S32 biggestBoxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;
    HI_S32 biggestBoxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;
    HI_S32 biggestBoxArea = biggestBoxWidth * biggestBoxHeight;

    for (handIndex = 1; handIndex < detectNum; handIndex++) {
        HI_S32 boxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;
        HI_S32 boxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;
        HI_S32 boxArea = boxWidth * boxHeight;
        if (biggestBoxArea < boxArea) {
            biggestBoxArea = boxArea;
            biggestBoxIndex = handIndex;
        }
        biggestBoxWidth = boxs[biggestBoxIndex].xmax - boxs[biggestBoxIndex].xmin + 1;
        biggestBoxHeight = boxs[biggestBoxIndex].ymax - boxs[biggestBoxIndex].ymin + 1;
    }

    if ((biggestBoxWidth == 1) || (biggestBoxHeight == 1) || (detectNum == 0)) {
        biggestBoxIndex = -1;
    }

    return biggestBoxIndex;
}

/* hand gesture recognition info */
static void HandDetect3Flag(uint16_t cls)
{
    HI_CHAR *gestureName = NULL;
    UartSendRead(uartFd, 54321); // 字母a
    switch (cls) {
        case 0u:
            gestureName = "gesture a";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 1u:
            gestureName = "gesture b";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 2u:
            gestureName = "gesture c";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 3u:
            gestureName = "gesture d";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 4u:
            gestureName = "gesture e";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 5u:
            gestureName = "gesture f";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 6u:
            gestureName = "gesture g";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 7u:
            gestureName = "gesture h";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 8u:
            gestureName = "gesture i";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 9u:
            gestureName = "gesture k";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 10u:
            gestureName = "gesture l";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 11u:
            gestureName = "gesture m";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 12u:
            gestureName = "gesture n";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 13u:
            gestureName = "gesture o";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 14u:
            gestureName = "gesture p";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 15u:
            gestureName = "gesture q";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 16u:
            gestureName = "gesture r";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 17u:
            gestureName = "gesture s";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 18u:
            gestureName = "gesture t";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 19u:
            gestureName = "gesture u";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 20u:
            gestureName = "gesture v";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 21u:
            gestureName = "gesture w";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 22u:
            gestureName = "gesture x";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        case 23u:
            gestureName = "gesture y";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
        default:
            gestureName = "gesture others";
            SAMPLE_PRT("----gesture name----:%s\n", gestureName);
            break;
    }
    SAMPLE_PRT("hand gesture success\n");
}

HI_S32 Yolo3HandDetectionCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm)
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    HI_S32 resLen = 0;
    int objNum;
    int ret;
    //int num = 0;

    ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)dstFrm, &img);  
    SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "hand detect for YUV Frm to Img FAIL, ret=%#x\n", ret);

    objNum = HandDetect3Cal(model,&img, objs); // Send IMG to the detection net for reasoning

    SAMPLE_PRT("Draw rect to output frame start.\n");
    for (int i = 0; i < objNum; i++) {
        RectBox *box = &objs[i].box;
        RectBoxTran(box, HAND_FRM_WIDTH, HAND_FRM_HEIGHT,
            srcFrm->stVFrame.u32Width, srcFrm->stVFrame.u32Height);  //坐标转换
        SAMPLE_PRT("yolo3_out: {%d, %d, %d, %d}\n", box->xmin, box->ymin, box->xmax, box->ymax);
        boxs[i] = *box;
    }
    biggestBoxIndex = GetBiggestHandIndex(boxs, objNum);
    SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex, objNum);

    // When an object is detected, a rectangle is drawn in the SRCFRM
    if (biggestBoxIndex >= 0) {
        objBoxs[0] = boxs[biggestBoxIndex];
        SAMPLE_PRT("start MppFrmDrawRects\n");
        MppFrmDrawRects(srcFrm,objBoxs,1,RGB888_GREEN,DRAW_RETC_THICK); // Target hand objnum is equal to 1

        for (int j = 0,num=0; (j < objNum) && (objNum > 1); j++) {
            if (j != biggestBoxIndex) {
                SAMPLE_PRT("draw  remain  boxes %d\n",j);
                remainingBoxs[num++] = boxs[j];
            }
        }
        // others hand objnum is equal to objnum -1
        MppFrmDrawRects(srcFrm, remainingBoxs, objNum - 1, RGB888_RED, DRAW_RETC_THICK);
        SAMPLE_PRT("end MppFrmDrawRects\n");
        HandDetect3Flag(objs[biggestBoxIndex].cls);
    }
    SAMPLE_PRT("Draw rect to output frame over.\n");

    //IveImgDestroy(&img);  //好像加了这个destroy会出问题
    return ret;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
