// 尽量以 main.js 为主体 由主体与各节点js文件单向传递数据和获取数据
$(document).ready(function() {
  // 与python通信
  var ws;
  ws=new WebSocket("ws://localhost:9001/");
  var strChart="0";
  var mouseState="00"

  const $target = $('#target');
  const targetSize = $target.outerWidth();

  //adapt to changes in screen window size
  var left_body=$('body').width();
  var top_body=$('body').height();
  var changeX=(left_body-50-8)/6;
  var changeY=(top_body-31.5-50)/10;
  var changeY_P=changeY;
  var changeY_N=(-1)*changeY;
  var left="+="+changeX+"px";
  var top_P="+="+changeY+"px";
  var top_N="-="+changeY+"px";

  var figureX=33+changeX;
  var figureY=50+changeY;
  //change
  function Refresh(numX,numY){
    // 更新数据集
    figureX+=numX;
    figureY+=numY;

    var RosX=figureX/$('body').width();
    var RosY=figureY/$('body').height();
    // dataset.captureExample(figureX,figureY);
    // 此处是直接调用 调用函数之后会获取鼠标的位置 需要进行更改
    dataset.captureExample(RosX,RosY);
  };

  // 整个页面加载完毕后 十秒 进入自动训练模式
  setTimeout(function(){$('#start-automatic-training').prop('disabled', false);},5000);



  // 训练图案移动代码 具有自适应的特性 left top_N 分别代表着X、Y轴的递增数值
  // 实践发现识别屏幕左边的效果不如预测右边  可以适当地增加左边区域的训练图片数量
  $('#start-automatic-training').click(function(){
    for(var i=0;i<10;i++){
      $("#neha").show();
      $("#neha").animate({left:'8px',top:top_P},1000,function(){
          Refresh(0,changeY_P);
          // document.getElementById('testX').innerHTML=2;
      });
    }
    $("#neha").animate({left:left},1000,function(){
      Refresh(changeX,0);
    });
    for(var i=0;i<10;i++){
      $("#neha").animate({top:top_N},1000,function(){
          Refresh(0,changeY_N);
      });
    }
    $("#neha").animate({left:left},1000,function(){
      Refresh(changeX,0);
    })
    for(var i=0;i<10;i++){
      $("#neha").animate({top:top_P},1000,function(){
          Refresh(0,changeY_P);
      });
    }
    $("#neha").animate({left:left},1000,function(){
      Refresh(changeX,0);
    });
    for(var i=0;i<10;i++){
      $("#neha").animate({top:top_N},1000,function(){
          Refresh(0,changeY_N);
      });
    }
    $("#neha").animate({left:left},1000,function(){
      Refresh(changeX,0);
    });
    for(var i=0;i<10;i++){
      $("#neha").animate({top:top_P},1000,function(){
          Refresh(0,changeY_P);
      });
    }
    $("#neha").animate({left:left},1000,function(){
      Refresh(changeX,0);
    });
    for(var i=0;i<10;i++){
      $("#neha").animate({top:top_N},1000,function(){
          Refresh(0,changeY_N);
      });
    }
    $("#neha").animate({left:left},1000,function(){
      Refresh(changeX,0);
    });
    for(var i=0;i<10;i++){
      $("#neha").animate({top:top_P},1000,function(){
          Refresh(0,changeY_P);
      });
    }
    $("#neha").animate({top:top_P},"slow",function(){
      Refresh(0,changeY_P);
      var neha=document.getElementById("neha");//收集完之后隐藏此元素
      neha.style.display="none";
      var info_1=document.getElementById("info-1");
      info_1.style.display="none";
    });
  });




  function moveTarget() {
    // Move the model target to where we predict the user is looking to
    if (training.currentModel == null || training.inTraining) {
      return;
    }

    training.getPrediction().then(prediction => {
      // 这里是更新预测点的位置
      const left = prediction[0] * ($('body').width() - targetSize);
      const top = prediction[1] * ($('body').height() - targetSize);

      //修改如下  
      // onSubmit();
      // document.write(typeof left);
      
      //将浮点数先转换为整数 判断整数的长度 将两个数字拼接成8位长度的字符串 每个数字各占4位
      // 再加鼠标状态两位 00 无 01 单击
      var num_x=prediction[0] * ($('body').width() - targetSize);
      var Num_x=parseInt(num_x);
      var str_x=Num_x.toString();
      var num_y=prediction[1] * ($('body').height() - targetSize);
      var Num_y=parseInt(num_y);
      var srt_y=Num_y.toString();

      //转化长度
      var xLen=str_x.length,yLen=srt_y.length;
      for(var z=0;z<4-xLen;z++){
        str_x=strChart+str_x;
      }
      for(var z=0;z<4-yLen;z++){
        srt_y=strChart+srt_y;
      }
      //拼接字符串
      var output=str_x.concat(srt_y);
      output=output.concat(mouseState);
      ws.send(output);
      // 发送完数据后将 mouseState 函数值初始化
      mouseState="00";
      // 修改结束


      $target.css('left', left + 'px');
      $target.css('top', top + 'px');
    });
  }

  setInterval(moveTarget, 100);

  function download(content, fileName, contentType) {
    const a = document.createElement('a');
    const file = new Blob([content], {
      type: contentType,
    });
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
  }

  function phase2(){
    // 将提醒的 info 进行隐藏 
    // 将第一阶段隐藏的虚拟键盘进行显示
    var changedStyle=document.getElementById("pkp");
    changedStyle.style.display="";
    changedStyle=document.getElementById("grid-container-1");
    changedStyle.style.display="";
    changedStyle=document.getElementById("item1-1");
    changedStyle.style.display="";
    changedStyle=document.getElementById("screen");
    changedStyle.style.display="";
    changedStyle=document.getElementById("text");
    changedStyle.style.display="";
    changedStyle=document.getElementById("cursor");
    changedStyle.style.display="";
    changedStyle=document.getElementById("item2-2");
    changedStyle.style.display="";
    changedStyle=document.getElementById("001");
    changedStyle.style.display="";
    changedStyle=document.getElementById("item3-3");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("002");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("item4-4");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("003");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("item5-5");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("004");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("item6-6");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("005");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("item7-7");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("006");
    changedStyle.style.display="block";
    // changedStyle=document.getElementById("item8-8");
    // changedStyle.style.display="block";
    // changedStyle=document.getElementById("007");
    // changedStyle.style.display="block";
    // changedStyle=document.getElementById("item9-9");
    // changedStyle.style.display="block";
    // changedStyle=document.getElementById("008");
    // changedStyle.style.display="block";
    changedStyle=document.getElementById("keys");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("row-1");
    changedStyle.style.display="block";
	  changedStyle=document.getElementById("row-2");
    changedStyle.style.display="block";
	  changedStyle=document.getElementById("row-3");
    changedStyle.style.display="block";
	  changedStyle=document.getElementById("row-4");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("009");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("010");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("011");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("012");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("013");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("014");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("015");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("016");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("017");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("018");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("019");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("020");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("021");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("022");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("023");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("024");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("025");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("026");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("027");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("028");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("029");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("030");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("031");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("032");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("033");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("034");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("035");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("036");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("037");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("038");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("039");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("040");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("041");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("042");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("043");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("044");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("045");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("046");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("047");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("048");
    changedStyle.style.display="block";
    changedStyle=document.getElementById("049");
    changedStyle.style.display="block";


    changedStyle=document.getElementById("target");
    setTimeout(function(){
      changedStyle.style.opacity=1;
    },5000);
  }


  function phase3(){
    var changedStyle=document.getElementById("pkp");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("grid-container-1");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("item1-1");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("screen");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("text");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("cursor");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("item2-2");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("001");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("item3-3");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("002");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("item4-4");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("003");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("item5-5");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("004");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("item6-6");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("005");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("item7-7");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("006");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("item8-8");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("007");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("item9-9");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("008");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("keys");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("row-1");
    changedStyle.style.display="none";
	  changedStyle=document.getElementById("row-2");
    changedStyle.style.display="none";
	  changedStyle=document.getElementById("row-3");
    changedStyle.style.display="none";
	  changedStyle=document.getElementById("row-4");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("009");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("010");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("011");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("012");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("013");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("014");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("015");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("016");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("017");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("018");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("019");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("020");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("021");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("022");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("023");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("024");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("025");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("026");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("027");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("028");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("029");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("030");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("031");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("032");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("033");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("034");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("035");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("036");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("037");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("038");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("039");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("040");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("041");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("042");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("043");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("044");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("045");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("046");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("047");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("048");
    changedStyle.style.display="none";
    changedStyle=document.getElementById("049");
    changedStyle.style.display="none";

    // changedStyle=document.getElementById("Start-playmusic");
    // changedStyle.style.display='block';
    // changedStyle=document.getElementById("PlayRNN");
    // changedStyle.style.display='block';

    
    
    
  }

  // 绑定各个按钮的事件
  // 鼠标悬停3s触发点击事件
  
  var timer;
  function mouseOver(){
    timer=setTimeout(function(){
      mouseState="01";
      alert("hahahah");
    },300);
  };
  function mouseOut(){
    clearTimeout(timer);
  }


  // 点击按键 AI-music 进入第三阶段
  $('#005').click(function(){
    phase3();
  })



  // Map functions to keys and buttons:

  $('body').keyup(function(e) {
    // On space key:
    if (e.keyCode === 32 && ui.readyToCollect) {
      dataset.captureExample();
      e.preventDefault();
      return false;
    }
  });

  $('#start-training').click(function(e) {
    training.fitModel();
    // 进入第二阶段
    setTimeout(function(){phase2()},500);
  });

  $('#reset-model').click(function(e) {
    training.resetModel();
  });

  $('#draw-heatmap').click(function(e) {
    heatmap.drawHeatmap(dataset, training.currentModel);
  });

  $('#clear-heatmap').click(function(e) {
    heatmap.clearHeatmap();
  });

  $('#store-data').click(function(e) {
    const data = dataset.toJSON();
    const json = JSON.stringify(data);
    download(json, 'dataset.json', 'text/plain');
  });

  $('#load-data').click(function(e) {
    $('#data-uploader').trigger('click');
  });

  $('#data-uploader').change(function(e) {
    const file = e.target.files[0];
    const reader = new FileReader();

    reader.onload = function() {
      const data = reader.result;
      const json = JSON.parse(data);
      dataset.fromJSON(json);
    };

    reader.readAsBinaryString(file);
  });

  $('#store-model').click(async function(e) {
    await training.currentModel.save('downloads://model');
  });

  $('#load-model').click(function(e) {
    $('#model-uploader').trigger('click');
  });

  $('#model-uploader').change(async function(e) {
    const files = e.target.files;
    training.currentModel = await tf.loadLayersModel(
      tf.io.browserFiles([files[0], files[1]]),
    );
    ui.onFinishTraining();
  });


  
});
