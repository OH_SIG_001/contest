#define Gap_value 429.5
#define Standard 50
#define Weight_Minus 9014530
#define DT   2
#define SCK  8
#define KEY1 5
#define KEY2 7

struct  shopping_cart
{
    uint16_t num;
    float wet[20];
};

typedef struct {
    hi_u16 id;
    char *name;
    unsigned int num;
    float price;
    float weight;
    hi_u16 code[32];
}Item_Msg;

typedef struct {
    float weight;
    float price;
    unsigned int num;   //商品总数
    unsigned int type_num; //商品类别总数
    unsigned int id; //本次传递商品的调用号，item[id]
    Item_Msg item[20];
}Taurus_Msg;


#define AHT20_BAUDRATE (400 * 1000)
#define AHT20_I2C_IDX 0

//uint32_t hx711_read(void);//读取一次hx711数据
//uint32_t weight_average(void);//times平均
//uint32_t abs(uint32_t a,uint32_t b);//取绝对值
//uint16_t shopping_msg(float weight_now);