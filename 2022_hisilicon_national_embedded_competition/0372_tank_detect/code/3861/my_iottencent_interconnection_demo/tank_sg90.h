

#ifndef TANK_SG90_H__
#define TANK_SG90_H__


void engine_turn_forward(void);
void engine_turn_reverse(void);
void engine_stop(void);
void engine_turn_left(void);
void engine_middle(void);
void engine_turn_right(void);
#endif
