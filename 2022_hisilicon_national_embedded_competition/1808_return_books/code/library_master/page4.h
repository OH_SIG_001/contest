#ifndef PAGE4_H
#define PAGE4_H

#include <QWidget>
#include "bookdatabase.h"
#include <QLabel>
#include <QDebug>
#include <QPushButton>
#include <QPixmap>
#include <QStandardItemModel>
extern BookDatabase* bookdatabase;
extern QFont font1,font2,font3,font4;
namespace Ui {
class Page4;
}

class Page4 : public QWidget
{
    Q_OBJECT

public:
    explicit Page4(QWidget *parent = 0);
    ~Page4();
    void update();
private:
    Ui::Page4 *ui;
};

#endif // PAGE4_H
