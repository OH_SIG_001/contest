#include "page2.h"
#include "ui_page2.h"

Page2::Page2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Page2)
{
    ui->setupUi(this);
    ui->listWidget->setFont(font2);
    ui->pushButton_save->setFont(font2);
    ui->pushButton_save->setText("保存");
    ui->pushButton_clear->setFont(font2);
    ui->pushButton_clear->setText("清空");
    connect(ui->pushButton_save,&QPushButton::clicked,[=](){
       QString fileName = QFileDialog::getSaveFileName(this,"保存文件","e:/");
       QFile file(fileName);

       if (!file.open(QIODevice::WriteOnly|QIODevice::Truncate))
       {
           QMessageBox::critical(this, "critical", tr("文件保存失败！"),
                                 QMessageBox::Yes);
       }
       else
       {
           for(int i=0;i<ui->listWidget->count();i++)
           {
               QString arr = ui->listWidget->item(i)->text();
               arr.append("\r\n");
               file.write(arr.toUtf8());
           }
           file.close();
       }
    });
    connect(ui->pushButton_clear,&QPushButton::clicked,[=](){
        ui->listWidget->clear();
    });
}

Page2::~Page2()
{
    delete ui;
}
void Page2::setListWidget(QString message)
{
    qDebug()<<"done setListWidget";
    ui->listWidget->addItem(message);
}

