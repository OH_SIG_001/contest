/*Cocube Motion*/
/*Author MeitaoHuang*/
/*Date 2022.7.12*/

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_time.h"
#include "iot_pwm.h"
#include "hi_pwm.h"

#define GPIO0 0
#define GPIO1 1
#define GPIO9 9
#define GPIO10 10
#define GPIOFUNC 0
#define TASK_STAK_SIZE    (1024*10)

void pwm_init(void)
 
{
 
 //IoTGpioInit();
 
 //引脚复用
    hi_io_set_func(GPIO0, HI_IO_FUNC_GPIO_0_PWM3_OUT); //HI_IO_FUNC_GPIO_0_PWM3_OUT
 
    hi_io_set_func(GPIO1, HI_IO_FUNC_GPIO_1_PWM4_OUT);  
 
    hi_io_set_func(GPIO9, HI_IO_FUNC_GPIO_9_PWM0_OUT); 
 
    hi_io_set_func(GPIO10,HI_IO_FUNC_GPIO_10_PWM1_OUT); 
 //初始化pwm
 
    hi_pwm_init(HI_PWM_PORT_PWM3);
 
    hi_pwm_init(HI_PWM_PORT_PWM4);
 
    hi_pwm_init(HI_PWM_PORT_PWM0);
 
    hi_pwm_init(HI_PWM_PORT_PWM1);
 
}


void pwm_control(int l,int r)

{   l = l-255;
    r = r-255;
    
    printf("l=%d",l);
    
    printf("move now");

    hi_pwm_stop(HI_PWM_PORT_PWM3);
 
    hi_pwm_stop(HI_PWM_PORT_PWM4);
 
    hi_pwm_stop(HI_PWM_PORT_PWM0);
 
    hi_pwm_stop(HI_PWM_PORT_PWM1);

	if(l>=0){
          hi_pwm_start(HI_PWM_PORT_PWM3,l*10, 2550);
          }
        if(l<0){
          l = -l;
          hi_pwm_start(HI_PWM_PORT_PWM4,l*10, 2550);    
          }
        if(r>=0){
          hi_pwm_start(HI_PWM_PORT_PWM0,r*10, 2550);
          }
        if(r<0){
	  r = -r;
          hi_pwm_start(HI_PWM_PORT_PWM1,r*10, 2550);
          }
}	





















