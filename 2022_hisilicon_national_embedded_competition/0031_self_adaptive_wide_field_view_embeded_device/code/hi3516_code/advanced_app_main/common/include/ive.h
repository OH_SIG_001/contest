#include "hi_comm_vgs.h"
#include "hi_comm_video.h"
#include "hi_ive.h"

#define IVE_RECT_NUM        64

typedef struct hiSAMPLE_IVE_RECT_S 
{
    POINT_S astPoint[4];    //每一个里面有一个x、y坐标
} SAMPLE_IVE_RECT_S;


typedef struct hiSAMPLE_RECT_ARRAY_S {
    HI_U16 u16Num;
    SAMPLE_IVE_RECT_S astRect[IVE_RECT_NUM];
} SAMPLE_RECT_ARRAY_S;

int SAMPLE_IVE_KcfTracking(VIDEO_FRAME_INFO_S *pstFrmInfo, IVE_ROI_INFO_S *RoiInfo, HI_BOOL re_track, int *buffer);

int IVE_KCFInit();

void IVE_KCFDeInit();
