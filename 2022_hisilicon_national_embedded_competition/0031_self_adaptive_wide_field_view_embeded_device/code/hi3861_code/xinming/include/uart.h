#ifndef UART_H
#define UART_H

#define UART_BUFF_SIZE           32
#define DEMO_UART_NUM            HI_UART_IDX_1


#define BOARD_SELECT_IS_EXPANSION_BOARD
#ifdef BOARD_SELECT_IS_EXPANSION_BOARD
#define EXPANSION_BOARD
#else
#define ROBOT_BOARD
#endif

#include <hi_gpio.h>

hi_void UartTransmit(hi_void);

#endif