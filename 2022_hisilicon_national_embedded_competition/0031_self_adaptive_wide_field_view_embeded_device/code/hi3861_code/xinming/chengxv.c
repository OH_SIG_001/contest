#include <stdio.h>
#include "ohos_init.h"
#include "hi_io.h"
#include "hi_gpio.h"
#include "hi_errno.h"

#include "uart.h"
#include "servo.h"

#include "cmsis_os2.h"

void test(void)
{
    int ret = 0;

    ret = hi_gpio_init();
    if (ret != HI_ERR_SUCCESS)
    {
        printf("[ERROR] Gpio Init error!\n");
    }

    hi_io_set_func(HI_GPIO_SERVO, 0);

    ret = hi_gpio_set_dir(HI_GPIO_SERVO, HI_GPIO_DIR_OUT);
    if (ret != HI_ERR_SUCCESS)
    {
        printf("[ERROR] Gpio SetDir error!");
    }

    UartTransmit();
}

SYS_RUN(test);
