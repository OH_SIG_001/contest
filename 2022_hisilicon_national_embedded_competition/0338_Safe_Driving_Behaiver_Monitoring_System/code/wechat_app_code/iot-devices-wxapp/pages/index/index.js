// 获取应用实例
const app = getApp()
var mqtt = require('./mqtt.min.js') //根据自己存放的路径修改
// 下面为node引入方式，浏览器的话，使用对应的方式引入crypto-js库
// var mqtt = require('mqtt') //根据自己存放的路径修改
const crypto = require('./crypto-js.js')


Page({

  /**
   * 页面的初始数据
   */
  data: {
    switch1Checked: false,
    switch2Checked: true,
    client: {},
    weight: 68,
    heartrate: 92,
    spo2: 96,
    show: false,

    driver_time: 2.5,
    driver_reset: 2,

    over_driver_eye: 10,
    over_driver_notice: 3,

    Distraction_water: 1,
    Distraction_phone: 6,
    Distraction_cigrate: 4,
  },

  onDisplay() {
    this.setData({
      show: true
    });
  },
  onClose() {
    this.setData({
      show: false
    });
  },
  formatDate(date) {
    date = new Date(date);
    return `${date.getMonth() + 1}/${date.getDate()}`;
  },
  onConfirm(event) {
    this.setData({
      show: false,
      date: this.formatDate(event.detail),
    });
  },

  solarAndLunar(e) {
    this.setData({
      alltime: e.detail
    })
  },
   /**
   * switch样式点击事件
   */
  switch1Change: function (e){
    console.log(`Switch样式点击后是否选中：`, e.detail.value)
    if(e.detail.value){
      this.doConnect();
    }else{
      this.disconnect();
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(app.globalData.mqttUrl);

    //   this.data.client = mqtt.connect(app.globalData.mqttUrl, {
    //     'username': '12V6FHBXQIhi3861-wechat;12010126;e8499;1664985600',
    //     'password': 'ad37a77ba6c9da38c19ff93008f830e8a425fef9889febda4df5201ae0a74d53;hmacsha256',
    //     'ClientId': '12V6FHBXQIhi3861-wechat'
    //   })
    //   this.data.client.on('connect', function () {
    //     console.log('连接服务器成功')
    // })
    // this.doConnect();
    // this.data.client = connect('wxs://12V6FHBXQI.ap-guangzhou.iothub.tencentdevices.com')
  },
  /**
   * 断开连接
   */
  disconnect() {
    this.data.client.end()
    this.data.client = null
    wx.showToast({
      title: '成功断开连接'
    })
  },
  doConnect() {
    // 需要产品id，设备名和设备密钥,自己替换参数
    const productId = 'HOM8IRUD7A';
    const deviceName = 'wechat';
    const devicePsk = 's1lxr3TXRDfE3GprBJ3sng==';
    const topic = 'HOM8IRUD7A/wechat/data';

    // 1. 生成 connid 为一个随机字符串，方便后台定位问题
    const connid = Math.random().toString(36).substr(2);
    // 2. 生成过期时间，表示签名的过期时间,从纪元1970年1月1日 00:00:00 UTC 时间至今秒数的 UTF8 字符串
    const expiry = Math.round(new Date().getTime() / 1000) + 3600 * 24;
    // 3. 生成 MQTT 的 clientid 部分, 格式为 ${productid}${devicename}
    const clientId = productId + deviceName;
    // 4. 生成 MQTT 的 username 部分, 格式为 ${clientid};${sdkappid};${connid};${expiry}
    const userName = `${clientId};12010126;${connid};${expiry}`;
    //5.  对 username 进行签名，生成token、根据物联网通信平台规则生成 password 字段
    const rawKey = crypto.enc.Base64.parse(devicePsk); // 对设备密钥进行base64解码
    const token = crypto.HmacSHA256(userName, rawKey);
    const password = token.toString(crypto.enc.Hex) + ";hmacsha256";

    console.log(password);
    const options = {
      keepalive: 60, //60s
      clean: false, //cleanSession不保持持久会话
      protocolVersion: 4, //MQTT v3.1.1
      clientId: Math.random().toString(36).substr(2),
      username: userName,
      password: password
    };
    console.log(options)
    let url = `wxs:${productId}.ap-guangzhou.iothub.tencentdevices.com`;
    console.log(url);
    const that = this;
    this.data.client = mqtt.connect(url, options)
    console.log(options);
    this.data.client.on('connect', function () {
      console.log('连接服务器成功')
      //订阅消息
      if (that.data.client) {
        // 仅订阅单个主题
        that.data.client.subscribe(topic, function (err, granted) {
          if (!err) {
            wx.showToast({
              title: "订阅主题成功"
            });
          } else {
            wx.showToast({
              title: "订阅主题失败",
              icon: "fail",
              duration: 2000
            });
          }
        });
      } else {
        wx.showToast({
          title: "请先连接服务器",
          icon: "none",
          duration: 2000
        });
      }
      //接收消息监听
      that.data.client.on('message', function (topic, message) {
        // message is Buffer
        let msg1 = message.toString();
        console.log('收到消息：' + msg);

        var msg = JSON.parse(msg1);
        /**
         * 
         */
        console.log(msg);
        that.setData({

          over_driver_eye: msg.state.reported.MQ3,
          over_driver_notice: msg.state.reported.FSR,
          Distraction_water: msg.state.reported.drink,
          Distraction_phone: msg.state.reported.phone,
          Distraction_cigrate: msg.state.reported.smoke


        })
        //关闭连接 client.end()
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

})