# 目录结构

```
├─distraction_detect
├─histreaming_server
│  ├─include
│  ├─lib
│  └─src
└─model_train
    ├─backup_2
    └─cfg
```

# 简略说明

1. distraction_detect 分心检测
2. histreaming_server hi3516发送串口数据
3. model_train 模型的训练文件



效果

![image-20220713175029374](https://picture.nongchatea.xyz/images/2022/07/13/image-20220713175029374.png)

![image-20220713175714154](https://picture.nongchatea.xyz/images/2022/07/13/image-20220713175714154.png)





