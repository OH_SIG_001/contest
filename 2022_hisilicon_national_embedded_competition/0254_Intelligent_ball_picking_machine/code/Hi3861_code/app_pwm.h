#ifndef _APP_DEMO_PWM_H
#define _APP_DEMO_PWM_H

void car_gpioinit(void);

void car_backward(void);
void car_forward(void);
void car_left(void);
void car_right(void);
void car_stop(void);
void mode_pwmFreq(int location, int Freq)；

extern int LFreq;
extern int RFreq;

extern int CarStartFlag;
extern int AutoFlag;

#endif