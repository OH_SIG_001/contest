/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/prctl.h>
#include <time.h>

#include "sample_comm_nnie.h"
#include "sample_media_ai.h"
#include "ai_infer_process.h"
#include "yolov2_people_detect.h"
// #include "barcode_detect_decode.h"
#include "vgs_img.h"
#include "ive_img.h"
#include "misc_util.h"
#include "hisignalling.h"
#include "osd_img.h"//显示文字
#include "barcode_wrapper.h"
#include "audio_test.h"


#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#define PEOPLE_FRM_WIDTH     640
#define PEOPLE_FRM_HEIGHT    384
#define DETECT_OBJ_MAX     32
#define RET_NUM_MAX        4
#define DRAW_RETC_THICK    2    // Draw the width of the line
#define WIDTH_LIMIT        32
#define HEIGHT_LIMIT       32
#define IMAGE_WIDTH        224  // The resolution of the model IMAGE sent to the classification is 224*224
#define IMAGE_HEIGHT       224
#define MODEL_FILE_GESTURE    "/userdata/models/hand_classify/hand_gesture.wk" // darknet framework wk model
#define TXT_BEGX            20
#define TXT_BEGY            20
#define ARGB1555_RED        0xFC00 // 1 11111 00000 00000
#define ARGB1555_GREEN      0x83E0 // 1 00000 11111 00000
#define ARGB1555_BLUE       0x801F // 1 00000 00000 11111
#define ARGB1555_YELLOW     0xFFE0 // 1 11111 11111 00000
#define ARGB1555_YELLOW2    0xFF00 // 1 11111 11111 00000
#define ARGB1555_WHITE      0xFFFF // 1 11111 11111 11111
#define ARGB1555_BLACK      0x8000 // 1 00000 00000 00000


int PeopleFlag=0xaa;
int BarcodeFlag=0xff;

static int biggestBoxIndex;
static IVE_IMAGE_S img;
static DetectObjInfo objs[DETECT_OBJ_MAX] = {0};
static RectBox boxs[DETECT_OBJ_MAX] = {0};
static RectBox objBoxs[DETECT_OBJ_MAX] = {0};
static RectBox remainingBoxs[DETECT_OBJ_MAX] = {0};
static RectBox cnnBoxs[DETECT_OBJ_MAX] = {0}; // Store the results of the classification network
static RecogNumInfo numInfo[RET_NUM_MAX] = {0};
static IVE_IMAGE_S imgIn;
static IVE_IMAGE_S imgDst;
static VIDEO_FRAME_INFO_S frmIn;
static VIDEO_FRAME_INFO_S frmDst;

#pragma region audio 

#include "posix_help.h"

#define BUFFER_SIZE           16    // buffer size
#define AUDIO_FRAME        2      // Recognize once every 15 frames, can be configured by yourself
static int g_count = 0;
static HI_BOOL g_bAudioProcessStopSignal = HI_FALSE;
static pthread_t g_audioProcessThread = 0;
static int g_num = 108;

static SkPair g_stmChn = {
    .in = -1,
    .out = -1
};


static HI_VOID PlayAudio(const int num)
{
    // if  (g_count < AUDIO_FRAME) {
    //     g_count++;
    //     return;
    // }
    g_num=num;
    
    SAMPLE_PRT("------------------------------ Num Before AudioTest= %d ------------------------------\n",g_num);
   
    AudioTest(g_num, -1);
    // const RecogNumInfo *item = &items;
    // uint32_t score = item->score * MULTIPLE_OF_EXPANSION / SCORE_MAX;
    // if ((score > AUDIO_SCORE) && (g_num != item->num)) {
    //     g_num = item->num;
    //     if (g_num != UNKOWN_WASTE) {
    //         AudioTest(g_num, -1);///g_num=N就播放N.acc
    //     }
    // }
    g_count = 0;
}

static HI_VOID* GetAudioFileName(HI_VOID* arg)
{
    int num=-1;
    int ret;

    while (g_bAudioProcessStopSignal == false) {
        ret = FdReadMsg(g_stmChn.in, &num, sizeof(int));
        // SAMPLE_PRT("------------------------------ Num = %d ------------------------------\n",num);
        if (ret == sizeof(int)) {
            SAMPLE_PRT("------------------------------ Num Before PlayAudio= %d ------------------------------\n",num);
            PlayAudio(num);
        }
    }

    return NULL;
}

#pragma endregion

/* Get the maximum Box */
static HI_S32 GetBiggestBoxIndex(RectBox boxs[], int detectNum)
{
    HI_S32 boxIndex = 0;
    HI_S32 biggestBoxIndex = boxIndex;
    HI_S32 biggestBoxWidth = boxs[boxIndex].xmax - boxs[boxIndex].xmin + 1;
    HI_S32 biggestBoxHeight = boxs[boxIndex].ymax - boxs[boxIndex].ymin + 1;
    HI_S32 biggestBoxArea = biggestBoxWidth * biggestBoxHeight;

    for (boxIndex = 1; boxIndex < detectNum; boxIndex++) {
        HI_S32 boxWidth = boxs[boxIndex].xmax - boxs[boxIndex].xmin + 1;
        HI_S32 boxHeight = boxs[boxIndex].ymax - boxs[boxIndex].ymin + 1;
        HI_S32 boxArea = boxWidth * boxHeight;
        if (biggestBoxArea < boxArea) {
            biggestBoxArea = boxArea;
            biggestBoxIndex = boxIndex;
        }
        biggestBoxWidth = boxs[biggestBoxIndex].xmax - boxs[biggestBoxIndex].xmin + 1;
        biggestBoxHeight = boxs[biggestBoxIndex].ymax - boxs[biggestBoxIndex].ymin + 1;
    }

    if ((biggestBoxWidth == 1) || (biggestBoxHeight == 1) || (detectNum == 0)) {
        biggestBoxIndex = -1;
    }

    return biggestBoxIndex;
}

HI_S32 Yolo2MartPalLoad(uintptr_t* model)
{
    SAMPLE_SVP_NNIE_CFG_S *self = NULL;
    HI_S32 ret;
    HI_CHAR audioThreadName[BUFFER_SIZE] = {0};

    ret = CnnCreate(&self, MODEL_FILE_GESTURE);
    *model = ret < 0 ? 0 : (uintptr_t)self;
    BarcodeDetectInit(); // Initialize the Barcode detection model
    SAMPLE_PRT("Load Barcode detect claasify model success\n");

    PeopleDetectInit(); // Initialize the People detection model
    SAMPLE_PRT("Load People detect claasify model success\n");

    //创建音频线程
    if (GetCfgBool("audio_player:support_audio", true)) {
        ret = SkPairCreate(&g_stmChn);
        HI_ASSERT(ret == 0);
        if (snprintf_s(audioThreadName, BUFFER_SIZE, BUFFER_SIZE - 1, "AudioProcess") < 0) {
            HI_ASSERT(0);
        }
        prctl(PR_SET_NAME, (unsigned long)audioThreadName, 0, 0, 0);
        ret = pthread_create(&g_audioProcessThread, NULL, GetAudioFileName, NULL);
        if (ret != 0) {
            SAMPLE_PRT("audio proccess thread creat fail:%s\n", strerror(ret));
            return ret;
        }
    }
}

HI_S32 MartPalUnload(uintptr_t model)
{
    CnnDestroy((SAMPLE_SVP_NNIE_CFG_S*)model);

    PeopleDetectExit(); // Uninitialize the People detection model
    SAMPLE_PRT("Unload People detect claasify model success\n");

    BarcodeDetectExit(); // Uninitialize the People detection model
    SAMPLE_PRT("Unload Barcode detect claasify model success\n");

    if (GetCfgBool("audio_player:support_audio", true)) {
        SkPairDestroy(&g_stmChn);
        SAMPLE_PRT("SkPairDestroy success\n");
        g_bAudioProcessStopSignal = HI_TRUE;
        pthread_join(g_audioProcessThread, NULL);
        g_audioProcessThread = 0;
    }

    return 0;
}




HI_S32 Yolo2PeopleDetectResnetClassifyCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm,int uartFd0)
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    HI_S32 resLen = 0;
    int objNum;
    int ret;
    int num = 0;
    

    ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);
    SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "People detect for YUV Frm to Img FAIL, ret=%#x\n", ret);

    objNum = PeopleDetectCal(&img, objs); // Send IMG to the detection net for reasoning
    for (int i = 0; i < objNum; i++) {
        cnnBoxs[i] = objs[i].box;
        RectBox *box = &objs[i].box;
        RectBoxTran(box, PEOPLE_FRM_WIDTH, PEOPLE_FRM_HEIGHT,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
        SAMPLE_PRT("yolo2_out: {%d, %d, %d, %d}\n", box->xmin,  box->xmax, box->ymin, box->ymax);
        boxs[i] = *box;
    }
    biggestBoxIndex = GetBiggestBoxIndex(boxs, objNum);
    SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex, objNum);

    // When an object is detected, a rectangle is drawn in the DSTFRM
    if (biggestBoxIndex >= 0) {
        objBoxs[0] = boxs[biggestBoxIndex];
        MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_GREEN, DRAW_RETC_THICK); // Target people objnum is equal to 1

        for (int j = 0; (j < objNum) && (objNum > 1); j++) {
            if (j != biggestBoxIndex) {
                remainingBoxs[num++] = boxs[j];
                // others people objnum is equal to objnum -1
                MppFrmDrawRects(dstFrm, remainingBoxs, objNum - 1, RGB888_RED, DRAW_RETC_THICK);
            }
        }
        
        RectBox* box = &boxs[biggestBoxIndex];

        unsigned char coordinateBuffer[9];
        
        char* xmin = (char*)&box->xmin;
        char* xmax = (char*)&box->xmax;
        char* ymin = (char*)&box->ymin;
        char* ymax = (char*)&box->ymax;
        
        coordinateBuffer[0]=(char*)PeopleFlag;

        for (int i = 1; i < 3; i++)
        {
            coordinateBuffer[i] = xmin[i-1];
            coordinateBuffer[i + 2] = xmax[i-1];
            coordinateBuffer[i + 4] = ymin[i-1];
            coordinateBuffer[i + 6] = ymax[i-1];
        }
        #ifdef  EXPANSION_BOARD
            my_HisignallingMsgSend(uartFd0, coordinateBuffer, sizeof(coordinateBuffer) / sizeof(coordinateBuffer[0]));
            printf("send person coordinate\r\n");
        #endif
    }
    



    return ret;
}

int waitamount =20;
int decodesuccess=21;
int no_barcode=0;
int barcode=0;
bool has_decoded=false;
int opencv_cost_all=0;
int opencv_caled=0;
HI_S32 Yolo2BarcodeDetectResnetClassifyCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm,int uartFd0)
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    HI_S32 resLen = 0;
    int objNum;
    int ret;
    int num = 0;
    

    ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);
    SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "People detect for YUV Frm to Img FAIL, ret=%#x\n", ret);

    objNum = BarcodeDetectCal(&img, objs); // Send IMG to the detection net for reasoning
    for (int i = 0; i < objNum; i++) {
        cnnBoxs[i] = objs[i].box;
        RectBox *box = &objs[i].box;
        RectBoxTran(box, 1920, 1080,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
        SAMPLE_PRT("dstfrm width : %d   height : %d \n", dstFrm->stVFrame.u32Width,  dstFrm->stVFrame.u32Height);
        SAMPLE_PRT("yolo2_out: {%d, %d, %d, %d}\n", box->xmin,  box->xmax, box->ymin, box->ymax);
        boxs[i] = *box;
    }
    biggestBoxIndex = GetBiggestBoxIndex(boxs, objNum);
    SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex, objNum);


    SAMPLE_PRT("---------- no_barcode =%d ----------\n",no_barcode);
    SAMPLE_PRT("----------   barcode =%d  ----------\n",barcode);
    

    // When an object is detected, a rectangle is drawn in the DSTFRM
    if (biggestBoxIndex >= 0) {

        barcode++;

        objBoxs[0] = boxs[biggestBoxIndex];
        MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_GREEN, DRAW_RETC_THICK); // Target people objnum is equal to 1
        
        for (int j = 0; (j < objNum) && (objNum > 1); j++) {
            if (j != biggestBoxIndex) {
                remainingBoxs[num++] = boxs[j];
                // others people objnum is equal to objnum -1
                MppFrmDrawRects(dstFrm, remainingBoxs, objNum - 1, RGB888_RED, DRAW_RETC_THICK);
            }
        }
        struct tennis_detect * pbarcode;

        SAMPLE_PRT("---------- has_decoded=%d ----------\n",(int)has_decoded);

        unsigned char uartsendinfo[9];


        if (GetCfgBool("audio_player:support_audio", true))
        {
            if(no_barcode>=5&&barcode==1)//如果第一次检测到条码
            {
                
                    SAMPLE_PRT("------------------------------ IN Waitamount ------------------------------\n");
                    if (FdWriteMsg(g_stmChn.out, &waitamount, sizeof(waitamount)) != sizeof(waitamount)) 
                    {//发送消息到语音线程
                        SAMPLE_PRT("FdWriteMsg FAIL\n");
                    }
                    has_decoded=false;

                    for (int i = 0; i < 9; i++)
                    {
                        uartsendinfo[i] = (char*)BarcodeFlag;
                    }

                    #ifdef  EXPANSION_BOARD
                        my_HisignallingMsgSend(uartFd0, uartsendinfo, sizeof(uartsendinfo) / sizeof(uartsendinfo[0]));
                        printf("send barcode id successfully\r\n");
                    #endif
            }
        }
        
        
        no_barcode=0;

        uintptr_t modelb=1;
        long long int info=0;
        if(!has_decoded)
        {
            time_t t1=time(NULL);
            TennisDetectCal_C(pbarcode,modelb, dstFrm,&info);//, dstFrm);//条形码解码
            time_t t2=time(NULL);
            int time_cost=t2-t1;
            opencv_cost_all+=time_cost;
            opencv_caled++;
            SAMPLE_PRT("------------------------------- time_cost : %d -------------------------------\n", time_cost);
            SAMPLE_PRT("------------------------------- opencv_cost_all : %d -------------------------------\n", opencv_cost_all);
            SAMPLE_PRT("------------------------------- opencv_caled : %d -------------------------------\n", opencv_caled);
        }
           

        SAMPLE_PRT("-------- INFO : %lld --------\n",info);
        
        if(info>=6900000000000&&info<7000000000000)//如果条码解码成功
        {
            
            if (GetCfgBool("audio_player:support_audio", true))
            {
                SAMPLE_PRT("------------------------------ IN Decodesuccess ------------------------------\n");
                    
                if (FdWriteMsg(g_stmChn.out, &decodesuccess, sizeof(decodesuccess)) != sizeof(decodesuccess)) 
                {//发送消息到FdReadMsg
                    SAMPLE_PRT("FdWriteMsg FAIL\n");
                }
    
                SAMPLE_PRT("-------- INFO in if : %lld --------\n",info);

                
                
                char* pinfo = (char*)&info;
                
                uartsendinfo[0]=(char*)BarcodeFlag;

                for (int i = 1; i < 9; i++)
                {
                    uartsendinfo[i] = pinfo[8-i];
                }

                #ifdef  EXPANSION_BOARD
                    my_HisignallingMsgSend(uartFd0, uartsendinfo, sizeof(uartsendinfo) / sizeof(uartsendinfo[0]));
                    printf("send barcode id successfully\r\n");
                #endif

                has_decoded=true;
            
            }   
        }

             
    }
    else
    {
        no_barcode++;
        barcode=0;
    }
    



    return ret;
}


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
