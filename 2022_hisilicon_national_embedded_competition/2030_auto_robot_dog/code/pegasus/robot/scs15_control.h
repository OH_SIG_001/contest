/*
 * @Author: liu Shihao
 * @Date: 2022-06-12 20:54:47
 * @LastEditors: liu Shihao
 * @LastEditTime: 2022-06-13 19:22:38
 * @FilePath: \bearpi-hm_nano\applications\BearPi\BearPi-HM_Nano\sample\robot\include\scs15_control.h
 * @Description: 空闲中断
 * Copyright (c) 2022 by ${fzu} email: logic_fzu@outlook.com, All Rights Reserved.
 */

#ifndef __SCS15_CONTROL_H
#define __SCS15_CONTROL_H
#include <stdio.h>
#include <string.h>
#include "stdint.h"
extern void scs_uart2_init(void);
extern void SnycWrite(uint8_t ID[], uint8_t IDnumber,  int *nDat);

#endif  //__SCS15_CONTROL_H



