/*
 * @Author: liu Shihao
 * @Date: 2022-06-12 22:16:29
 * @LastEditors: liu Shihao
 * @LastEditTime: 2022-06-13 19:54:41
 * @FilePath: \bearpi-hm_nano\applications\BearPi\BearPi-HM_Nano\sample\robot\src\adc_voltage.c
 * @Description: 
 * Copyright (c) 2022 by ${fzu} email: logic_fzu@outlook.com, All Rights Reserved.
 */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include "hi_gpio.h"
#include "hi_io.h"
#include "hi_adc.h"

void adc_voltage_init(void)
{
    hi_io_set_func(HI_IO_NAME_GPIO_2, HI_IO_FUNC_GPIO_2_GPIO);
    hi_io_set_pull(HI_IO_NAME_GPIO_2,HI_IO_PULL_DOWN);
}

float voltage_get_data(void)
{
    unsigned int ret;
    unsigned short data;
   //ret = AdcRead(WIFI_IOT_ADC_CHANNEL_6, &data, WIFI_IOT_ADC_EQU_MODEL_8, WIFI_IOT_ADC_CUR_BAIS_DEFAULT, 0xff);
    ret = hi_adc_read(HI_ADC_CHANNEL_6, &data,HI_ADC_EQU_MODEL_8,HI_ADC_CUR_BAIS_DEFAULT, 0xff);
    if (ret != 0)
    {
        printf("ADC Read Fail\n");
    }
   float voltage= (float)data * 1.8 * 4 / 4096.0;
   voltage=voltage*6;
   
  // printf("voltage:%f\n",voltage);
   if (voltage<6)
   {
       hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_2,HI_GPIO_VALUE1);    
   }else
   {
       hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_2,HI_GPIO_VALUE0);
   }
    return voltage;
}





