/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <hi_stdlib.h>
#include <hi_uart.h>
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "iot_gpio_ex.h"
#include "iot_gpio.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "app_demo_uart.h"

#define LED_INTERVAL_TIME_US 300000
#define PWM1_OUT_GPIO 6
#define PWM2_OUT_GPIO 7
#define PWM_YAW 9
#define PWM_PITCH 10

int Cat_tag = 0;
int Dog_tag = 0;
int tag0 = 0;
int tag1 = 0;
unsigned long tag = 0;

UartDefConfig uartDefConfig = {0};

static void Uart1GpioCOnfig(void)
{


    IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);

}


static hi_void *UartDemoTask(char *param)
{
    hi_u8 uartBuff[UART_BUFF_SIZE] = {0};
    hi_unref_param(param);
    printf("Initialize uart demo successfully, please enter some datas via DEMO_UART_NUM port...\n");
    Uart1GpioCOnfig();//复用gpio为串口
    for (;;) {
        uartDefConfig.g_uartLen = IoTUartRead(DEMO_UART_NUM, uartBuff, UART_BUFF_SIZE);
        if ((uartDefConfig.g_uartLen > 0) && (uartBuff[0] == 0xaa) && (uartBuff[1] == 0x55)) {//读取数据长度大于0或者第一个数据为aa或第二个数据为55
            for (int i = 0; i < UART_BUFF_SIZE; i++) {
                printf("0x%x\n", uartBuff[i]);
            }
            printf("\r\n");
        }
        if(uartBuff[5] == 0x01&& Cat_tag == 0)
        {
            tag1 += 1;
            if(tag1 == 10)
            {
                
                Low_Pwm_CAT_open();
                hi_udelay(50000);
                Low_Pwm_CAT_Close();
            
                printf("now buf = %x",uartBuff[0]);
                uartBuff[5] = 0x00;
                Cat_tag += 1;
                tag1 = 0;
            }
        }
        if(uartBuff[5] == 0x2 && Dog_tag == 0)
        {
            tag0 += 1;
            if(tag0 == 20)
            {
                Low_Pwm_DOG_open();
                hi_udelay(50000);
                Low_Pwm_DOG_Close();
                uartBuff[5] = 0x00;
                Dog_tag += 1;
                tag0 = 0;
            }
        }
        
        tag += 1;
        
        if(tag == 0xfffff)
        {
            Cat_tag = 0;
            Dog_tag = 0;
        }
        // IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
        // usleep(LED_INTERVAL_TIME_US);
        // IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
        // usleep(LED_INTERVAL_TIME_US);
        // TaskMsleep(20); /* 20:sleep 20ms */

    }
    return HI_NULL;
}

/*
 * This demo simply shows how to read datas from UART2 port and then echo back.
 */
hi_void UartTransmit(hi_void)
{
    hi_u32 ret = 0;
    // IoTGpioInit(LED_TEST_GPIO);
    // IoTGpioSetDir(LED_TEST_GPIO, IOT_GPIO_DIR_OUT);
    // IoTGpioInit(PWM1_OUT_GPIO);
    IoSetFunc(PWM1_OUT_GPIO , 0); /* 将GPIO6定义为普通GPIO功能*/
    IoTGpioSetDir(PWM1_OUT_GPIO , IOT_GPIO_DIR_OUT); /* 设置GPIO6方向为输出*/
    IoTGpioInit(PWM2_OUT_GPIO);
    IoSetFunc(PWM2_OUT_GPIO , 0); /* 将GPIO7定义为普通GPIO功能*/
    IoTGpioSetDir(PWM2_OUT_GPIO , IOT_GPIO_DIR_OUT); /* 设置GPIO7方向为输出*/

    IoTGpioInit(PWM_YAW);
    IoSetFunc(PWM_YAW , 0); /* 将GPIO6定义为普通GPIO功能*/
    IoTGpioSetDir(PWM_YAW , IOT_GPIO_DIR_OUT); /* 设置GPIO6方向为输出*/
     IoTGpioInit(PWM_PITCH);
    IoSetFunc(PWM_PITCH , 0); /* 将GPIO6定义为普通GPIO功能*/
    IoTGpioSetDir(PWM_PITCH , IOT_GPIO_DIR_OUT); /* 设置GPIO6方向为输出*/

    IoTWatchDogDisable();
    IotUartAttribute uartAttr = {
        .baudRate = 115200, /* baudRate: 115200 */
        .dataBits = 8, /* dataBits: 8bits */
        .stopBits = 1, /* stop bit */
        .parity = 0,
    };
    /* Initialize uart driver */
    ret = IoTUartInit(DEMO_UART_NUM, &uartAttr);
    if (ret != HI_ERR_SUCCESS) {
        printf("Failed to init uart! Err code = %d\n", ret);
        return;
    }
    /* Create a task to handle uart communication */
    osThreadAttr_t attr = {0};
    attr.name = "uart demo";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024; 
    attr.priority = osPriorityNormal;
    if (osThreadNew((osThreadFunc_t)UartDemoTask, NULL, &attr) == NULL) {
        printf("Failed to create uart demo task!\n");
    }
}
SYS_RUN(UartTransmit);

void Low_Pwm_CAT_open(void)
{
    printf("pwm1 open");
    for(int i = 0;i <=50;i++)
    {
        IoTGpioSetOutputVal(PWM1_OUT_GPIO, 1);
        hi_udelay(2500);
        IoTGpioSetOutputVal(PWM1_OUT_GPIO, 0);
        hi_udelay(17500);
    }
}

void Low_Pwm_CAT_Close(void)
{
    printf("pwm1 close");
    for(int i = 0;i <=50;i++)
    {
        IoTGpioSetOutputVal(PWM1_OUT_GPIO, 1);
        hi_udelay(600);
        IoTGpioSetOutputVal(PWM1_OUT_GPIO, 0);
        hi_udelay(19400);
    }
}

void Low_Pwm_DOG_open(void)
{
    printf("pwm2 open");
    for(int i = 0;i <=50;i++)
    {
        IoTGpioSetOutputVal(PWM2_OUT_GPIO, 1);
        hi_udelay(650);
        IoTGpioSetOutputVal(PWM2_OUT_GPIO, 0);
        hi_udelay(19350);
    }
}

void Low_Pwm_DOG_Close(void)
{
    
    printf("pwm2 close");
    for(int i = 0;i <=20;i++)
    {
        IoTGpioSetOutputVal(PWM2_OUT_GPIO, 1);
        hi_udelay(2500);
        IoTGpioSetOutputVal(PWM2_OUT_GPIO, 0);
        hi_udelay(17500);
    }
}


