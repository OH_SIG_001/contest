# This is a sample Python script.
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

img_NameList = 'file_list.txt'
img_PicDstTraPath = 'tra'
img_PicDstValPath = 'val'
img_PicDstTstPath = 'tst'

# os 用于操作文件夹
import os
# shutil 用于文件/文件夹拷贝操作
import shutil
from os import listdir, getcwd
# 如果目标路径不存在 则创建
if not os.path.exists(img_PicDstTraPath):
    os.mkdir(img_PicDstTraPath)

if not os.path.exists(img_PicDstValPath):
    os.mkdir(img_PicDstValPath)

if not os.path.exists(img_PicDstTstPath):
    os.mkdir(img_PicDstTstPath)


def save_file(SrcDir,DstDir):
    shutil.copy(SrcDir, DstDir)

if __name__ == '__main__':
    # 获取当前路径
    wd = getcwd()
    jpgpath = img_PicDstValPath
    savepath = 'person_valid.txt'
    # 打开目标文件
    list_file = open(savepath, 'w')
    list = os.listdir(jpgpath)
    for i in range(0, len(list)):
        path = jpgpath + '/' + str(list[i])
        if ('.jpg' in path) or ('.jpeg' in path) or ('.png' in path):
            pathid = str(list[i])
            list_file.write('%s\\%s\%s\n' % (wd, jpgpath, pathid))





