# This is a sample Python script.
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
img_PicSrcPath = 'D:/baidunetdisk/train2017/train2017/'
img_NameList = 'file_list.txt'
img_PicDstTraPath = 'tra/'
img_PicDstValPath = 'val/'
img_PicDstTstPath = 'tst/'
countNum = 0
trainNum = 2000
valueNum = 2500
testNum = 3000
# os 用于操作文件夹
import os
# shutil 用于文件/文件夹拷贝操作
import shutil

# 如果目标路径不存在 则创建
if not os.path.exists(img_PicDstTraPath):
    os.mkdir(img_PicDstTraPath)

if not os.path.exists(img_PicDstValPath):
    os.mkdir(img_PicDstValPath)

if not os.path.exists(img_PicDstTstPath):
    os.mkdir(img_PicDstTstPath)


def save_images(SrcDir,DstDir):
    shutil.copy(SrcDir, DstDir)




if __name__ == '__main__':
    # print_hi('PyCharm')
    # 读取txt文档中的每一行
    fileList = open(img_NameList).readlines()
    # 遍历txt文档中的每一行
    for listName in  fileList:
        # 判定文件是否存在
        # src 为源文件的全路径
        src = img_PicSrcPath + listName.split('.')[0]+'.jpg'
        # 使用'.'分割字符串，取分割结果的0 也就是不带尾缀的文件名出来
        # listName.split('.')[0]
        if(os.path.exists(src)):
            countNum = countNum + 1
            if countNum >= 0 and countNum <= trainNum :
                save_images(src,img_PicDstTraPath)
            elif countNum >= trainNum and countNum <= valueNum :
                save_images(src, img_PicDstValPath)
            elif countNum >= valueNum and countNum <= testNum :
                save_images(src, img_PicDstTstPath)
            else:
                break




