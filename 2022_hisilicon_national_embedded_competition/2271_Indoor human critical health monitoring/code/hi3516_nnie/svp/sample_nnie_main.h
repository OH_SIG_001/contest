/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef SAMPLE_SVP_MAIN_H
#define SAMPLE_SVP_MAIN_H

#include "hi_type.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

void SAMPLE_SVP_NNIE_Cnn(void);

void SAMPLE_SVP_NNIE_Segnet(void);

void SAMPLE_SVP_NNIE_FasterRcnn(void);

void SAMPLE_SVP_NNIE_FasterRcnn_DoubleRoiPooling(void);

void SAMPLE_SVP_NNIE_Rfcn(void);

void SAMPLE_SVP_NNIE_Rfcn_File(void);

void SAMPLE_SVP_NNIE_Ssd(void);

void SAMPLE_SVP_NNIE_Yolov1(void);

void SAMPLE_SVP_NNIE_Yolov2(void);

void SAMPLE_SVP_NNIE_Yolov3(void);

void SAMPLE_SVP_NNIE_Lstm(void);

void SAMPLE_SVP_NNIE_Pvanet(void);

void SAMPLE_SVP_NNIE_Cnn_HandleSig(void);

void SAMPLE_SVP_NNIE_Segnet_HandleSig(void);

void SAMPLE_SVP_NNIE_FasterRcnn_HandleSig(void);

void SAMPLE_SVP_NNIE_Rfcn_HandleSig(void);

void SAMPLE_SVP_NNIE_Rfcn_HandleSig_File(void);

void SAMPLE_SVP_NNIE_Ssd_HandleSig(void);

void SAMPLE_SVP_NNIE_Yolov1_HandleSig(void);

void SAMPLE_SVP_NNIE_Yolov2_HandleSig(void);

void SAMPLE_SVP_NNIE_Yolov3_HandleSig(void);

void SAMPLE_SVP_NNIE_Lstm_HandleSig(void);

void SAMPLE_SVP_NNIE_Pvanet_HandleSig(void);
HI_S32 SAMPLE_SVP_NNIE_Yolov3_ParamInit(SAMPLE_SVP_NNIE_CFG_S *pstCfg, SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam,
    SAMPLE_SVP_NNIE_YOLOV3_SOFTWARE_PARAM_S *pstSoftWareParam);
HI_S32 SAMPLE_SVP_NNIE_Detection_PrintResult(SVP_BLOB_S *pstDstScore, SVP_BLOB_S *pstDstRoi,
    SVP_BLOB_S *pstClassRoiNum, HI_FLOAT f32PrintResultThresh);
HI_S32 SAMPLE_SVP_NNIE_Yolov3_Deinit(SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam,
SAMPLE_SVP_NNIE_YOLOV3_SOFTWARE_PARAM_S *pstSoftWareParam, SAMPLE_SVP_NNIE_MODEL_S *pstNnieModel);
#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* SAMPLE_SVP_MAIN_H */
