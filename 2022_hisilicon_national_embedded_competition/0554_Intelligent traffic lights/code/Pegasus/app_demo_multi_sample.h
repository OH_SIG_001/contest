/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APP_DEMO_MULTI_SAMPLE_H
#define APP_DEMO_MULTI_SAMPLE_H

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <hi_time.h>
#include <hi_watchdog.h>
#include <hi_io.h>
#include "ssd1306_oled.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "iot_errno.h"

#define KEY_INTERRUPT_PROTECT_TIME (40)
#define KEY_DOWN_INTERRUPT          (1)
#define GPIO_DEMO_TASK_STAK_SIZE    (1024*2)
#define GPIO_DEMO_TASK_PRIORITY     (25)

#define MAX_FUNCTION_DEMO_NUM       (4)
#define MAX_TRAFFIC_LIGHT_MODE      (2)

#define MAX_TRAFFIC_CONTROL_TYPE    (8)
#define MAX_TRAFFIC_AUTO_TYPE       (8)

#define OLED_FALG_ON                ((hi_u8)0x01)
#define OLED_FALG_OFF               ((hi_u8)0x00)

#define  KEY_GPIO_5                 (1)
#define  KEY_GPIO_7                 (2)

#define TRAFFIC_AUTO_MODE_TIME_COUNT            (3)

#define COUNT_NUM (3)

#define FACTORY_HISPARK_BOARD_TEST(fmt, ...) \
do { \
    printf(fmt, ##__VA_ARGS__); \
    for (hi_s32 i = 0; i < COUNT_NUM; i++) { \
        HisparkBoardTest(HI_GPIO_VALUE0); \
        hi_udelay(DELAY_250_MS); \
        HisparkBoardTest(HI_GPIO_VALUE1); \
        hi_udelay(DELAY_250_MS); \
    } \
} while (0)

typedef enum {
    HI_GPIO_0 = 0,
    HI_GPIO_1,
    HI_GPIO_2,
    HI_GPIO_3,
    HI_GPIO_4,
    HI_GPIO_5,
    HI_GPIO_6,
    HI_GPIO_7,
    HI_GPIO_8,
    HI_GPIO_9,
    HI_GPIO_10,
    HI_GPIO_11,
    HI_GPIO_12,
    HI_GPIO_13,
    HI_GPIO_14
} HiGpioNum;

typedef enum {
    HI_PWM0 = 0,
    HI_PWM1,
    HI_PWM2,
    HI_PWM3,
    HI_PWM_OUT = 5,
    HI_PWM_MAX
} HiPwmChannal;

typedef enum 
{
    CONTROL_MODE = 0,
} HiColorfulLightMode;

typedef enum 
{
    TRAFFIC_CONTROL_MODE = 0,
    TRAFFIC_AUTO_MODE,
    
} HiTrafficLightMode;

typedef enum 
{
    TRAFFIC_NORMAL_TYPE = 0,
    TRAFFIC_HUMAN_TYPE
} HiTrafficLightType;

typedef enum 
{
    STRA_NS_GREEN = 0,
    STRA_NS_YELLOW,
    STRA_EW_GREEN,
    STRA_EW_YELLOW,
} HiControlModeType;

typedef enum 
{
    // MENU_SELECT = 0,
    // MENU_MODE,
    CURRENT_MODE = 0,
    CURRENT_TYPE,
    // KEY_DOWN_FLAG,
}GloableStatuDef;

typedef struct 
{
    unsigned char g_menuSelect;
    unsigned char g_menuMode;
    unsigned char g_currentMode;
    unsigned char g_currentType;
    unsigned char g_keyDownFlag;
    unsigned int g_gpio5Tick;
    unsigned int g_gpio7Tick;
    unsigned int g_gpio8Tick;
    unsigned char g_gpio9Tick;
    unsigned int g_gpio7FirstKeyDwon;
    unsigned char g_gpio8CurrentType;
    unsigned char g_ocBeepStatus;
    unsigned char g_someoneWalkingFlag;
    unsigned char g_withLightFlag;
} GlobalStausType;

void HisparkBoardTest(IotGpioValue value);
void GpioControl(unsigned int gpio, unsigned int id, IotGpioDir dir, IotGpioValue gpioVal, unsigned char val);
void PwmInit(unsigned int id, unsigned char val, unsigned int port);
void ControlModeSample(void);
unsigned char SetKeyStatus(HiTrafficLightMode setSta);
unsigned char SetKeyType(HiControlModeType setSta);
unsigned char GetKeyStatus(GloableStatuDef staDef);
#endif