/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/prctl.h>

#include "sample_comm_nnie.h"
#include "sample_media_ai.h"
#include "ai_infer_process.h"
#include "yolov2_hand_detect.h"
#include "vgs_img.h"
#include "ive_img.h"
#include "misc_util.h"
#include "hisignalling.h"
#include "posix_help.h"
#include "hand_classify.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#define HAND_FRM_WIDTH     640
#define HAND_FRM_HEIGHT    384
#define DETECT_OBJ_MAX     32
#define RET_NUM_MAX        4
#define DRAW_RETC_THICK    4    // Draw the width of the line
// #define WIDTH_LIMIT        32
// #define HEIGHT_LIMIT       32
#define WIDTH_LIMIT        64
#define HEIGHT_LIMIT       64
#define IMAGE_WIDTH        224  // The resolution of the model IMAGE sent to the classification is 224*224
#define IMAGE_HEIGHT       224
#define MODEL_FILE_GESTURE    "/userdata/models/hand_classify/hand_gesture.wk" // darknet framework wk model
#define BUFFER_SIZE        16    // buffer size
#define MULTIPLE_OF_EXPANSION 100   // Multiple of expansion
#define UNKOWN_WASTE          20    // Unkown Waste
#define AUDIO_CASE_TWO     2
#define AUDIO_SCORE        40       // Confidence can be configured by yourself  置信度
#define AUDIO_FRAME        14       // Recognize once every 15 frames, can be configured by yourself
#define SCORE_MAX           4096    // The score corresponding to the maximum probability

#define NORM_BUF_SIZE_FACE 256  
/* ARGB1555 common colors */
#define ARGB1555_RED        0xFC00 // 1 11111 00000 00000
#define ARGB1555_GREEN      0x83E0 // 1 00000 11111 00000
#define ARGB1555_BLUE       0x801F // 1 00000 00000 11111
#define ARGB1555_YELLOW     0xFFE0 // 1 11111 11111 00000
#define ARGB1555_YELLOW2    0xFF00 // 1 11111 11111 00000
#define ARGB1555_WHITE      0xFFFF // 1 11111 11111 11111
#define ARGB1555_BLACK      0x8000 // 1 00000 00000 00000




static int biggestBoxIndex;
static IVE_IMAGE_S img;
static DetectObjInfo objs[DETECT_OBJ_MAX] = {0};
static RectBox boxs[DETECT_OBJ_MAX] = {0};
static RectBox objBoxs[DETECT_OBJ_MAX] = {0};
static RectBox remainingBoxs[DETECT_OBJ_MAX] = {0};
static RectBox cnnBoxs[DETECT_OBJ_MAX] = {0}; // Store the results of the classification network
static RecogNumInfo numInfo[RET_NUM_MAX] = {0};
static IVE_IMAGE_S imgIn;
static IVE_IMAGE_S imgDst;
static VIDEO_FRAME_INFO_S frmIn;
static VIDEO_FRAME_INFO_S frmDst;

static HI_BOOL g_bAudioProcessStopSignal = HI_FALSE;   //声音播放的标志位
static pthread_t g_audioProcessThread = 0;  //单独开启一个声音的线程

static pthread_t g_msgProcessThread = 0; //消息处理线程id

static int g_count = 0;
static int g_num = 108;


int uartFd_hand=0;

static SkPair g_stmChn = {
    .in = -1,
    .out = -1
};

HI_VOID* MsgReceiveHandler(HI_VOID* arg)
{
    unsigned char readBuff[16] = {0};

    SAMPLE_PRT("MsgReceiveHandler starting listening: \n");
    int receive_len = 0;
    while (g_bAudioProcessStopSignal == false) 
    {
        receive_len = UartRead(uartFd_hand, readBuff, 11, 100); /* 1000 :time out */ //溢出时间限制，后面可以设定
        // receive_len = HisignallingMsgReceive(uartFd_hand, readBuff, 11);
        printf("RECEIVED: \r\n");
        for (int i = 0; i < receive_len; i++) 
        {
            printf("0x%x ", readBuff[i]);
        }
        printf("\r\n");
        SAMPLE_PRT("[Audio Thread]: play /userdata/aac_file/%d.acc \n", readBuff[4]);
        if(receive_len>0)
        {
            
            switch(readBuff[4]) {
                case 0:
                    AudioTest(0, -1);
                    break;
                case 1:
                    AudioTest(1, -1);
                    break;
                case 2:
                    AudioTest(2, -1);
                    break;
                case 3:
                    AudioTest(3, -1);
                    break;
                case 4:
                    AudioTest(4, -1);
                    break;
                case 5:
                    AudioTest(5, -1);
                    break;
                default:
                    break;
            }
        }
    }

    return HI_NULL;
}

HI_S32 HandMsgTask(HI_VOID)
{
    HI_S32 s32Ret;
    HI_CHAR acThreadName[16] = {0};
    if (snprintf_s(acThreadName, BUFFER_SIZE, BUFFER_SIZE - 1, "MsgHandler") < 0) {
        HI_ASSERT(0);
    }
    prctl(PR_SET_NAME, (unsigned long)acThreadName, 0, 0, 0);
    s32Ret = pthread_create(&g_msgProcessThread, NULL, MsgReceiveHandler, NULL);

    return s32Ret;
}



HI_S32 Yolo2HandDetectResnetClassifyLoad(uintptr_t* model)
{
    SAMPLE_SVP_NNIE_CFG_S *self = NULL;
    HI_S32 ret;
    HI_CHAR audioThreadName[BUFFER_SIZE] = {0};

    ret = CnnCreate(&self, MODEL_FILE_GESTURE);
    *model = ret < 0 ? 0 : (uintptr_t)self;
    HandDetectInit(); // Initialize the hand detection model
    SAMPLE_PRT("Load hand detect claasify model success\n");

    /* uart open init */
    uartFd_hand = UartOpenInit();
    if (uartFd_hand < 0) {
        printf("uart1 open failed\r\n");  //返回值小于0，打开失败
    } else {
        printf("uart1 open successed\r\n");
    }
    
    // 创建socket对用于音频和识别两个线程之间通信，主要传递识别结果（类别，置信度），创建音频的线程
    if (GetCfgBool("audio_player:support_audio", true)) 
    {
        ret = SkPairCreate(&g_stmChn);

        ret = HandMsgTask();    //启动监听线程

        printf("MsgTaskHandler open successed\r\n");
        return ret;
    }

    return ret;
}

//
HI_S32 Yolo2HandDetectResnetClassifyUnload(uintptr_t model)
{
    CnnDestroy((SAMPLE_SVP_NNIE_CFG_S*)model);
    HandDetectExit(); // Uninitialize the hand detection model
    SAMPLE_PRT("Unload hand detect claasify model success\n");

    // //socket对摧毁，音频线程销毁
    if (GetCfgBool("audio_player:support_audio", true)) {
        SkPairDestroy(&g_stmChn);
        SAMPLE_PRT("SkPairDestroy success\n");
        //同时将音频停止标志位置为true
        g_bAudioProcessStopSignal = HI_TRUE;
        pthread_join(g_msgProcessThread, NULL);
        g_msgProcessThread = 0;
    }

    return 0;
}

/* Get the maximum hand */
static HI_S32 GetBiggestHandIndex(RectBox boxs[], int detectNum)
{
    HI_S32 handIndex = 0;
    HI_S32 biggestBoxIndex = handIndex;
    HI_S32 biggestBoxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;
    HI_S32 biggestBoxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;
    HI_S32 biggestBoxArea = biggestBoxWidth * biggestBoxHeight;

    for (handIndex = 1; handIndex < detectNum; handIndex++) {
        HI_S32 boxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;
        HI_S32 boxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;
        HI_S32 boxArea = boxWidth * boxHeight;
        if (biggestBoxArea < boxArea) {
            biggestBoxArea = boxArea;
            biggestBoxIndex = handIndex;
        }
        biggestBoxWidth = boxs[biggestBoxIndex].xmax - boxs[biggestBoxIndex].xmin + 1;
        biggestBoxHeight = boxs[biggestBoxIndex].ymax - boxs[biggestBoxIndex].ymin + 1;
    }

    if ((biggestBoxWidth == 1) || (biggestBoxHeight == 1) || (detectNum == 0)) {
        biggestBoxIndex = -1;
    }

    return biggestBoxIndex;
}

/* hand gesture recognition info */
//手势检测函数
static void HandDetectFlag(const RecogNumInfo resBuf)
{
    HI_CHAR *gestureName = NULL;
    
    switch (resBuf.num) {
        case 0u:
            gestureName = "01_normal";
            // 发送串口数据
            UartSendRead(uartFd_hand, Normal); // 正常表情
            SAMPLE_PRT("----face expression name----:%s\n", gestureName);
            break;
        case 1u:
            gestureName = "02_pain";
            UartSendRead(uartFd_hand, Pain); // 疼痛表情
            SAMPLE_PRT("----face expression----:%s\n", gestureName);
            break;
        default:
            gestureName = "face others";
            UartSendRead(uartFd_hand, UnknownExpression); // 无效值
            SAMPLE_PRT("----face expression----:%s\n", gestureName);
            break;
    }

    SAMPLE_PRT("face emotion classify success\n");
}

HI_S32 Yolo2HandDetectResnetClassifyCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm)
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    HI_S32 resLen = 0;
    int objNum;
    int ret;
    int num = 0;

    // 将视频帧转换为图片，传入scrFrm，通过img接收  yuv格式  img
    ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);
    SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "hand detect for YUV Frm to Img FAIL, ret=%#x\n", ret);

    // 先进行检测
    objNum = HandDetectCal(&img, objs); // Send IMG to the detection net for reasoning
    for (int i = 0; i < objNum; i++) {
        cnnBoxs[i] = objs[i].box;
        RectBox *box = &objs[i].box;
        //等比例缩放
        RectBoxTran(box, HAND_FRM_WIDTH, HAND_FRM_HEIGHT,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
        SAMPLE_PRT("yolo2_out: {%d, %d, %d, %d}\n", box->xmin, box->ymin, box->xmax, box->ymax);
        boxs[i] = *box;
    }
    biggestBoxIndex = GetBiggestHandIndex(boxs, objNum);
    SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex, objNum);

    // When an object is detected, a rectangle is drawn in the DSTFRM
    // 当检测到一个物体时，在DSTFRM中绘制一个矩形
    if (biggestBoxIndex >= 0) {
        objBoxs[0] = boxs[biggestBoxIndex];
        MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_GREEN, DRAW_RETC_THICK); // Target hand objnum is equal to 1

        for (int j = 0; (j < objNum) && (objNum > 1); j++) {
            if (j != biggestBoxIndex) {
                remainingBoxs[num++] = boxs[j];
                // others hand objnum is equal to objnum -1
                MppFrmDrawRects(dstFrm, remainingBoxs, objNum - 1, RGB888_RED, DRAW_RETC_THICK);
            }
        }

        // Crop the image to classification network
        // 将检测网检测到的目标进行crop，送入分类网络进行分类
        ret = ImgYuvCrop(&img, &imgIn, &cnnBoxs[biggestBoxIndex]);

        SAMPLE_PRT("crop u32Width = %d, img.u32Height = %d\n", imgIn.u32Width, imgIn.u32Height);
        SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "ImgYuvCrop FAIL, ret=%#x\n", ret);

        if ((imgIn.u32Width >= WIDTH_LIMIT) && (imgIn.u32Height >= HEIGHT_LIMIT)) {
            //resize只能对帧进行处理，因此先将图片转换为帧，再resize
            COMPRESS_MODE_E enCompressMode = srcFrm->stVFrame.enCompressMode;
            ret = OrigImgToFrm(&imgIn, &frmIn);
            frmIn.stVFrame.enCompressMode = enCompressMode;
            SAMPLE_PRT("crop u32Width = %d, img.u32Height = %d\n", imgIn.u32Width, imgIn.u32Height);
            SAMPLE_PRT("before resize u32Width = %d, img.u32Height = %d\n", frmIn.stVFrame.u32Width, frmIn.stVFrame.u32Height);
            ret = MppFrmResize(&frmIn, &frmDst, IMAGE_WIDTH, IMAGE_HEIGHT);
            SAMPLE_PRT("after resize u32Width = %d, img.u32Height = %d\n", frmDst.stVFrame.u32Width, frmDst.stVFrame.u32Height);
            //打印日志，看一下resize的尺寸
            // SAMPLE_PRT("resize u32Width = %d, img.u32Height = %d\n", IMAGE_WIDTH, IMAGE_HEIGHT);
            ret = FrmToOrigImg(&frmDst, &imgDst);
            
            ret = CnnCalU8c1Img(self,  &imgDst, numInfo, sizeof(numInfo) / sizeof((numInfo)[0]), &resLen);
            ///* Calculate a U8C1 image */U8c1 格式？？？
            SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "CnnCalU8c1Img FAIL, ret=%#x\n", ret);
            HI_ASSERT(resLen <= sizeof(numInfo) / sizeof(numInfo[0]));
            HandDetectFlag(numInfo[0]);
            MppFrmDestroy(&frmDst);
        }
        IveImgDestroy(&imgIn);
    }

    return ret;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */


