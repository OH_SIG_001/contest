#include <hi_stdlib.h>
#include <hi_uart.h>
#include <app_demo_uart.h>
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "iot_gpio_ex.h"
#include "ohos_init.h"
#include "cmsis_os2.h"

UartDefConfig uartDefConfig = {0};

#define VOICE_TIP  50

//全局变量
extern  int pain_num ;
extern  int normal_num ;
extern  int voice_tips_num ;

extern unsigned char WriteBuff2;
extern unsigned int  write_len;


//uart1 口的初始化
static void Uart1GpioCOnfig(void)
{
// #ifdef ROBOT_BOARD
//     IoSetFunc(HI_IO_NAME_GPIO_5, IOT_IO_FUNC_GPIO_5_UART1_RXD);
//     IoSetFunc(HI_IO_NAME_GPIO_6, IOT_IO_FUNC_GPIO_6_UART1_TXD);
//     /* IOT_BOARD */
// #elif defined (EXPANSION_BOARD)
    IoTGpioInit(HI_IO_NAME_GPIO_0);
    IoTGpioInit(HI_IO_NAME_GPIO_1);

    IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
// #endif
}


//相当于一个回调函数
//标志位，判断东西是否收，或者有没有错误
int SetUartRecvFlag(UartRecvDef def)
{
    if (def == UART_RECV_TRUE)  //接收的标志位
    {
        uartDefConfig.g_uartReceiveFlag = HI_TRUE;
    } else {
        uartDefConfig.g_uartReceiveFlag = HI_FALSE;
    }
    
    return uartDefConfig.g_uartReceiveFlag;
}

int GetUartConfig(UartDefType type)
{
    int receive = 0;

    switch (type) {
        case UART_RECEIVE_FLAG:
            receive = uartDefConfig.g_uartReceiveFlag;
            break;
        case UART_RECVIVE_LEN:
            receive = uartDefConfig.g_uartLen;
            break;
        default:
            break;
    }
    return receive;
}

void ResetUartReceiveMsg(void)
{
    (void)memset_s(uartDefConfig.g_receiveUartBuff, sizeof(uartDefConfig.g_receiveUartBuff),
        0x0, sizeof(uartDefConfig.g_receiveUartBuff));
}



//返回的是接收缓冲区的数据
unsigned char *GetUartReceiveMsg(void)
{
    return uartDefConfig.g_receiveUartBuff;
}


static hi_void *UartDemoTask(char *param)
{
    hi_u8 uartBuff[UART_BUFF_SIZE] = {0};
    // hi_unref_param(param);
    printf("Initialize uart successfully, please enter some datas via DEMO_UART_NUM port...\n");
    Uart1GpioCOnfig();

    //创建一个空的buf区，用来接收传递过来的数据
    unsigned char *readBuff = NULL; 
    int read_len =0;
    
    //一直检测串口发送与接收的消息
    while(1)
    {
        //一直监听串口发送过来的数据，最好是进行一个计数
        uartDefConfig.g_uartLen = IoTUartRead(DEMO_UART_NUM, uartBuff, 11);
        
        if (uartDefConfig.g_uartLen > 0) 
        {
            printf("start receive!  \n");
            (void)memcpy_s(uartDefConfig.g_receiveUartBuff, uartDefConfig.g_uartLen,
                    uartBuff, uartDefConfig.g_uartLen);
            readBuff=uartDefConfig.g_receiveUartBuff;  //获取read buf 里面的数据值
            read_len=uartDefConfig.g_uartLen;
            //将接收到的数据进行打印
            for (int i = 0; i < read_len; i++) 
            {
                printf("0x%x", readBuff[i]);  //打印接收的数据  十六进制
            }
            //对接收的数据进行判断，并进行相应的计数
            if(read_len>0)
            {
                if(readBuff[5]==2){ 
                    normal_num++; 
                    printf("face emotion classify:01_normal score:d%%%\n",readBuff[6]);
                }
                else if (readBuff[5]==3){
                    pain_num++;
                    voice_tips_num++;
                    printf("face emotion classify:02_pain score:d%%%\n",readBuff[6]);
                    printf("\n %d ",pain_num);
                    if(voice_tips_num==VOICE_TIP)
                    {
                        IoTUartWrite(DEMO_UART_NUM,WriteBuff2,write_len);
                        voice_tips_num=0;
                    }
                }
            }
            printf("\n");
            printf("%x",readBuff[5]);
            usleep(100); 
        }              
        
    }
    return HI_NULL;
}

/*
 * This demo simply shows how to read datas from UART2 port and then echo back.
 */
hi_void UartTransmit(hi_void)
{
    hi_u32 ret = 0;

    IotUartAttribute uartAttr = {
        .baudRate = 115200, /* baudRate: 115200 */
        .dataBits = 8, /* dataBits: 8bits */
        .stopBits = 1, /* stop bit */
        .parity = 0,
    };
    /* Initialize uart driver */
    //串口的初始化
    ret = IoTUartInit(DEMO_UART_NUM, &uartAttr);


    //创建线程去处理相关任务
    /* Create a task to handle uart communication */
    osThreadAttr_t attr = {0};
    attr.stack_size = UART_DEMO_TASK_STAK_SIZE;
    attr.priority = 24;
    attr.name = (hi_char*)"uart demo";
    if (osThreadNew((osThreadFunc_t)UartDemoTask, NULL, &attr) == NULL) 
    {
        printf("Falied to create uart demo task!\n");
    }
    else 
    {
        printf("success to create uart demo task!\n");
    }
}
// SYS_RUN(UartTransmit);