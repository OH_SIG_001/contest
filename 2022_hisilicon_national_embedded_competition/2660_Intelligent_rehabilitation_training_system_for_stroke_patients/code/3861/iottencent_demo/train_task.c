#include <hi_stdlib.h>
#include <hi_uart.h>
#include <app_demo_uart.h>
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "iot_gpio_ex.h"
#include "ohos_init.h"
#include "cmsis_os2.h"

//引脚以及相关参数设置
#define GPIO_train 9           //GPIO10
#define train_time 1500000     //3s
#define EXTRA_TIME  250000      //0.25s

#define stop_time 3000000 //x下落以及等待时间 3s


//发送的buf=
unsigned char WriteBuff0[11]={0xaa,0x55,0x0,0x2,0x0,0x0,0xff,0x11,0x22,0x33,0x44};
unsigned char WriteBuff1[11]={0xaa,0x55,0x0,0x2,0x1,0x1,0xff,0x11,0x22,0x33,0x44};
unsigned char WriteBuff2[11]={0xaa,0x55,0x0,0x2,0x2,0x2,0xff,0x11,0x22,0x33,0x44};
unsigned char WriteBuff3[11]={0xaa,0x55,0x0,0x2,0x3,0x3,0xff,0x11,0x22,0x33,0x44};
unsigned char WriteBuff4[11]={0xaa,0x55,0x0,0x2,0x4,0x4,0xff,0x11,0x22,0x33,0x44};
unsigned char WriteBuff5[11]={0xaa,0x55,0x0,0x2,0x5,0x5,0xff,0x11,0x22,0x33,0x44};
unsigned int  write_len     = sizeof(WriteBuff0)/sizeof(WriteBuff0[0]);



//全局变量
int pain_num=0;
int normal_num=0;
int voice_tips_num=0;
int old_user = 0;   //新老用户的全局变量
int enhance_time =0;

//获取反馈时间
static void get_enhance_time(void)
{
    printf("normal_num: %d",normal_num);
    printf("pain_num: %d",pain_num);
    printf("enhance_time: %d",enhance_time);
    if(pain_num==0)
    {enhance_time=200000;}
    else if((0<pain_num)&&(pain_num<=30))
    {enhance_time=500000;} //0.5s
    else if((30<pain_num)&&(pain_num<=60))  
    {enhance_time=350000;}
    else if((60<pain_num)&&(pain_num<=90))
    {enhance_time=200000;}
    else
    {enhance_time=10000;}
}


//基础训练模式
static void basic_train(void)
{
    
    printf("start basic train!\n");
    //等待语音提示信息结束
    usleep(3*1000*1000);  //1s
    //控制
    for(int i=0;i<3;i++)
    {
    IoTGpioSetOutputVal(GPIO_train, 1);
    usleep(train_time+old_user);
    IoTGpioSetOutputVal(GPIO_train, 0);
    usleep(stop_time);
    } 
    printf("end basic train!\n");
}

//加强训练模式    
//想法是定义一个全局变量  通过3516传过来的数据进行判断 
//改变usleep的值，从而改变
static void enhance_train(void)
{
    IoTUartWrite(DEMO_UART_NUM,WriteBuff3,write_len);
    printf("start enhance train!\n");
    //等待语音提示信息结束
    usleep(3*1000*1000);  //1s
    //控制  
    ///////最好加一个判断，对接收的消息做一个判断之类
    for(int i=0;i<3;i++)
    {
    get_enhance_time();
    IoTGpioSetOutputVal(GPIO_train, 1);
    usleep(train_time+EXTRA_TIME+enhance_time+old_user);
    IoTGpioSetOutputVal(GPIO_train, 0);
    usleep(stop_time);
    } 
    printf("start enhance train!\n");
}

//放松训练模式
static void relax_train(void)
{
    printf("start relax train!\n");
    IoTUartWrite(DEMO_UART_NUM,WriteBuff4,write_len);
    //等待语音提示信息结束
    usleep(3*1000*1000);  //1s
    //控制
    for(int i=0;i<3;i++)
    {
    IoTGpioSetOutputVal(GPIO_train, 1);
    usleep(train_time+old_user);
    IoTGpioSetOutputVal(GPIO_train, 0);
    usleep(stop_time);
    } 
    printf("end relax train!\n");
}


//实际的康复训练过程
static void *TrainDemo(const char *arg)
{
    (void)arg;
    // UartTransmit();
    while(1)
    {
        printf("initializing start\r\n");
        usleep(4000*1000);
        IoTUartWrite(DEMO_UART_NUM,WriteBuff0,write_len);
        printf("write voice0 finish\n");
        usleep(6*1000*1000);  //6s
        IoTUartWrite(DEMO_UART_NUM,WriteBuff1,write_len);
        //基础训练
        basic_train();
        //加强训练
        enhance_train();
        //放松训练
        relax_train();
        IoTUartWrite(DEMO_UART_NUM,WriteBuff5,write_len);
        usleep(4000*1000);
    }
    return NULL;
}

 void TrainTask(void)
{
    osThreadAttr_t attr;  //单独开启一个线程，创建按一个任务
    //引脚初始化
    IoTGpioInit(GPIO_train);
    IoTGpioSetDir(GPIO_train, 1);
    IoTWatchDogDisable();

    attr.name = "TrainDemo";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096; /* 堆栈大小为1024 */
    attr.priority = 28;   // 线程，或者是任务的优先级，越小表示优先级越高最小是24或者是25

    if (osThreadNew((osThreadFunc_t)TrainDemo, NULL, &attr) == NULL) 
    {
        printf("Falied to create TrainTask!\n");//报错的话是直接在串口中进行打印的
    }
    else
    {
        printf("success to create TrainTask!\n");

    }
}


// SYS_RUN(TrainTask);  //主程序的入口
// //注册一个在启动阶段会执行的函数






