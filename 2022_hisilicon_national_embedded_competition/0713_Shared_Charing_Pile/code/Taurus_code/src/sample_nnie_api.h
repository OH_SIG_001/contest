
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include <cstring>
#ifdef __cplusplus
extern "C" {
#endif

using namespace std;

int yolo_init(const char *yolo_model_path);
int yolo_run(cv::Mat img, int input_w, int input_h,string &forStore,int* StoreID,int &countID);
int yolo_unit();
int yolo_run_yuv(unsigned char *input_yuv420_data, int input_w,int input_h, string& forStore,int* StoreID,\
		int &countID);


#ifdef __cplusplus
}
#endif //__cplusplus

//int yolo_run(unsigned char *input_yuv420_data, int input_w, int input_h);
//int yolo_run(cv::Mat img, int input_w, int input_h);
//int yolo_run(char* filepath, int input_w, int input_h);
