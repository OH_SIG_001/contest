/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OLOED_FONTS_H
#define OLOED_FONTS_H


/***************************16*16的点阵字体取模方式：共阴——列行式——逆向输出*********/
static const unsigned char g_f16x16[] =
{

	0x00,0x08,0x08,0xE8,0xE8,0x28,0x28,0x2E,0x2C,0x28,0x28,0xE8,0xE8,0x08,0x08,0x00,
    0x00,0x00,0x20,0x33,0x1F,0x0A,0xC2,0x42,0x7E,0x02,0x0A,0x1B,0x13,0x20,0x20,0x00,/*"京",0*/

	0x00,0x60,0x44,0x98,0x28,0x20,0xA8,0xA8,0xA8,0xFE,0xA8,0xA8,0xF8,0xF8,0x20,0x00,
	0x00,0x60,0x38,0x06,0x10,0x14,0x14,0x14,0x14,0xFF,0x14,0x14,0x14,0x14,0x10,0x00,/*"津",1*/

	0x00,0x14,0x14,0xD4,0xD4,0xD4,0xDE,0xC0,0xC0,0xDE,0xD4,0xD4,0xD4,0x1C,0x10,0x00,
	0x00,0x10,0x94,0x57,0x56,0x7E,0x16,0x17,0x17,0x1E,0x7E,0x56,0x57,0xD4,0x10,0x00,/*"冀",2*/

	0x00,0xC0,0xC6,0xDE,0xF6,0xC6,0xFE,0xC6,0xC6,0xFE,0xC6,0xF6,0xDE,0xC6,0xC0,0x00,
	0x00,0x00,0x00,0xFE,0x4A,0x4A,0x4A,0x4A,0x4A,0x4A,0x4A,0x4A,0xFE,0x00,0x00,0x00,/*"晋",3*/

	0x00,0x74,0x74,0x14,0x54,0x5E,0x54,0x54,0x54,0x5E,0x5E,0x54,0x14,0x74,0x74,0x00,
	0x00,0x10,0xF5,0x55,0x55,0x2B,0x2B,0x57,0xDD,0x79,0x05,0x1D,0x33,0x61,0x40,0x00,/*"蒙",4*/

	0x00,0x40,0x46,0xCC,0x00,0x00,0x04,0x04,0x04,0x04,0xE4,0x14,0x1C,0x0C,0x04,0x00,
	0x00,0x40,0x20,0x1F,0x20,0x20,0x40,0x40,0x58,0x58,0x4F,0x40,0x40,0x40,0x40,0x00,/*"辽",5*/

	0x00,0x18,0x98,0x98,0x98,0x98,0x98,0xFE,0xFE,0x98,0x98,0x98,0x98,0x98,0x18,0x00,
	0x00,0x00,0x00,0xFE,0x22,0x22,0x22,0x22,0x22,0x22,0x22,0x22,0x7E,0x00,0x00,0x00,/*"吉",6*/

	0x00,0x00,0x7E,0x42,0x4A,0x7A,0x62,0xFE,0xFE,0x62,0x7A,0x4A,0x42,0x7E,0x00,0x00,
	0x00,0x48,0x79,0x09,0x09,0x29,0x69,0x0F,0x0F,0x79,0x49,0x09,0x09,0x79,0x48,0x00,/*"黑",7*/

	0x00,0x60,0x46,0xCC,0x08,0x00,0x00,0xF0,0x10,0x1A,0x16,0x10,0x10,0xF0,0xF0,0x00,
	0x00,0x40,0x70,0x1C,0x06,0xC0,0x38,0x0F,0x01,0x01,0x01,0x01,0x01,0x03,0x03,0x00,/*"沪",8*/

	0x00,0x08,0x88,0x88,0xBE,0xBE,0x88,0xE8,0x88,0x88,0xBE,0x88,0x08,0x08,0x08,0x00,
	0x00,0x10,0x4C,0x46,0x20,0x38,0x0E,0x03,0x40,0x40,0x78,0x3F,0x02,0x06,0x08,0x00,/*"苏",9*/

	0x00,0x44,0xCC,0x98,0x20,0x20,0xFE,0xFE,0x20,0x00,0xFC,0x44,0x44,0xC4,0x46,0x00,
	0x00,0x40,0x78,0x0D,0x02,0x42,0x7F,0x3F,0x41,0x78,0x0F,0x00,0x00,0x7F,0x00,0x00,/*"浙",10*/

	0x00,0xF8,0x1C,0x1E,0xF8,0x08,0x38,0x48,0x48,0x4A,0x4E,0x48,0x48,0x18,0x18,0x00,
	0x00,0x7F,0x11,0x11,0x1F,0x00,0x41,0x61,0x3F,0x01,0x01,0x7F,0x41,0x41,0x61,0x00,/*"皖",11*/

	0x00,0x00,0xF0,0x0C,0xE0,0xE0,0x24,0x24,0xF4,0x24,0xE4,0xE4,0x04,0xFC,0xFC,0x00,
	0x00,0x80,0xFF,0x00,0x17,0x17,0x12,0x12,0x1F,0x12,0x1F,0x1B,0xE0,0x7F,0x3F,0x00,/*"闽",12*/

	0x00,0xA4,0xB4,0xA6,0xA4,0xB4,0xAC,0x50,0x58,0xAE,0xB6,0xB4,0xBC,0x24,0x20,0x00,
	0x10,0x17,0x16,0xFE,0xFE,0x17,0x10,0xC1,0x5D,0x45,0x3D,0x15,0x65,0x5D,0xC1,0x00,/*"赣",13*/

	0x00,0x20,0x10,0xF8,0xDC,0xD6,0xD6,0xF4,0xF4,0xDC,0xD4,0xD0,0xF0,0x00,0x00,0x00,
	0x00,0x02,0x02,0xFE,0x56,0x56,0x56,0x56,0x56,0x56,0x56,0x56,0xFE,0x02,0x02,0x00,/*"鲁",14*/

	0x00,0x84,0x94,0xE4,0x9C,0x8C,0x84,0x18,0xFC,0x96,0xD4,0xB4,0x9C,0x90,0xF0,0x00,
	0x00,0x40,0xC0,0x7F,0x00,0x03,0x54,0x6A,0x2B,0x55,0x5B,0x7C,0x06,0x1B,0x60,0x00,/*"豫",15*/

	0x00,0x5E,0x52,0x5E,0x40,0x5E,0x5E,0x52,0x5E,0x00,0xFE,0x02,0x42,0xFE,0x06,0x00,
	0x00,0x01,0x01,0x0F,0x09,0x49,0x49,0x79,0x01,0x01,0xFF,0x00,0x10,0x11,0x1F,0x00,/*"鄂",16*/

	0x00,0x64,0xC4,0x18,0x20,0xA0,0xFE,0x20,0x20,0x00,0xFC,0x44,0x44,0x44,0xFC,0x00,
	0x00,0x70,0x1E,0x10,0x1C,0x03,0xFF,0x01,0x03,0x00,0xFF,0x22,0x22,0x22,0x7F,0x00,/*"湘",17*/

	0x00,0x00,0x00,0xFC,0xD4,0xDC,0xB6,0x9E,0xFC,0xB4,0xDC,0x84,0xFC,0x00,0x00,0x00,
	0x00,0x02,0x02,0x02,0x0E,0x0E,0x0A,0x0A,0x4A,0x4A,0x4A,0x7A,0x0A,0x02,0x02,0x00,/*"粤",18*/

	0x00,0x30,0xB0,0xFE,0x32,0xF0,0x80,0x88,0x88,0x88,0xFE,0x88,0x88,0x88,0x80,0x00,
	0x00,0x0C,0x03,0xFF,0x81,0x03,0x20,0x24,0x24,0x24,0x3F,0x24,0x24,0x24,0x20,0x00,/*"桂",19*/

	0x00,0x88,0x88,0xF8,0x88,0x88,0x08,0xC8,0x48,0x4A,0x4E,0x48,0x48,0xC8,0x08,0x00,
	0x00,0x30,0x10,0x1F,0x08,0x48,0x60,0x1B,0x02,0xC2,0x7E,0x02,0x02,0x1B,0x60,0x00,/*"琼",20*/

	0x00,0x40,0xC4,0x8C,0x48,0xE0,0xB0,0xB8,0xAC,0xA6,0x2C,0xB8,0x10,0xA0,0x20,0x00,
	0x00,0x40,0x70,0x0C,0x00,0x7F,0x7F,0x4A,0x7F,0x7F,0x00,0x1F,0x40,0x7F,0x00,0x00,/*"渝",21*/

	0x00,0x00,0x00,0xFC,0xFC,0x00,0x00,0x00,0xF8,0x00,0x00,0x00,0xFE,0xFE,0x00,0x00,
	0x00,0x40,0x70,0x1F,0x01,0x00,0x00,0x00,0x3F,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,/*"川",22*/

	0x00,0x80,0x80,0xBC,0xA4,0xA4,0xA4,0xFE,0xFE,0xA4,0xA4,0xA4,0xBC,0xA0,0x80,0x00,
	0x00,0x40,0xC0,0x5E,0x42,0x62,0x22,0x1E,0x06,0x12,0x22,0x22,0x6E,0x40,0x00,0x00,/*"贵",23*/

	0x00,0xC0,0xC0,0xC4,0xC4,0xC4,0xC4,0xC4,0xC4,0xC4,0xC4,0xC4,0xC4,0xC0,0xC0,0x00,
	0x00,0x00,0x20,0x70,0x78,0x2C,0x23,0x21,0x20,0x20,0x2C,0x38,0x30,0x60,0x00,0x00,/*"云",24*/

	0x00,0xE4,0x84,0x84,0xE4,0x2E,0xAE,0xA4,0xA4,0xAE,0x34,0xF4,0x34,0xBC,0x24,0x00,
	0x00,0x76,0x8E,0xF6,0x1F,0x00,0x3F,0x2A,0x3B,0xEE,0x60,0x3F,0x38,0x67,0x60,0x00,/*"藏",25*/

	0x00,0xFC,0xFC,0x64,0xBC,0x04,0x08,0xE8,0x08,0xFE,0xFE,0x88,0xE8,0x28,0x00,0x00,
	0x00,0x7F,0x7F,0x08,0x4D,0x47,0x61,0x31,0x19,0x0F,0x07,0x19,0x31,0x61,0x41,0x00,/*"陕",26*/

	0x00,0x20,0x20,0x20,0xFE,0x20,0x20,0x20,0x20,0x20,0x20,0xFE,0x20,0x20,0x20,0x00,
	0x00,0x00,0x00,0x00,0x7F,0x22,0x22,0x22,0x22,0x22,0x22,0x7F,0x00,0x00,0x00,0x00,/*"甘",27*/

	0x00,0x40,0x44,0x54,0x54,0x54,0x54,0x7E,0x7E,0x54,0x54,0x54,0x54,0x44,0x40,0x00,
	0x00,0x00,0x00,0xFF,0x15,0x15,0x15,0x15,0x15,0x15,0x55,0x55,0x7F,0x00,0x00,0x00,/*"青",28*/

	0x00,0xB8,0xB8,0x88,0x88,0x88,0x88,0x8A,0x8E,0x88,0x88,0x88,0x88,0xB8,0xB8,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x40,0x40,0x7F,0x00,0x00,0x00,0x00,0x00,0x00,0x00,/*"宁",29*/

	0x00,0xC8,0xD8,0xE8,0xCE,0xCC,0xF8,0xC8,0xC0,0xFC,0xFC,0x44,0x44,0xC2,0x42,0x00,
	0x00,0x32,0x1A,0xCA,0x7F,0x02,0x0A,0x12,0xE2,0x3F,0x07,0x00,0x00,0xFF,0x00,0x00,/*"新",30*/

	0x10,0x60,0x02,0x8C,0x00,0x00,0xFE,0x92,0x92,0x92,0x92,0x92,0xFE,0x00,0x00,0x00,
	0x04,0x04,0x7E,0x01,0x40,0x7E,0x42,0x42,0x7E,0x42,0x7E,0x42,0x42,0x7E,0x40,0x00,/*"温",31*/
	
	0x10,0x60,0x02,0x8C,0x00,0xFE,0x92,0x92,0x92,0x92,0x92,0x92,0xFE,0x00,0x00,0x00,
	0x04,0x04,0x7E,0x01,0x44,0x48,0x50,0x7F,0x40,0x40,0x7F,0x50,0x48,0x44,0x40,0x00,/*"湿",32*/

	0x00,0x00,0xFC,0x24,0x24,0x24,0xFC,0x25,0x26,0x24,0xFC,0x24,0x24,0x24,0x04,0x00,
	0x40,0x30,0x8F,0x80,0x84,0x4C,0x55,0x25,0x25,0x25,0x55,0x4C,0x80,0x80,0x80,0x00,/*"度",33*/



};

/************************************6*8的点阵************************************/
static unsigned char g_f6X8[][6] = {
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, // sp
    { 0x00, 0x00, 0x00, 0x2f, 0x00, 0x00 }, // !
    { 0x00, 0x00, 0x07, 0x00, 0x07, 0x00 }, // "
    { 0x00, 0x14, 0x7f, 0x14, 0x7f, 0x14 }, // #
    { 0x00, 0x24, 0x2a, 0x7f, 0x2a, 0x12 }, // $
    { 0x00, 0x62, 0x64, 0x08, 0x13, 0x23 }, // %
    { 0x00, 0x36, 0x49, 0x55, 0x22, 0x50 }, // &
    { 0x00, 0x00, 0x05, 0x03, 0x00, 0x00 }, // '
    { 0x00, 0x00, 0x1c, 0x22, 0x41, 0x00 }, // (
    { 0x00, 0x00, 0x41, 0x22, 0x1c, 0x00 }, // )
    { 0x00, 0x14, 0x08, 0x3E, 0x08, 0x14 }, // *
    { 0x00, 0x08, 0x08, 0x3E, 0x08, 0x08 }, // +
    { 0x00, 0x00, 0x00, 0xA0, 0x60, 0x00 }, // ,
    { 0x00, 0x08, 0x08, 0x08, 0x08, 0x08 }, // -
    { 0x00, 0x00, 0x60, 0x60, 0x00, 0x00 }, // .
    { 0x00, 0x20, 0x10, 0x08, 0x04, 0x02 }, // /
    { 0x00, 0x3E, 0x51, 0x49, 0x45, 0x3E }, // 0
    { 0x00, 0x00, 0x42, 0x7F, 0x40, 0x00 }, // 1
    { 0x00, 0x42, 0x61, 0x51, 0x49, 0x46 }, // 2
    { 0x00, 0x21, 0x41, 0x45, 0x4B, 0x31 }, // 3
    { 0x00, 0x18, 0x14, 0x12, 0x7F, 0x10 }, // 4
    { 0x00, 0x27, 0x45, 0x45, 0x45, 0x39 }, // 5
    { 0x00, 0x3C, 0x4A, 0x49, 0x49, 0x30 }, // 6
    { 0x00, 0x01, 0x71, 0x09, 0x05, 0x03 }, // 7
    { 0x00, 0x36, 0x49, 0x49, 0x49, 0x36 }, // 8
    { 0x00, 0x06, 0x49, 0x49, 0x29, 0x1E }, // 9
    { 0x00, 0x00, 0x36, 0x36, 0x00, 0x00 }, // :
    { 0x00, 0x00, 0x56, 0x36, 0x00, 0x00 }, // ;号
    { 0x00, 0x08, 0x14, 0x22, 0x41, 0x00 }, // <
    { 0x00, 0x14, 0x14, 0x14, 0x14, 0x14 }, // =
    { 0x00, 0x00, 0x41, 0x22, 0x14, 0x08 }, // >
    { 0x00, 0x02, 0x01, 0x51, 0x09, 0x06 }, // ?
    { 0x00, 0x32, 0x49, 0x59, 0x51, 0x3E }, // @
    { 0x00, 0x7C, 0x12, 0x11, 0x12, 0x7C }, // A
    { 0x00, 0x7F, 0x49, 0x49, 0x49, 0x36 }, // B
    { 0x00, 0x3E, 0x41, 0x41, 0x41, 0x22 }, // C
    { 0x00, 0x7F, 0x41, 0x41, 0x22, 0x1C }, // D
    { 0x00, 0x7F, 0x49, 0x49, 0x49, 0x41 }, // E
    { 0x00, 0x7F, 0x09, 0x09, 0x09, 0x01 }, // F
    { 0x00, 0x3E, 0x41, 0x49, 0x49, 0x7A }, // G
    { 0x00, 0x7F, 0x08, 0x08, 0x08, 0x7F }, // H
    { 0x00, 0x00, 0x41, 0x7F, 0x41, 0x00 }, // I
    { 0x00, 0x20, 0x40, 0x41, 0x3F, 0x01 }, // J
    { 0x00, 0x7F, 0x08, 0x14, 0x22, 0x41 }, // K
    { 0x00, 0x7F, 0x40, 0x40, 0x40, 0x40 }, // L
    { 0x00, 0x7F, 0x02, 0x0C, 0x02, 0x7F }, // M
    { 0x00, 0x7F, 0x04, 0x08, 0x10, 0x7F }, // N
    { 0x00, 0x3E, 0x41, 0x41, 0x41, 0x3E }, // O
    { 0x00, 0x7F, 0x09, 0x09, 0x09, 0x06 }, // P
    { 0x00, 0x3E, 0x41, 0x51, 0x21, 0x5E }, // Q
    { 0x00, 0x7F, 0x09, 0x19, 0x29, 0x46 }, // R
    { 0x00, 0x46, 0x49, 0x49, 0x49, 0x31 }, // S
    { 0x00, 0x01, 0x01, 0x7F, 0x01, 0x01 }, // T
    { 0x00, 0x3F, 0x40, 0x40, 0x40, 0x3F }, // U
    { 0x00, 0x1F, 0x20, 0x40, 0x20, 0x1F }, // V
    { 0x00, 0x3F, 0x40, 0x38, 0x40, 0x3F }, // W
    { 0x00, 0x63, 0x14, 0x08, 0x14, 0x63 }, // X
    { 0x00, 0x07, 0x08, 0x70, 0x08, 0x07 }, // Y
    { 0x00, 0x61, 0x51, 0x49, 0x45, 0x43 }, // Z
    { 0x00, 0x00, 0x7F, 0x41, 0x41, 0x00 }, // [
    { 0x00, 0x55, 0x2A, 0x55, 0x2A, 0x55 }, // 55
    { 0x00, 0x00, 0x41, 0x41, 0x7F, 0x00 }, // ]
    { 0x00, 0x04, 0x02, 0x01, 0x02, 0x04 }, // ^
    { 0x00, 0x40, 0x40, 0x40, 0x40, 0x40 }, // _
    { 0x00, 0x00, 0x01, 0x02, 0x04, 0x00 }, // '
    { 0x00, 0x20, 0x54, 0x54, 0x54, 0x78 }, // a
    { 0x00, 0x7F, 0x48, 0x44, 0x44, 0x38 }, // b
    { 0x00, 0x38, 0x44, 0x44, 0x44, 0x20 }, // c
    { 0x00, 0x38, 0x44, 0x44, 0x48, 0x7F }, // d
    { 0x00, 0x38, 0x54, 0x54, 0x54, 0x18 }, // e
    { 0x00, 0x08, 0x7E, 0x09, 0x01, 0x02 }, // f
    { 0x00, 0x18, 0xA4, 0xA4, 0xA4, 0x7C }, // g
    { 0x00, 0x7F, 0x08, 0x04, 0x04, 0x78 }, // h
    { 0x00, 0x00, 0x44, 0x7D, 0x40, 0x00 }, // i
    { 0x00, 0x40, 0x80, 0x84, 0x7D, 0x00 }, // j
    { 0x00, 0x7F, 0x10, 0x28, 0x44, 0x00 }, // k
    { 0x00, 0x00, 0x41, 0x7F, 0x40, 0x00 }, // l
    { 0x00, 0x7C, 0x04, 0x18, 0x04, 0x78 }, // m
    { 0x00, 0x7C, 0x08, 0x04, 0x04, 0x78 }, // n
    { 0x00, 0x38, 0x44, 0x44, 0x44, 0x38 }, // o
    { 0x00, 0xFC, 0x24, 0x24, 0x24, 0x18 }, // p
    { 0x00, 0x18, 0x24, 0x24, 0x18, 0xFC }, // q
    { 0x00, 0x7C, 0x08, 0x04, 0x04, 0x08 }, // r
    { 0x00, 0x48, 0x54, 0x54, 0x54, 0x20 }, // s
    { 0x00, 0x04, 0x3F, 0x44, 0x40, 0x20 }, // t
    { 0x00, 0x3C, 0x40, 0x40, 0x20, 0x7C }, // u
    { 0x00, 0x1C, 0x20, 0x40, 0x20, 0x1C }, // v
    { 0x00, 0x3C, 0x40, 0x30, 0x40, 0x3C }, // w
    { 0x00, 0x44, 0x28, 0x10, 0x28, 0x44 }, // x
    { 0x00, 0x1C, 0xA0, 0xA0, 0xA0, 0x7C }, // y
    { 0x00, 0x44, 0x64, 0x54, 0x4C, 0x44 }, // z
    { 0x14, 0x14, 0x14, 0x14, 0x14, 0x14 }, // horiz lines
};

/****************************************8*16的点阵************************************/
static const unsigned char g_f8X16[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 0
    
0x00,0x08,0x08,0xE8,0xE8,0x28,0x28,0x2E, 0x00,0x00,0x20,0x33,0x1F,0x0A,0xC2,0x42,// ! 1
0x2C,0x28,0x28,0xE8,0xE8,0x08,0x08,0x00,0x7E,0x02,0x0A,0x1B,0x13,0x20,0x20,0x00,// " 2	/*"京",0*/
               
0x00,0x60,0x44,0x98,0x28,0x20,0xA8,0xA8,0x00,0x60,0x38,0x06,0x10,0x14,0x14,0x14,// # 3
0xA8,0xFE,0xA8,0xA8,0xF8,0xF8,0x20,0x00,0x14,0xFF,0x14,0x14,0x14,0x14,0x10,0x00,// $ 4	/*"津",1*/
	
0x00,0x14,0x14,0xD4,0xD4,0xD4,0xDE,0xC0,0x00,0x10,0x94,0x57,0x56,0x7E,0x16,0x17,// % 5
0xC0,0xDE,0xD4,0xD4,0xD4,0x1C,0x10,0x00,0x17,0x1E,0x7E,0x56,0x57,0xD4,0x10,0x00,// & 6	/*"冀",2*/

0x00,0xC0,0xC6,0xDE,0xF6,0xC6,0xFE,0xC6,0x00,0x00,0x00,0xFE,0x4A,0x4A,0x4A,0x4A,// ' 7
0xC6,0xFE,0xC6,0xF6,0xDE,0xC6,0xC0,0x00,0x4A,0x4A,0x4A,0x4A,0xFE,0x00,0x00,0x00, // ( 8	/*"晋",3*/

0x00,0x74,0x74,0x14,0x54,0x5E,0x54,0x54,0x00,0x10,0xF5,0x55,0x55,0x2B,0x2B,0x57,// ) 9
0x54,0x5E,0x5E,0x54,0x14,0x74,0x74,0x00,0xDD,0x79,0x05,0x1D,0x33,0x61,0x40,0x00,// * 10	/*"蒙",4*/

0x00,0x40,0x46,0xCC,0x00,0x00,0x04,0x04,0x00,0x40,0x20,0x1F,0x20,0x20,0x40,0x40,// + 11
0x04,0x04,0xE4,0x14,0x1C,0x0C,0x04,0x00,0x58,0x58,0x4F,0x40,0x40,0x40,0x40,0x00,// , 12	/*"辽",5*/

0x00,0x18,0x98,0x98,0x98,0x98,0x98,0xFE,0x00,0x00,0x00,0xFE,0x22,0x22,0x22,0x22,// - 13
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, // . 14
0xFE,0x98,0x98,0x98,0x98,0x98,0x18,0x00,0x22,0x22,0x22,0x22,0x7E,0x00,0x00,0x00,// / 15	/*"吉",6*/

    0x00, 0xE0, 0x10, 0x08, 0x08, 0x10, 0xE0, 0x00, 0x00, 0x0F, 0x10, 0x20, 0x20, 0x10, 0x0F, 0x00, // 0 16
    0x00, 0x10, 0x10, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x20, 0x3F, 0x20, 0x20, 0x00, 0x00, // 1 17
    0x00, 0x70, 0x08, 0x08, 0x08, 0x88, 0x70, 0x00, 0x00, 0x30, 0x28, 0x24, 0x22, 0x21, 0x30, 0x00, // 2 18
    0x00, 0x30, 0x08, 0x88, 0x88, 0x48, 0x30, 0x00, 0x00, 0x18, 0x20, 0x20, 0x20, 0x11, 0x0E, 0x00, // 3 19
    0x00, 0x00, 0xC0, 0x20, 0x10, 0xF8, 0x00, 0x00, 0x00, 0x07, 0x04, 0x24, 0x24, 0x3F, 0x24, 0x00, // 4 20
    0x00, 0xF8, 0x08, 0x88, 0x88, 0x08, 0x08, 0x00, 0x00, 0x19, 0x21, 0x20, 0x20, 0x11, 0x0E, 0x00, // 5 21
    0x00, 0xE0, 0x10, 0x88, 0x88, 0x18, 0x00, 0x00, 0x00, 0x0F, 0x11, 0x20, 0x20, 0x11, 0x0E, 0x00, // 6 22
    0x00, 0x38, 0x08, 0x08, 0xC8, 0x38, 0x08, 0x00, 0x00, 0x00, 0x00, 0x3F, 0x00, 0x00, 0x00, 0x00, // 7 23
    0x00, 0x70, 0x88, 0x08, 0x08, 0x88, 0x70, 0x00, 0x00, 0x1C, 0x22, 0x21, 0x21, 0x22, 0x1C, 0x00, // 8 24
    0x00, 0xE0, 0x10, 0x08, 0x08, 0x10, 0xE0, 0x00, 0x00, 0x00, 0x31, 0x22, 0x22, 0x11, 0x0F, 0x00, // 9 25
    0x00, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x00, 0x00, // : 26

0x00,0x00,0x7E,0x42,0x4A,0x7A,0x62,0xFE,0x00,0x48,0x79,0x09,0x09,0x29,0x69,0x0F,// ;号 27
0xFE,0x62,0x7A,0x4A,0x42,0x7E,0x00,0x00,0x0F,0x79,0x49,0x09,0x09,0x79,0x48,0x00,// < 28 /*"黑",7*/

0x00,0x60,0x46,0xCC,0x08,0x00,0x00,0xF0,0x00,0x40,0x70,0x1C,0x06,0xC0,0x38,0x0F,// = 29
0x10,0x1A,0x16,0x10,0x10,0xF0,0xF0,0x00,0x01,0x01,0x01,0x01,0x01,0x03,0x03,0x00,// > 30	/*"沪",8*/

0x00,0x08,0x88,0x88,0xBE,0xBE,0x88,0xE8,0x00,0x10,0x4C,0x46,0x20,0x38,0x0E,0x03,// ? 31
0x88,0x88,0xBE,0x88,0x08,0x08,0x08,0x00,0x40,0x40,0x78,0x3F,0x02,0x06,0x08,0x00,// @ 32	/*"苏",9*/

    0x00, 0x00, 0xC0, 0x38, 0xE0, 0x00, 0x00, 0x00, 0x20, 0x3C, 0x23, 0x02, 0x02, 0x27, 0x38, 0x20, // A 33
    0x08, 0xF8, 0x88, 0x88, 0x88, 0x70, 0x00, 0x00, 0x20, 0x3F, 0x20, 0x20, 0x20, 0x11, 0x0E, 0x00, // B 34
    0xC0, 0x30, 0x08, 0x08, 0x08, 0x08, 0x38, 0x00, 0x07, 0x18, 0x20, 0x20, 0x20, 0x10, 0x08, 0x00, // C 35
    0x08, 0xF8, 0x08, 0x08, 0x08, 0x10, 0xE0, 0x00, 0x20, 0x3F, 0x20, 0x20, 0x20, 0x10, 0x0F, 0x00, // D 36
    0x08, 0xF8, 0x88, 0x88, 0xE8, 0x08, 0x10, 0x00, 0x20, 0x3F, 0x20, 0x20, 0x23, 0x20, 0x18, 0x00, // E 37
    0x08, 0xF8, 0x88, 0x88, 0xE8, 0x08, 0x10, 0x00, 0x20, 0x3F, 0x20, 0x00, 0x03, 0x00, 0x00, 0x00, // F 38
    0xC0, 0x30, 0x08, 0x08, 0x08, 0x38, 0x00, 0x00, 0x07, 0x18, 0x20, 0x20, 0x22, 0x1E, 0x02, 0x00, // G 39
    0x08, 0xF8, 0x08, 0x00, 0x00, 0x08, 0xF8, 0x08, 0x20, 0x3F, 0x21, 0x01, 0x01, 0x21, 0x3F, 0x20, // H 40
    0x00, 0x08, 0x08, 0xF8, 0x08, 0x08, 0x00, 0x00, 0x00, 0x20, 0x20, 0x3F, 0x20, 0x20, 0x00, 0x00, // I 41
    0x00, 0x00, 0x08, 0x08, 0xF8, 0x08, 0x08, 0x00, 0xC0, 0x80, 0x80, 0x80, 0x7F, 0x00, 0x00, 0x00, // J 42
    0x08, 0xF8, 0x88, 0xC0, 0x28, 0x18, 0x08, 0x00, 0x20, 0x3F, 0x20, 0x01, 0x26, 0x38, 0x20, 0x00, // K 43
    0x08, 0xF8, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x3F, 0x20, 0x20, 0x20, 0x20, 0x30, 0x00, // L 44
    0x08, 0xF8, 0xF8, 0x00, 0xF8, 0xF8, 0x08, 0x00, 0x20, 0x3F, 0x00, 0x3F, 0x00, 0x3F, 0x20, 0x00, // M 45
    0x08, 0xF8, 0x30, 0xC0, 0x00, 0x08, 0xF8, 0x08, 0x20, 0x3F, 0x20, 0x00, 0x07, 0x18, 0x3F, 0x00, // N 46
    0xE0, 0x10, 0x08, 0x08, 0x08, 0x10, 0xE0, 0x00, 0x0F, 0x10, 0x20, 0x20, 0x20, 0x10, 0x0F, 0x00, // O 47
    0x08, 0xF8, 0x08, 0x08, 0x08, 0x08, 0xF0, 0x00, 0x20, 0x3F, 0x21, 0x01, 0x01, 0x01, 0x00, 0x00, // P 48
    0xE0, 0x10, 0x08, 0x08, 0x08, 0x10, 0xE0, 0x00, 0x0F, 0x18, 0x24, 0x24, 0x38, 0x50, 0x4F, 0x00, // Q 49
    0x08, 0xF8, 0x88, 0x88, 0x88, 0x88, 0x70, 0x00, 0x20, 0x3F, 0x20, 0x00, 0x03, 0x0C, 0x30, 0x20, // R 50
    0x00, 0x70, 0x88, 0x08, 0x08, 0x08, 0x38, 0x00, 0x00, 0x38, 0x20, 0x21, 0x21, 0x22, 0x1C, 0x00, // S 51
    0x18, 0x08, 0x08, 0xF8, 0x08, 0x08, 0x18, 0x00, 0x00, 0x00, 0x20, 0x3F, 0x20, 0x00, 0x00, 0x00, // T 52

    //COST: TIME: 车牌 大写字母 数字
0x00,0x44,0xCC,0x98,0x20,0x20,0xFE,0xFE,0x00,0x40,0x78,0x0D,0x02,0x42,0x7F,0x3F,// U 53
0x20,0x00,0xFC,0x44,0x44,0xC4,0x46,0x00,0x41,0x78,0x0F,0x00,0x00,0x7F,0x00,0x00,// V 54	/*"浙",10*/


0x00,0xF8,0x1C,0x1E,0xF8,0x08,0x38,0x48,0x00,0x7F,0x11,0x11,0x1F,0x00,0x41,0x61,// W 55
0x48,0x4A,0x4E,0x48,0x48,0x18,0x18,0x00,0x3F,0x01,0x01,0x7F,0x41,0x41,0x61,0x00,// X 56	/*"皖",11*/

0x00,0x00,0xF0,0x0C,0xE0,0xE0,0x24,0x24,0x00,0x80,0xFF,0x00,0x17,0x17,0x12,0x12,// Y 57
0xF4,0x24,0xE4,0xE4,0x04,0xFC,0xFC,0x00,0x1F,0x12,0x1F,0x1B,0xE0,0x7F,0x3F,0x00,// Z 58	/*"闽",12*/

0x00,0xA4,0xB4,0xA6,0xA4,0xB4,0xAC,0x50,0x10,0x17,0x16,0xFE,0xFE,0x17,0x10,0xC1,// [ 59
0x58,0xAE,0xB6,0xB4,0xBC,0x24,0x20,0x00,0x5D,0x45,0x3D,0x15,0x65,0x5D,0xC1,0x00,// \ 60	/*"赣",13*/

0x00,0x20,0x10,0xF8,0xDC,0xD6,0xD6,0xF4,0x00,0x02,0x02,0xFE,0x56,0x56,0x56,0x56,// ] 61
0xF4,0xDC,0xD4,0xD0,0xF0,0x00,0x00,0x00,0x56,0x56,0x56,0x56,0xFE,0x02,0x02,0x00,// ^ 62	/*"鲁",14*/

0x00,0x84,0x94,0xE4,0x9C,0x8C,0x84,0x18,0x00,0x40,0xC0,0x7F,0x00,0x03,0x54,0x6A,// _ 63
0xFC,0x96,0xD4,0xB4,0x9C,0x90,0xF0,0x00,0x2B,0x55,0x5B,0x7C,0x06,0x1B,0x60,0x00,// ` 64	/*"豫",15*/

0x00,0x5E,0x52,0x5E,0x40,0x5E,0x5E,0x52,0x00,0x01,0x01,0x0F,0x09,0x49,0x49,0x79,// a 65
0x5E,0x00,0xFE,0x02,0x42,0xFE,0x06,0x00,0x01,0x01,0xFF,0x00,0x10,0x11,0x1F,0x00,// b 66	/*"鄂",16*/

0x00,0x64,0xC4,0x18,0x20,0xA0,0xFE,0x20,0x00,0x70,0x1E,0x10,0x1C,0x03,0xFF,0x01,// c 67
0x20,0x00,0xFC,0x44,0x44,0x44,0xFC,0x00,0x03,0x00,0xFF,0x22,0x22,0x22,0x7F,0x00,// d 68	/*"湘",17*/

0x00,0x00,0x00,0xFC,0xD4,0xDC,0xB6,0x9E,0x00,0x02,0x02,0x02,0x0E,0x0E,0x0A,0x0A,// e 69
0xFC,0xB4,0xDC,0x84,0xFC,0x00,0x00,0x00,0x4A,0x4A,0x4A,0x7A,0x0A,0x02,0x02,0x00,// f 70	/*"粤",18*/

0x00,0x30,0xB0,0xFE,0x32,0xF0,0x80,0x88,0x00,0x0C,0x03,0xFF,0x81,0x03,0x20,0x24,// g 71
0x88,0x88,0xFE,0x88,0x88,0x88,0x80,0x00,0x24,0x24,0x3F,0x24,0x24,0x24,0x20,0x00,// h 72	/*"桂",19*/

0x00,0x88,0x88,0xF8,0x88,0x88,0x08,0xC8,0x00,0x30,0x10,0x1F,0x08,0x48,0x60,0x1B,// i 73
0x48,0x4A,0x4E,0x48,0x48,0xC8,0x08,0x00,0x02,0xC2,0x7E,0x02,0x02,0x1B,0x60,0x00,// j 74	/*"琼",20*/

0x00,0x40,0xC4,0x8C,0x48,0xE0,0xB0,0xB8,0x00,0x40,0x70,0x0C,0x00,0x7F,0x7F,0x4A,// k 75
0xAC,0xA6,0x2C,0xB8,0x10,0xA0,0x20,0x00,0x7F,0x7F,0x00,0x1F,0x40,0x7F,0x00,0x00,// l 76	/*"渝",21*/

0x00,0x00,0x00,0xFC,0xFC,0x00,0x00,0x00,0x00,0x40,0x70,0x1F,0x01,0x00,0x00,0x00,// m 77
0xF8,0x00,0x00,0x00,0xFE,0xFE,0x00,0x00,0x3F,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,// n 78	/*"川",22*/

0x00,0x80,0x80,0xBC,0xA4,0xA4,0xA4,0xFE,0x00,0x40,0xC0,0x5E,0x42,0x62,0x22,0x1E,// o 79
0xFE,0xA4,0xA4,0xA4,0xBC,0xA0,0x80,0x00,0x06,0x12,0x22,0x22,0x6E,0x40,0x00,0x00,// p 80	/*"贵",23*/

0x00,0xC0,0xC0,0xC4,0xC4,0xC4,0xC4,0xC4,0x00,0x00,0x20,0x70,0x78,0x2C,0x23,0x21,// q 81
0xC4,0xC4,0xC4,0xC4,0xC4,0xC0,0xC0,0x00,0x20,0x20,0x2C,0x38,0x30,0x60,0x00,0x00,// r 82	/*"云",24*/

0x00,0xE4,0x84,0x84,0xE4,0x2E,0xAE,0xA4,0x00,0x76,0x8E,0xF6,0x1F,0x00,0x3F,0x2A,// s 83
0xA4,0xAE,0x34,0xF4,0x34,0xBC,0x24,0x00,0x3B,0xEE,0x60,0x3F,0x38,0x67,0x60,0x00,// t 84	/*"藏",25*/

0x00,0xFC,0xFC,0x64,0xBC,0x04,0x08,0xE8,0x00,0x7F,0x7F,0x08,0x4D,0x47,0x61,0x31,// u 85
0x08,0xFE,0xFE,0x88,0xE8,0x28,0x00,0x00,0x19,0x0F,0x07,0x19,0x31,0x61,0x41,0x00,// v 86	/*"陕",26*/

0x00,0x20,0x20,0x20,0xFE,0x20,0x20,0x20,0x00,0x00,0x00,0x00,0x7F,0x22,0x22,0x22,// w 87
0x20,0x20,0x20,0xFE,0x20,0x20,0x20,0x00,0x22,0x22,0x22,0x7F,0x00,0x00,0x00,0x00,// x 88	/*"甘",27*/

0x00,0x40,0x44,0x54,0x54,0x54,0x54,0x7E,0x00,0x00,0x00,0xFF,0x15,0x15,0x15,0x15,// y 89
0x7E,0x54,0x54,0x54,0x54,0x44,0x40,0x00,0x15,0x15,0x55,0x55,0x7F,0x00,0x00,0x00,// z 90	/*"青",28*/

0x00,0xB8,0xB8,0x88,0x88,0x88,0x88,0x8A,0x00,0x00,0x00,0x00,0x00,0x00,0x40,0x40,// { 91
0x8E,0x88,0x88,0x88,0x88,0xB8,0xB8,0x00,0x7F,0x00,0x00,0x00,0x00,0x00,0x00,0x00,// | 92	/*"宁",29*/

0x00,0xC8,0xD8,0xE8,0xCE,0xCC,0xF8,0xC8,0x00,0x32,0x1A,0xCA,0x7F,0x02,0x0A,0x12,// } 93
0xC0,0xFC,0xFC,0x44,0x44,0xC2,0x42,0x00,0xE2,0x3F,0x07,0x00,0x00,0xFF,0x00,0x00,// ~ 94	/*"新",30*/
};

#endif