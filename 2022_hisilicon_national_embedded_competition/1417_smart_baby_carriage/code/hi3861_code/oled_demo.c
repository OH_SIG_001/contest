/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_i2c.h"
#include "iot_gpio.h"
#include "iot_errno.h"
#include "iot_adc.h"//adc函数
#include "beep.h"
#include "oled_ssd1306.h"

#define AHT20_BAUDRATE (400 * 1000)
#define AHT20_I2C_IDX 0

/*******************************************************************************/
#define ADC_GPIO_OUTPORT 9
#define ADC_TASK_STACK_SIZE 1024
#define ADC_TASK_PRIO 24

static float GetVoltage(void)
{
    unsigned int ret;
    unsigned short data;
    //ret相当于返回数值，通过ret的数值来判断是否初始化成功
    ret=AdcRead(IOT_ADC_CHANNEL_4,&data,IOT_ADC_EQU_MODEL_8,IOT_ADC_CUR_BAIS_DEFAULT,0xff);//使用通道ADC4
    if(ret!=IOT_SUCCESS)
    {
        printf("ADC READ FAIL!");
    }
    return (float)data*1.8*4/4096.0;
}
static float GetVoltage2(void)//11、、雨滴
{
    unsigned int ret;
    unsigned short data;
    //ret相当于返回数值，通过ret的数值来判断是否初始化成功
    ret=AdcRead(IOT_ADC_CHANNEL_5,&data,IOT_ADC_EQU_MODEL_8,IOT_ADC_CUR_BAIS_DEFAULT,0xff);//使用通道ADC4
    if(ret!=IOT_SUCCESS)
    {
        printf("ADC READ FAIL!");
    }
    return (float)data*1.8*4/4096.0;
}
static float GetVoltage3(void)//7人体
{
    unsigned int ret;
    unsigned short data;
    //ret相当于返回数值，通过ret的数值来判断是否初始化成功
    ret=AdcRead(IOT_ADC_CHANNEL_3,&data,IOT_ADC_EQU_MODEL_8,IOT_ADC_CUR_BAIS_DEFAULT,0xff);//使用通道ADC4
    if(ret!=IOT_SUCCESS)
    {
        printf("ADC READ FAIL!");
    }
    return (float)data*1.8*4/4096.0;
}
static void ADCTask(void)
{
    float Voltage,Voltage2,Voltage3;
    Beep_Init();
    IoTGpioSetOutputVal(ADC_GPIO_OUTPORT,1);//将GPIO9的引脚电平拉高光敏
    IoTGpioSetOutputVal(11,1);//雨滴传感器
    IoTGpioSetOutputVal(7,0);//人体传感器
    while(1)
    {
        printf("======================\r\n");
        printf("  智能婴儿车 车内数据  \r\n");
        printf("======================\r\n");
        Voltage=GetVoltage();
        Voltage2=GetVoltage2();
        Voltage3=GetVoltage3();
        printf("当前光照强度：%.2fLux\n",(10240/(1.1*Voltage))-2500);//勒克斯
        printf("婴儿床垫湿度%.2f\n",Voltage2);
        if(Voltage2<3)
        {
            printf("当前是否尿床：是\r\n");
            printf("请为婴儿更换尿布\r\n");
        }
        else
        {
            printf("当前是否尿床：否\r\n");
        }
        printf("人员是否靠近：%.2fV\n",Voltage3);
        if(Voltage3>=3)
        {
            printf("警告！！！可疑人员靠近！！！注意防范！！！\r\n");
            printf("警告！！！可疑人员靠近！！！注意防范！！！\r\n");
            printf("警告！！！可疑人员靠近！！！注意防范！！！\r\n");
            printf("警告！！！可疑人员靠近！！！注意防范！！！\r\n");
            printf("警告！！！可疑人员靠近！！！注意防范！！！\r\n");
            printf("警告！！！可疑人员靠近！！！注意防范！！！\r\n");
            printf("警告！！！可疑人员靠近！！！注意防范！！！\r\n");
            printf("警告！！！可疑人员靠近！！！注意防范！！！\r\n");
            printf("警告！！！可疑人员靠近！！！注意防范！！！\r\n");
            Beep_Alarm();
        }
        else
        {
            printf("安全\r\n");
        }
        usleep(1000000);//每延时1s输出一次电压情况

    }
}
static void ADCtext(void)//adc测试程序
{
    osThreadAttr_t attr;
    attr.name = "ADCTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = ADC_TASK_STACK_SIZE; /* 任务大小4096 */
    attr.priority = osPriorityNormal;
    if (osThreadNew(ADCTask, NULL, &attr) == NULL) {
        printf("[ADCTask] Falied to create ADCTask!\n");
    }
}
APP_FEATURE_INIT(ADCtext);


/*******************************************************************************/


static void OledmentTask(const char *arg)
{

    (void)arg;
    OledInit();
    OledFillScreen(0);
    IoTI2cInit(AHT20_I2C_IDX, AHT20_BAUDRATE);
    OledShowString(15, 0, "Welcom ZZULI", 1);
    OledShowString(1, 3, "Smart baby carriage", 1); /* 屏幕第20列3行显示1行 */
    
}

static void OledDemo(void)
{
    osThreadAttr_t attr;
    attr.name = "OledmentTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096; /* 任务大小4096 */
    attr.priority = 23;//任务优先级24
    if (osThreadNew(OledmentTask, NULL, &attr) == NULL) {
        printf("[OledDemo] Falied to create OledmentTask!\n");
    }
}

APP_FEATURE_INIT(OledDemo);