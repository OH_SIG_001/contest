# _*_ coding:utf-8 _*_
'''
python3 
opencv 
ffmpeg 
rtmp 推流视频直播pipe:: Invalid argumentb
'''
import cv2
import os
import subprocess
import time

class FFmpegPlayer(object):
    def __init__(self,uri):
        self.URI=uri
    
    def play(self,uri=None):
        if(uri!=None): self.URI=uri
        self.cap = cv2.VideoCapture(self.URI)
        while(self.cap.isOpened()):
            ret, frame = self.cap.read()
            if not ret:
                print("拉取视频流时出现错误！")
                break
            cv2.imshow(self.URI,frame)
            if(cv2.waitKey(1)==ord('q')): break

#cap = cv2.VideoCapture("rtmp://43.132.239.7:8081/live/")

def main():
    with open("player.cfg",'r',encoding='utf-8') as file:
        addr=file.readline()
    fp = FFmpegPlayer("rtmp://1.13.152.174/live/10004")
    fp.play()

if __name__=="__main__":
    main()
