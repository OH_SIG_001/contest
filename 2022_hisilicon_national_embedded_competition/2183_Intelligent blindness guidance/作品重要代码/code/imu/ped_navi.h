// ped_navi.h : main header file for the PED_NAVI application
//

#if !defined(AFX_PED_NAVI_H__A5074507_0F19_49CB_A9AA_A35DE3701174__INCLUDED_)
#define AFX_PED_NAVI_H__A5074507_0F19_49CB_A9AA_A35DE3701174__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CPed_naviApp:
// See ped_navi.cpp for the implementation of this class
//

class CPed_naviApp : public CWinApp
{
public:
	CPed_naviApp();

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CPed_naviApp)
public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CPed_naviApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PED_NAVI_H__A5074507_0F19_49CB_A9AA_A35DE3701174__INCLUDED_)
