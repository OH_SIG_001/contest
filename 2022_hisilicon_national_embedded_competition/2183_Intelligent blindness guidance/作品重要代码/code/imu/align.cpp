#include "stdafx.h"
#include "align.h"
#include "math.h"

extern double atti[3];//该变量在navigation.cpp中定义，下面的函数可以直接修改这个值，作为导航的初值
extern double velo[3];
extern double posi[3];

extern double real_g;
extern double modi_g;
extern double pi;


void AlignAtti(double accex,double accey,double accez)//初始对准中通过该函数求得初始姿态角（仅利用加表数据）
{

     atti[0]=-atan(accex/accez);
     atti[1]=asin((accey*real_g)/real_g);//2013-05-14修改g by wanjunwei1
	 /////////////////////转换为角度///////////////////////
	 atti[0]=atti[0]*180.0/pi;
	 atti[1]=atti[1]*180.0/pi;
}


void AlignPosi()
{
      posi[0]=0;
	  posi[1]=0;
	  posi[2]=0;


}
void AlignVelo()
{
	velo[0]=0;
	velo[1]=0;
	velo[2]=0;
}
