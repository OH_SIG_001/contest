import os, glob, random
import cv2,numpy as np
from PIL import Image
orgList = glob.glob("./Examples/dataset/6/" +"*." + "jpg")
i= 0

for index in range(len(orgList)):
    img=Image.open(orgList[index])
    out1 = img.transpose(Image.FLIP_LEFT_RIGHT)  # 水平翻转

    if (i < 10):
        out1.save("./Examples/data/6/000000" + str(i) + ".jpg")
        img.save("./Examples/data/6/200000" + str(i) + ".png")

    if (10 <= i < 100):
        out1.save("./Examples/data/6/00000" + str(i) + ".png")
        img.save("./Examples/data/6/20000" + str(i) + ".png")

    if (i >= 100):
        out1.save("./Examples/data/6/0000" + str(i) + ".jpg")
        img.save("./Examples/data/6/2000" + str(i) + ".jpg")

    i = i + 1
