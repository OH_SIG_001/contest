#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_time.h"

#define GPIO2 2
void set_angle( unsigned int duty) {
    IoTGpioSetDir(GPIO2, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(GPIO2, IOT_GPIO_VALUE1);
    hi_udelay(duty);
    IoTGpioSetOutputVal(GPIO2, IOT_GPIO_VALUE0);
    hi_udelay(20000 - duty);
}

/*Turn 45 degrees to the left of the steering gear
1、依据角度与脉冲的关系，设置高电平时间为1000微秒
2、不断地发送信号，控制舵机向左旋转45度
*/
/*void engine_turn_left_45(void)
{
    for (int i = 0; i <10; i++) 
    {
        set_angle(1000);
    }
}
*/
/*Turn 90 degrees to the left of the steering gear
1、依据角度与脉冲的关系，设置高电平时间为500微秒
2、不断地发送信号，控制舵机向左旋转90度
*/
void engine_turn_left_90(void)
{
    for (int i = 0; i <10; i++) 
    {
        set_angle(500);
    }
}

/*Turn 45 degrees to the right of the steering gear
1、依据角度与脉冲的关系，设置高电平时间为2000微秒
2、不断地发送信号，控制舵机向右旋转45度
*/
/*void engine_turn_right_45(void)
{
    for (int i = 0; i <10; i++) 
    {
        set_angle(2000);
    }
}
*/
/*Turn 90 degrees to the right of the steering gear
1、依据角度与脉冲的关系，设置高电平时间为2500微秒
2、不断地发送信号，控制舵机向右旋转90度
*/
void engine_turn_right_90(void)
{
    for (int i = 0; i <10; i++) 
    {
        set_angle(2500);
    }
}

/*The steering gear is centered
1、依据角度与脉冲的关系，设置高电平时间为1500微秒
2、不断地发送信号，控制舵机居中
*/
/*void regress_middle(void)
{
    for (int i = 0; i <10; i++) 
    {
        set_angle(1500);
    }
}
*/
/*任务实现*/
void RobotTask(void* parame) {
    (void)parame;
    printf("start test sg90\r\n");
    engine_turn_left_90();//控制舵机向左转90°
    osDelay(500);//延时5s
    engine_turn_right_90();//控制舵机向右转90°
}


static void RobotDemo(void)
{
    osThreadAttr_t attr;

    attr.name = "RobotTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 10240;
    attr.priority = osPriorityNormal;

    if (osThreadNew(RobotTask, NULL, &attr) == NULL) {
        printf("[RobotDemo] Falied to create RobotTask!\n");
    }
}

APP_FEATURE_INIT(RobotDemo);  