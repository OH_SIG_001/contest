## 智能家居安防系统

段佳雨 黄寅昌

**第一部分**  **设计概述**

1.1  设计目的

家居安全一直是重中之重，随着现在智能系统不断更新进步，智能安防系统有着不可替代的作用，区别于传统的安防产品，智能安防更人性化、智能化，构筑了立体化的智能安防体系，保障了生命财产的安全。本次竞赛我们围绕海思Hi3861V100、Hi3516DV300进行硬件系统搭建，并在移动端搭建了轻量化小程序，最终设计出具有实时监控、预警和通知的智能安防系统。

 

1.2  应用领域

主要应用在家居安防领域，或者具有私人性、保密性的场景，当有人进入监控区域时，安防系统会识别访客身份，并根据访客身份设置系统进入安全模式或者预警模式。

 

1.3  主要技术特点

（1）基于resnet18的分类网和yolov2的检测网完成对人脸识别模型的训练。

（2）基于串口通讯完成数据从Hi3516DV300到Hi3861V100的传输。

（3）在腾讯云平台创建产品和设备，使连接WiFi的Hi3861V100通过MQTT协议与云端通信，将监控系统识别结果上传，微信小程序调用腾讯云API读取绑定的设备

 

1.4  关键性能指标

 

1.5  主要创新点

（1）基于腾讯云实现远端实时监控和控制，完成轻量化小程序设计

（2）设计产品可应用于具有私人性、保密性，特征下的多场景

 

**第二部分**  **系统组成及功能说明**

 

2   

2.1  整体介绍

从系统搭建的角度该系统主要分为三部分，以3516DV300为核心的子系统主要搭载了监控模块、语音模块、红外模块。以Hi3861V100为核心的子系统主要搭载了警示灯模块、显示屏模块、和WiF模块，以腾讯云和小程序为核心的子系统搭载了远程通知和控制模块。

![wps](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image002.gif)

2.2  各模块介绍

（1）人脸识别模块：

人脸识别模块首先进行数据集的制作，通过3516DV300摄像头采集两种人物信息以及一种背景信息。并将采集到的三种视频在本地通过FFmpeg每一秒截取一张图片用作训练的数据集。

**Resnet18****分类网训练：**

**![eebc16d6d2066eddcf9f585ae174480](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image004.gif)**![img](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image006.jpg)

 

![c3785f256e50524d1c5879563f6e614](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image008.gif)

在这里要注意resnet18分类网进行训练时要将每一类图片保存在单独的文件夹中

人物1（djy）1440张图片

人物2（xxq）1000张图片

背景 1000张图片

采用darknet深度学习框架进行模型的训练

![be84b6464419f63073d93596da2c494](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image010.jpg)

 

**Yolo2****网络训练：**

人脸检测大约选取2000张照片，人脸图片的获取途径主要是在互联网和手机拍摄

![9ffc1e22b9f853899b6f9fa215ba05c](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image012.gif)

![010c5c9960de4eb8e7fcbd9c5432158](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image014.gif)

获取到数据集后对每张照片进行标注，标注后的图片为txt格式

![d2b8a27b69b3409e88825596e0a58c3](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image016.gif)

 

![e268af760e11bc43ae978eb3eec1841](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image018.gif)![64681f2c8af67d4f797981564979346](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image020.gif)

将yolo2转换成resnet18 网络并将caffe模型转换成darknet模型进行训练

![cb1e389c870fb2cfbb4dc16cd7c3b1c](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image022.jpg)

训练完成后通过ruyistdio转化为适合于板端部署的.wk文件

 

![e1c87fe87829ddd2e367ac5eccc4ea8](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image024.gif)

板端部署模型：

 

![img](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image026.jpg)

 

![img](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image028.jpg)

 

Face_classfy.c关键代码：

![img](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image030.jpg)

 

（2）预警模块

预警模块主要分为语音警告、警示灯警告和显示屏警告。其中3516DV300子系统进行语音播报，Hi3861V100子系统用于警示灯和显示屏警告。

当监控模块识别结果为房主（实验过程中为“段佳雨”、“许西庆”）时，进行播放语音，“已经检测到您已进入监控区”。显示屏将显示“welcome djy”或“welcome xxq”；

当监控模块识别结果为stranger时，进行播放语音“已经检测到您已进入监控区，请立即离开”，警告灯亮起，并在显示屏显示“unsafe people”；

（3）小程序模块：

小程序将解析云端的实时上传数据，实时显示监控区域的人物身份，包括姓名和人脸截图。当系统启动预警模式时，小程序可以手动关闭警示灯（该功能主要应用于房主判断监控区域内的人为安全时，可以解除警报）

 

 

**第三部分**  **完成情况及性能参数**

基本完成上述模块的设计，并进行现场实验演示

部分现场图如下：

![微信图片_20220714154234](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image032.jpg)

![微信图片_20220714154220](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image034.jpg)

![微信图片_20220714154228](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image036.jpg)

 

![5c41824fde1c6fb6eed78ce1ed9ee53](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image038.jpg)

3  

4  

**第六部分**  **附录**

**![小程序2](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image040.gif)**

**![主要代码1](file:///C:/Users/Duanjy/AppData/Local/Temp/msohtmlclip1/01/clip_image042.gif)**