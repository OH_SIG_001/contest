#include <Servo.h>
#include <SoftwareSerial.h>         
#include <Adafruit_PWMServoDriver.h>

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(); 
#define SERVO_0  102 
#define SERVO_45  187 
#define SERVO_60  218 
#define SERVO_90  280 
#define SERVO_135  373
#define SERVO_150  417 


Servo myservo;  
 
char SerialData;


char val= '/';

void setup() {
  Serial.begin(115200);
  myservo.attach(10);  
  BT.begin(9600);
  
  pwm.begin();
  pwm.setPWMFreq(500); 
  delay(10);
}
 
void loop() {
 
Serial.println("go");
val=Serial.read();


if (val == '0') 
{
    pwm.setPWM(0, 0, SERVO_135);
    pwm.setPWM(1, 0, SERVO_135);
    pwm.setPWM(3, 0, SERVO_112);
    pwm.setPWM(4, 0, SERVO_45);
    pwm.setPWM(5, 0, SERVO_45);
    pwm.setPWM(6, 0, SERVO_45);
    pwm.setPWM(7, 0, SERVO_45);
    pwm.setPWM(8, 0, SERVO_45);
    pwm.setPWM(9, 0, SERVO_45); 
}
else if (val == '1')
   {
    pwm.setPWM(0, 0, SERVO_60);
    pwm.setPWM(1, 0, SERVO_90);
    pwm.setPWM(2, 0, SERVO_150);
    pwm.setPWM(3, 0, SERVO_150);
    pwm.setPWM(4, 0, SERVO_45);
    pwm.setPWM(5, 0, SERVO_45);
    pwm.setPWM(6, 0, SERVO_45);
    pwm.setPWM(7, 0, SERVO_45);
    pwm.setPWM(8, 0, SERVO_45);
    pwm.setPWM(9, 0, SERVO_45);
   }

else if (val == '2')
   {
    pwm.setPWM(0, 0, SERVO_60);
    pwm.setPWM(1, 0, SERVO_90);
    pwm.setPWM(2, 0, SERVO_45);
    pwm.setPWM(3, 0, SERVO_45);
    pwm.setPWM(4, 0, SERVO_150);
    pwm.setPWM(5, 0, SERVO_150);
    pwm.setPWM(6, 0, SERVO_45);
    pwm.setPWM(7, 0, SERVO_45);
    pwm.setPWM(8, 0, SERVO_45);
    pwm.setPWM(9, 0, SERVO_45);
   }
else if (val == '3')
   {
    pwm.setPWM(0, 0, SERVO_60);
    pwm.setPWM(1, 0, SERVO_90);
    pwm.setPWM(2, 0, SERVO_45);
    pwm.setPWM(3, 0, SERVO_45);
    pwm.setPWM(4, 0, SERVO_45);
    pwm.setPWM(5, 0, SERVO_45);
    pwm.setPWM(6, 0, SERVO_150);
    pwm.setPWM(7, 0, SERVO_150);
    pwm.setPWM(8, 0, SERVO_45);
    pwm.setPWM(9, 0, SERVO_45);
   }
else if (val == '4')
   {
    pwm.setPWM(0, 0, SERVO_60);
    pwm.setPWM(1, 0, SERVO_90);
    pwm.setPWM(2, 0, SERVO_45);
    pwm.setPWM(3, 0, SERVO_45);
    pwm.setPWM(4, 0, SERVO_45);
    pwm.setPWM(5, 0, SERVO_45);
    pwm.setPWM(6, 0, SERVO_45);
    pwm.setPWM(7, 0, SERVO_45);
    pwm.setPWM(8, 0, SERVO_150);
    pwm.setPWM(9, 0, SERVO_150);
   }
   
else if (val == 'b')
   {
    pwm.setPWM(0, 0, SERVO_60);
    pwm.setPWM(1, 0, SERVO_90);
    pwm.setPWM(2, 0, SERVO_150);
    pwm.setPWM(3, 0, SERVO_150);
    pwm.setPWM(4, 0, SERVO_150);
    pwm.setPWM(5, 0, SERVO_150);
    pwm.setPWM(6, 0, SERVO_45);
    pwm.setPWM(7, 0, SERVO_45);
    pwm.setPWM(8, 0, SERVO_45);
    pwm.setPWM(9, 0, SERVO_45);
   }
else if (val == 'a')
   {
    pwm.setPWM(0, 0, SERVO_135);
    pwm.setPWM(1, 0, SERVO_135);
    pwm.setPWM(2, 0, SERVO_150);
    pwm.setPWM(3, 0, SERVO_150);
    pwm.setPWM(4, 0, SERVO_45);
    pwm.setPWM(5, 0, SERVO_45);
    pwm.setPWM(6, 0, SERVO_45);
    pwm.setPWM(7, 0, SERVO_45);
    pwm.setPWM(8, 0, SERVO_45);
    pwm.setPWM(9, 0, SERVO_45);
   }

//else if (val == '04')
//   {
//    pwm.setPWM(0, 0, SERVO_135);
//    pwm.setPWM(1, 0, SERVO_135);
//    pwm.setPWM(2, 0, SERVO_45);
//    pwm.setPWM(3, 0, SERVO_45);
//    pwm.setPWM(4, 0, SERVO_45);
//    pwm.setPWM(5, 0, SERVO_45);
//    pwm.setPWM(6, 0, SERVO_45);
//    pwm.setPWM(7, 0, SERVO_45);
//    pwm.setPWM(8, 0, SERVO_45);
//    pwm.setPWM(9, 0, SERVO_45);
//   } 
else if  (val == 'c')
   {
    pwm.setPWM(0, 0, SERVO_135);
    pwm.setPWM(1, 0, SERVO_135);
    pwm.setPWM(2, 0, SERVO_150);
    pwm.setPWM(3, 0, SERVO_150);
    pwm.setPWM(4, 0, SERVO_150);
    pwm.setPWM(5, 0, SERVO_150);
    pwm.setPWM(6, 0, SERVO_45);
    pwm.setPWM(7, 0, SERVO_45);
    pwm.setPWM(8, 0, SERVO_45);
    pwm.setPWM(9, 0, SERVO_45);
   }
else if  (val == 'e')
   {
    pwm.setPWM(0, 0, SERVO_60);
    pwm.setPWM(1, 0, SERVO_90);
    pwm.setPWM(2, 0, SERVO_45);
    pwm.setPWM(3, 0, SERVO_45);
    pwm.setPWM(4, 0, SERVO_150);
    pwm.setPWM(5, 0, SERVO_150);
    pwm.setPWM(6, 0, SERVO_150);
    pwm.setPWM(7, 0, SERVO_150);
    pwm.setPWM(8, 0, SERVO_150);
    pwm.setPWM(9, 0, SERVO_150);
    }

else if  (val == 'f')
   {
    pwm.setPWM(0, 0, SERVO_60);
    pwm.setPWM(1, 0, SERVO_90);
    pwm.setPWM(2, 0, SERVO_150);
    pwm.setPWM(3, 0, SERVO_150);
    pwm.setPWM(4, 0, SERVO_150);
    pwm.setPWM(5, 0, SERVO_150);
    pwm.setPWM(6, 0, SERVO_150);
    pwm.setPWM(7, 0, SERVO_150);
    pwm.setPWM(8, 0, SERVO_150);
    pwm.setPWM(9, 0, SERVO_150);
    }
else if  (val == 'g')
   {
    pwm.setPWM(0, 0, SERVO_135);
    pwm.setPWM(1, 0, SERVO_135);
    pwm.setPWM(2, 0, SERVO_150);
    pwm.setPWM(3, 0, SERVO_150);
    pwm.setPWM(4, 0, SERVO_150);
    pwm.setPWM(5, 0, SERVO_150);
    pwm.setPWM(6, 0, SERVO_150);
    pwm.setPWM(7, 0, SERVO_150);
    pwm.setPWM(8, 0, SERVO_150);
    pwm.setPWM(9, 0, SERVO_150);
    } 
}
