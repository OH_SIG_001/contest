def Brenner(img): # brenner检测模糊
    x, y = img.shape
    D = 0
    for i in range(x-2):
        for j in range(y-2):
            D += (img[i+2, j] - img[i, j])**2
    return D

def TestBrener():
    dir = "" # 测试路径
    imgList = getAllImg(dir)

    for i in range(len(imgList)):
        frame = ImageToMatrix(dir , imgList[i])
        score = Brenner(frame)
        print(str(imgList[i]) + " is " + str(score))
