const app = getApp()
var myrequest = require('../../utils/util.js')//相当于import

Page({
  data: {
    C_Gas: 0,
    class_num: 0,
    stateReported: {},//板子传回来的数据
    id :99,
    num :99,
    flag:0,//flag=10写入数据库
    history_class_1:0,
    history_class_2:0,
    history_class_3:0,
    history_class_4:0,
    history_class_5:0,
    history_gas_1:0,
    history_gas_2:0,
    history_gas_3:0,
    history_gas_4:0,
    history_gas_5:0,
    history_data_1:0,
    history_data_2:0,
    history_data_3:0,
    history_data_4:0,
    history_data_5:0,
    fireornot:0,
    //根据种类匹配image中的数据，0有火，1无火，默认为1
    fireimage1: '../../image/' + 2 + '.png',
    fireimage2: '../../image/' + 2 + '.png',
    fireimage3: '../../image/' + 2 + '.png',
    fireimage4: '../../image/' + 2 + '.png',
    fireimage5: '../../image/' + 2 + '.png',
  }, 

  //页面显示class_num, C_gas, 并且1s刷新一次 
  //设定tag=0，每秒+1，无火重置为0，有火不做操作
  //如果tag=10，即10s有火，post到一个url，并在历史记录显示。

  //页面显示class_num, C_gas，历史记录（无刷新按钮，前端无交互）

async getMydata(){
    var that = this

    await wx.cloud.callFunction({
      name: 'iothub-shadow-query',
      data: {
            ProductId: app.globalData.productId,
            DeviceName: app.globalData.deviceName,
            SecretId: app.globalData.secretId,
            SecretKey: app.globalData.secretKey,
          },
    }).then(res =>{
      let deviceData = JSON.parse(res.result.Data)//将p板获取到的所有数据，交给deviceData
      //console.log(deviceData.payload.state.reported)    
        this.setData({
          stateReported: deviceData.payload.state.reported,//deviceData交给全局变量stateReported
        })
      
    //获取P板数据
    //板子中请求来的数据给id和class_num :id：class_num；num：C_gas
      this.data.id = this.data.stateReported.class_num;
      this.data.num = this.data.stateReported.C_Gas;   
      // that.data.num = that.data.stateReported.weight;
      console.log("that.data.num:", that.data.num)
      console.log("that.data.id:", that.data.id)
    })

    
      console.log(this.data.id)
      console.log(this.data.num)
      
      var that = this;
      var id = that.data.id;//stateReported.class_num;  //用定义在里面的id就不能加入路径
      var num = that.data.num;//stateReported.C_gas;//p板传来的重量
      var flag  = that.data.flag;
      var fireornot = that.data.fireornot;//是否有火

      if(id==1&&num<=300){
        console.log("无火，flag重置为0",flag)
        flag=0
        fireornot='无火'
        that.setData({
          flag: flag,
          fireornot:fireornot
        })
      }
      else if (num>300){
        //传到数据库
        console.log("num>300了",num)
        const res = await myrequest.myrequest({
          url: 'http://81.71.13.99:5000/adddata',
          header: {
            'content-type': 'application/json' // 默认值
          },
          data:{
            'fire':id,
            'C_Gas':num
          },
          method: 'POST',       
        })
        wx.showToast({
          icon: 'success',
          duration: 2000,//持续的时间
          title: '检测到火情',
         });
        //this.sleep(1000*60*10)    
        flag=0
        fireornot='无火'
        that.setData({
          flag: flag,
          fireornot:fireornot
        })
      
      }
      else if((id==0&&flag<10)){
        flag+=1
        fireornot='有火'
        that.setData({
          flag: flag,
          fireornot:fireornot
        })
        console.log("执行flag+1，此时flag：",flag)
        if(flag==10){
          //传到数据库
          const res = await myrequest.myrequest({
            url: 'http://81.71.13.99:5000/adddata',
            header: {
              'content-type': 'application/json' // 默认值
            },
            data:{
              'fire':id,
              'C_Gas':num
            },
            method: 'POST',       
          })
          wx.showToast({
            icon: 'success',
            duration: 2000,//持续的时间
            title: '检测到火情',
           });
          //this.sleep(1000*60*10)
          fireornot='有火'                 
          flag=0
          that.setData({
            flag: flag,
            fireornot:fireornot
          })

        }
      }
      
      else{
          console.log("flag>10或错误，此时flag为",flag)
          flag=0
          that.setData({
            flag: flag
          })
          console.log("flag>10或错误后重置为0",flag)
      }      
},
async Historydata(){
      var that = this;
      var history_class_1=that.data.history_class_1;
      var history_class_2=that.data.history_class_2;
      var history_class_3=that.data.history_class_3;
      var history_class_4=that.data.history_class_4;
      var history_class_5=that.data.history_class_5;
      var history_gas_1=that.data.history_gas_1;
      var history_gas_2=that.data.history_gas_2;
      var history_gas_3=that.data.history_gas_3;
      var history_gas_4=that.data.history_gas_4;
      var history_gas_5=that.data.history_gas_5;
      var history_data_1=that.data.history_data_1;
      var history_data_2=that.data.history_data_2;
      var history_data_3=that.data.history_data_3;
      var history_data_4=that.data.history_data_4;
      var history_data_5=that.data.history_data_5;
      var fireimage1=that.data.fireimage1;
      var fireimage2=that.data.fireimage1;
      var fireimage3=that.data.fireimage1;
      var fireimage4=that.data.fireimage1;
      var fireimage5=that.data.fireimage1;
      

  setTimeout(()=>
   {
    wx.request({
      url: 'http://81.71.13.99:5000/getdata',
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success:function(res){
        
        console.log(res.data); 
        console.log("res.data.data[0][0]：", res.data.data[0][0])
        history_class_1=res.data.data[0][0];
        fireimage1='../../image/' + res.data.data[0][0] + '.png';
        console.log(fireimage1);
        console.log("res.data.data[0][1]", res.data.data[0][1])
        history_gas_1=res.data.data[0][1];
        console.log("res.data.data[0][2]", res.data.data[0][2])
        history_data_1=res.data.data[0][2];
        console.log("res.data.data[1][0]", res.data.data[1][0])
        history_class_2=res.data.data[1][0];
        fireimage2= '../../image/' + res.data.data[1][0] + '.png';
        console.log(fireimage2);
        console.log("res.data.data[1][1]", res.data.data[1][1])
        history_gas_2=res.data.data[1][1]; 
        console.log("res.data.data[1][2]", res.data.data[1][2])
        history_data_2=res.data.data[1][2];
        console.log("res.data.data[2][0]", res.data.data[2][0])
        history_class_3=res.data.data[2][0];
        fireimage3='../../image/' + res.data.data[2][0] + '.png';
        console.log(fireimage3);
        console.log("res.data.data[2][1]", res.data.data[2][1])
        history_gas_3=res.data.data[2][1]; 
        console.log("res.data.data[2][2]", res.data.data[2][2])
        history_data_3=res.data.data[2][2];
        console.log(res.data.data[3][0]); 
        history_class_3=res.data.data[3][0];
        fireimage4='../../image/' + res.data.data[3][0] + '.png';
        console.log(fireimage4);
        console.log(res.data.data[3][1]);  
        history_gas_4=res.data.data[3][1]; 
        console.log(res.data.data[3][2]); 
        history_data_4=res.data.data[3][2];
        console.log(res.data.data[4][0]); 
        history_class_4=res.data.data[4][0];
        fireimage5='../../image/' + res.data.data[4][0] + '.png';
        console.log(fireimage5);
        console.log(res.data.data[4][1]);  
        history_gas_5=res.data.data[4][1]; 
        console.log(res.data.data[4][2]); 
        history_data_5=res.data.data[4][2];
        console.log("获取一次数据库");

        that.setData({
          history_class_1: history_class_1,
          history_class_2: history_class_2,
          history_class_3: history_class_3,
          history_class_4: history_class_4,
          history_class_5: history_class_5,
          history_gas_1: history_gas_1,
          history_gas_2: history_gas_2,
          history_gas_3: history_gas_3,
          history_gas_4: history_gas_4,
          history_gas_5: history_gas_5,
          history_data_1:history_data_1,
          history_data_2:history_data_2,
          history_data_3:history_data_3,
          history_data_4:history_data_4,
          history_data_5:history_data_5,
          fireimage1:fireimage1,
          fireimage2:fireimage2,
          fireimage3:fireimage3,
          fireimage4:fireimage4,
          fireimage5:fireimage5,
        })
      },
      fail:function(res){
        console.log(".....fail.....");
      }
    })
   }, 1200)
},
    /**
   * 生命周期函数--监听页面显示
   */


   sleep: function(numberMillis) {
    var now = new Date();
    var exitTime = now.getTime() + numberMillis;
    while (true) {
      now = new Date();
      if (now.getTime() > exitTime)
        return;
    }
  },
 
  onShow: function () {
    var that = this;
    interval = setInterval(function() {
      that.getMydata();//2s获取一次数据
      console.log("1秒了----------------------------------------------------")
    }, 100000)
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  },
    /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  },
  onLoad: function () {
    // 调用函数时，传入new Date()参数，返回值是日期和时间
    var time = util.formatTime(new Date());
    // 再通过setData更改Page()里面的data，动态更新页面的数据
    this.setData({
      time: time
    });
  },
 
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  },
})
