const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

// promise 特点：一创建就立即执行，一般情况下解决这个问题我们会将其封装为一个函数
// options:请求时的参数对象
function myrequest(options) {
    return new Promise((resolve, reject) => {
      // 逻辑：发送请求到服务器
      wx.request({
        url: options.url,
        method: options.method || "GET",
        data: options.data || {},
        header: options.header || {},
        success: res => {
          resolve(res);
        },
        fail: err => {
          reject(err);
        }
      });
    });
  }

//倒计时
  // const formatTime = date => {
  
  //   const year = date.getFullYear()
  //   const month = date.getMonth() + 1
  //   const day = date.getDate()
  //   const hour = date.getHours()
  //   const minute = date.getMinutes()
  //   const second = date.getSeconds()
     
  //   return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
     
  //  }
     
  //  const formatNumber = n => {
  //   n = n.toString()
  //   return n[1] ? n : '0' + n
  //  }
     
  // 暴露给外界
module.exports = {
  // formatTime,
  formatTime: formatTime,
  myrequest: myrequest
}