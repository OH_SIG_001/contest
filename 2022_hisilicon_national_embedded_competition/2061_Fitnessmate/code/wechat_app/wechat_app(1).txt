首页  .wxml
<!--pages/bind/bind.wxml-->
<view class="tittle">定制计划</view>
<view class="message">
  <text class="item">请输入您的性别</text>
   <input placeholder="男/女" bindinput="changeData0">{{gender}}</input>
  <text class="item">您的身高是（m）</text>
   <input placeholder="m" bindinput="changeData1">{{stature}}</input>
  <text class="item">请输入您的体重（Kg）</text>
   <input  placeholder="Kg" bindinput="changeData2">{{weight}}</input>
  <text class="item">您期望的每周运动时长是（min）</text>
   <input  placeholder="min" bindinput="changeData3">{{time}}</input>

  <button bindtap = "bim" size="default" type="primary">生成锻炼计划</button>
</view>

<view class="result">
  <view class="littleTittle">结果如下：</view>
 <text>{{result}}</text>
</view>
<image class="little" src=""></image>

首页  .wxss
/* pages/bind/bind.wxss */

.tittle{
  font-size: 80rpx;
  display: flex;
  font-family:LiSu;
  justify-content:flex-end;
}
.message{
  height: 700rpx;
  display: flex;
  flex-direction:column;
  /*元素在主轴方向如何显示center、space-between、flex-start*/
  justify-content: space-around;
  background:#F5F5F5;
}
.message .item{
  display: flex;
  flex-direction:row;
  justify-content: center;
}
.message {
  display: flex;
  /* text input 居中显示 */
  align-items: center;
}

.message input{
  border: solid 2rpx #dfdfdf;
  border-color: #00BFFF;
  border-radius: 4rpx;
  width: 200rpx;
  height: 70rpx;
  line-height: 70rpx;
  margin-right: 10rpx;
  /* 文字水平居中 */
  text-align: center;
}

.message text{
  font-size: 40rpx;
  font-family:Microsoft YaHei;
}
.result view{
  font-size: 60rpx;
  font-family:LiSu;
}
.result text{
  color: blue;
  font-size: 50rpx;
  font-family:Microsoft JhengHei;
}

image{
  height: 250px;
  width: 150px;
}

首页 .js
// pages/bind/bind.ts
var BIM
Page({
  /**
   * 页面的初始数据
   */
  data: {
    gender:"",
    stature:"",
    weight:"",
    time:"",
    result:"",
  },
  changeData0:function(e){
   //获取数据
    console.log(e.detail.value)
  // 修改前端gender数据
    this.setData({
    gender:e.detail.value,
    })
    // console.log(this.data.gender)
  },

  changeData1:function(e){
    //获取数据
     console.log(e.detail.value)
   // 修改stature数据
     this.setData({
     stature:e.detail.value
     })
    //  console.log(this.data.stature)
   },
   
   changeData2:function(e){
    //获取数据
     console.log(e.detail.value)
   // 修改weight数据
     this.setData({
     weight:e.detail.value
     })
    //  console.log(this.data.weight)
   }, 
   
   changeData3:function(e){
    //获取数据
     console.log(e.detail.value)
   // 修改time数据
     this.setData({
     time:e.detail.value
     })
    //  console.log(this.data.time)
   }, 

   bim:function(){
    //  计算BIM值，此处为d
     var a = parseFloat(this.data.weight)
     console.log(a)
     var b = parseFloat(this.data.stature)
     console.log(b)
     var c = b*b
     console.log(c)
     var d = a/c
     console.log(d)
    //  this.setData({
    //    BIM : d
    //  })
    //  console.log(this.data.BIM)

    if (d <= 18.4) {
      console.log("您体重偏瘦，建议做无氧+力量训练,增加饮食摄入")
      this.setData({result : 
        "您体重偏瘦，建议做无氧+力量训练，,增加饮食摄入"
      })
    }
     else if(18.5 <=d & d <= 23.9) {
     console.log("您体重正常，保持运动习惯，健康每一天")
     this.setData({result : 
      "您体重正常，保持运动习惯，，健康每一天"})
    }
     else {
     console.log("您体重较重，建议做有氧运动")
     this.setData({result :
       "您体重较重，建议做有氧运动，减少多余摄入"})
    }
    // this.setData({
    //   result : ""
    // })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})

菜单 .wxml
<!--index.wxml-->
<view class = "home">
 <image src="/static/1-0.jpg"></image>
</view>
  <view class = "tittle">基于T&A的家庭健身伙伴</view>
 
<view class="menu0">
 <view class="item">
  <image bindtap="clikMe2" src="/static/shouye/shenzhan.png"></image>
   <text>深蹲</text>
 </view>
 <view class="item">
   <image bindtap="clikMe3" src="/static/shouye/jianshen.png"></image>
   <text>健身</text>
  </view>
</view>  
<view class = "menu1"> 
 <view class="item">
   <image bindtap="clikMe0" src="/static/shouye/yujia.png"></image>
   <text>瑜伽</text>
 </view> 
 <view class="item">
   <image bindtap="clikMe1" src="/static/shouye/ticao.png"></image>
   <text>体操</text>
 </view>
</view>

菜单 .wxss
/**index.wxss**/
.home image{
  width: 750rpx;
  height: 450rpx;
}
.tittle{
  font-size: 50rpx;
  font-weight: 200; 
}

.tittle{
  display: flex;
  align-items: center;
  justify-content: center;

}

.menu0 image,.menu1 image{
  width: 150rpx;
  height: 150rpx;
  border-radius:200rpx;
}

.menu0, .menu1{
  /*height:1000rpx ;*/    /*规定竖直主轴长度*/
  display: flex;
  /*row:规定主轴方向为水平*/
  flex-direction:row ;/*row-水平显示，column-竖直显示*/

  /*元素在主轴方向如何显示center、space-between、flex-start*/
  justify-content: space-around;
 /*元素在副轴方向如何显示center、space-between、flex-start*/
  align-items: center;
}
.menu0 .item,.menu1 .item{
  display: flex;
  flex-direction:column;
  align-items: center;
}

菜单 .js
// pages/menu/menu.js
// pages/redirect/rediret.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
  
  },
  /**
   * 点击绑定的事件
   */
  clikMe0:function(e){
   var nid = e.currentTarget.dataset.nid;
   console.log(nid);
   //跳转
   wx.navigateTo({
     url: '/pages/redirect-yujia/redirect-yujia?id='+nid,
   })
  },
  clikMe1:function(e){
    var nid = e.currentTarget.dataset.nid;
    console.log(nid);
    //跳转
    wx.navigateTo({
      url: '/pages/redirect-ticao/redirect-ticao?id='+nid,
    })
  },
   clikMe2:function(e){
    var nid = e.currentTarget.dataset.nid;
    console.log(nid);
    //跳转非TabBar页面
    wx.navigateTo({
      url: '/pages/redirect-shendun/redirect-shendun?id='+nid,
    })
   },
   clikMe3:function(e){
    var nid = e.currentTarget.dataset.nid;
    console.log(nid);
    //跳转
    wx.navigateTo({
      url: '/pages/redirect-jianshen/redirect-jianshen?id='+nid,
    })
   }
})

