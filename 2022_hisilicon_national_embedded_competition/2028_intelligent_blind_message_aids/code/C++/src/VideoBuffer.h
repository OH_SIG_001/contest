#ifndef VIDEO_BUFFER_H_
#define VIDEO_BUFFER_H_

#include "DefaultDefine.h"

namespace hiych {

//vb类
class VideoBuffer
{
public:
    using DATA_VB_TYPE = VB_CONFIG_S;
    using DATA_POOL_TYPE = VB_POOL_CONFIG_S;
private:
    DATA_VB_TYPE* vbParam;
    bool configFlag;
    SIZE_S pic;

private:
    void initVbParam();
public:
    bool isConfigSetComplete() const;
    DATA_VB_TYPE* getParam() const;

    VideoBuffer(SIZE_S pic);
    ~VideoBuffer();
};

VideoBuffer::VideoBuffer(SIZE_S pic):
    vbParam(nullptr),
    configFlag(false),
    pic(pic)
{
    try
    {
        this->vbParam = new DATA_VB_TYPE;
    }
    catch(const std::bad_alloc& e)
    {
        std::cerr << e.what() << '\n';
    }

    memset_s(this->vbParam, sizeof(DATA_VB_TYPE), 0, sizeof(DATA_VB_TYPE));

    initVbParam();
}

VideoBuffer::~VideoBuffer()
{
    delete vbParam;
}

VideoBuffer::DATA_VB_TYPE* VideoBuffer::getParam() const
{
    return this->vbParam;
}

void VideoBuffer::initVbParam()
{   

    this->vbParam->u32MaxPoolCnt = 2;

    DATA_POOL_TYPE *pictureBuff = &(vbParam->astCommPool[0]);

    pictureBuff->u64BlkSize = COMMON_GetPicBufferSize(
        pic.u32Width,
        pic.u32Height,
        PIXEL_FORMAT_YVU_SEMIPLANAR_420,
        DATA_BITWIDTH_8,
        COMPRESS_MODE_SEG,
        DEFAULT_ALIGN
    );

    pictureBuff->u32BlkCnt = 10;

    DATA_POOL_TYPE *rawBuffer = &(vbParam->astCommPool[1]);

    rawBuffer->u64BlkSize = VI_GetRawBufferSize(
        pic.u32Width,
        pic.u32Height,
        PIXEL_FORMAT_RGB_BAYER_16BPP,
        COMPRESS_MODE_NONE,
        DEFAULT_ALIGN
    );
    
    rawBuffer->u32BlkCnt = 4;

    configFlag = true;
}

bool VideoBuffer::isConfigSetComplete() const
{
    return configFlag;
}

}

#endif