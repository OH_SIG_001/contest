****

**第一部分**  **设计概述**

1.1    设计目的

目前，疫情催生了线上经济和教育的新模式。然而，网络场景下人物的状态难以判断，这给线上服务业、线上教育业等造成了困难。为了帮助判断在线人物的情绪属性，我们基于Hi3516DV300与Hi3861v100设计了一套人脸检测与表情识别系统，能够定位、标记人脸并提取人脸中等多个维度的信息、通过面部的特征点提取来完成表情识别。

1.2    应用领域

表情识别是计算机理解人类情感的一个重要方向，也是人机交互的一个重要方面。
本方案主要应用于情绪分析的场景，针对摄像头内的人脸信息进行识别，能够实时跟随人物情绪并且在微信小程序上及时显示。尤其适用于线上教育课堂或者线上服务行业等场景。


1.3    主要技术特点

本系统主要包括视觉推理部分和支持腾讯云的云端微信小程序控制部分。视觉推理部分采用Taurus 套件进行部署，通过检测网络和分类网络可在不同摄像条件下对人脸进行检测和表情识别。Taurus的推理结果发送至Pegasus。Pegasus在获得分类结果后，执行基于mqtt的数据上云，腾讯云收到数据后作为中转点再将数据通过wifi传给微信小程序，最终微信小程序将表情相关消息推送给用户。

1.4    关键性能指标

1)	人脸检测精准度
测试集准确率由模型在测试集上的推理结果给出，通过推理结果与测试样本图片标签一致的比例得到。
实际准确率由摄像头识别结果与实际人脸相符的程度给出。

2)	表情识别精准度
测试集准确率由模型在测试集上的推理结果给出，通过推理结果与测试样本图片标签一致的比例得到。
实际准确率由摄像头分类结果与实际人脸表情类型相符的程度给出。


1.5    主要创新点

1)	基于图像处理和深度学习的嵌入式机器视觉，利用yolov2+resnet18网络框架实现人脸检测与表情识别功能；

2)	采用NNIE对神经网络特别是深度学习卷积神经网络进行加速处理；

3)	在表情识别的分类网络训练时，我们采用开源人脸与自制图片结合的方法制作了数据集，在不影响效率的同时提高了对本项目应用场景的配适性;

4)	本项目基于UDP局域网与Hi3861v100板端互联，在微信小程序端通过mqtt技术进行数据上云的服务。


**第二部分**  **系统组成及功能说明**

2     

2.1    整体介绍

整体系统包括四大部分，分别是模型训练、AI计算、信息传输和应用处理。

1)	模型训练主要包括对检测网络和分类网络的训练以及模型转换两部分。对于检测网络和分类网络，我们利用yolo v2网络和resnet18网络对开源数据集进行训练。模型转换部分将训练所得到的模型转为caffe，进一步形成wk文件。

2)	AI计算层包括视频输入、模型的调用、分类计算和信息传输。Taurus套件接受输入的视频数据并计算出当前人脸所述的类别，再将计算结果传输给信息传输层。

3)	信息传输层接受来自Taurus的分类结果信息，利用Taurus和Pegasus的通信模块将结果传输给Pegasus。

4)	应用处理层进行微信小程序控制，pegasus将taurus通过uart传输的结果利用mqtt技术进行云端的上传，最终微信小程序将腾讯云中的数据进行演示。


2.2    各模块介绍

1)	模型训练
对于检测网络，我们使用的数据集是WIDER FACE开源数据集。该数据集包括12880张图片，分为62类，覆盖场景复杂，可以充分保证人脸检测的精度。为了将开源数据集的标注转换为darknet场景支持的格式，我们写了一个格式转换的python脚本get_human_face_lable_data.py（见附录）。数据集制作完成后，利用yolo2进行训练。模型训练完毕后，完成darknet2caffe步骤，生成caffe模型并获得模型中的.caffemodel和.protetxt文件，再转化得到wk模型。

对于分类网络，我们使用的数据集是expw开源数据集和自摄数据集的集合。利用的训练框架为pytorch，训练网络为Resnet18。模型训练完毕后，将pth模型转化为caffe并获得模型中的.caffemodel和.prototxt文件，再转化得到wk文件。

2)	AI计算
计算层的本质是模型的推理。本项目依赖于Taurus开发套件。Taurus基于专用的Smart HD IP Camera 芯片 Hi3516DV300 设计，集成了新一代 ISP、业界最新的 H.265 视频压缩编码器、高性能神经网络加速 NNIE 引擎。推理所需的数据输入由Taurus摄像头的视频流实时提供。推理得到的分类结果将实时跟踪和显示。


3)	信息传输
本系统采用Pegasus套件进行控制功能。Pegasus套件基于于海思 Hi3861V100 芯片，该芯片是一款专为物联网 终端领域打造的 WiFi SoC 芯片，继承了高性能微处理器、安全引擎以及丰富的外设接口。外设接口包括SPI、 UART、I2C、 PWM、 I2S、 GPIO 和多路外部 ADC、高速 SDIO2.0 Slave 接口等。由于Taurus 和 Pegasus 的代码是分离的，我们利用UART协议设置Taurus端发送计算结果，Pegasus端接受计算结果。利用的通信模块主要是Hisignalling。

4)	应用处理
Pegasus在获得分类结果后，执行基于mqtt的数据上云，腾讯云收到数据后作为中转点再将数据通过wifi传给微信小程序，最终微信小程序将表情相关消息推送给用户。


###文件结构

└── 0401 人脸识别与表情检测

        └── Hisilicon_pegasus_part

                └──代码

        └──Hisilicon_taurus_part

                └── 代码
        └──Wechat_app

                └── 代码
This reposiroty stores the software code for the full process implementation and running of the facial expression recognition system.
It contains three parts. 
Hisilicon_taurus_part is responsible for the visual processing and recognition with the taurus kit.
Hisilicon_pegasus_part is used to connect the taurus kit with the tencent cloud
Wechat_app is used to connect the cloud with the wechat app.