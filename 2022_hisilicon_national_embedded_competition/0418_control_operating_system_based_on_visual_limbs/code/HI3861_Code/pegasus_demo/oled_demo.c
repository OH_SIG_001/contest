/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_i2c.h"
#include "iot_gpio.h"
#include "iot_errno.h"
//#include "hdf_io_service.h"
#include "oled_ssd1306.h"
#include "hisignalling_protocol.h"

#define AHT20_BAUDRATE (400 * 1000)
#define AHT20_I2C_IDX 0
#define delay  1500000
extern unsigned char gesture;

static void OledmentTask(const char *arg)
{
    //extern unsigned char gesture;
    (void)arg;
    OledInit();
    OledFillScreen(0);
    IoTI2cInit(AHT20_I2C_IDX, AHT20_BAUDRATE);
  //HisignallingErrorType HisignallingMsgReceive(hi_u8 *buf, hi_u32 len);
  //  if (gesture == 1)
  // {
  //   OledShowString(5, 3, "curry ", 1);
  //   OledShowString(20, 5, "stephen", 1);
  // }
  
   while(1)
  {
    switch (gesture)
    {
    case 1:
        OledShowString(10, 12, "fist", 1);
        usleep(delay);
        OledFillScreen(0);
        break;
    case 2:
        OledShowString(3, 8, "indexUp", 1);
        usleep(delay);
        OledFillScreen(0);
        break;
    case 3:
        OledShowString(15, 16, "OK", 1);
        usleep(delay);
        OledFillScreen(0);
        break;
    case 4:
       OledShowString(14, 14, "palm", 1);
       usleep(delay);
       OledFillScreen(0);
        break;
    case 5:
        OledShowString(13, 15, "yes", 1);
        usleep(delay);
        OledFillScreen(0);
        break;
    case 6:
        OledShowString(14, 6, "pinchOpen", 1);
        usleep(delay);
        OledFillScreen(0);
        break;
    case 7:
        OledShowString(17, 8, "phoneCall", 1);
        usleep(delay);
        OledFillScreen(0);
        break;
    default:
        OledShowString(15, 12, "others", 1);
        usleep(delay);
        OledFillScreen(0);
        break;
    }
    if(gesture == 0xffffffff)
    {
      printf("out detect");
      break;
    }
  }
    //OledShowString(19, 1, "Stephen Curry ", 1); /* 屏幕第20列3行显示1行 */
    //OledShowString(20, 3, "Klay Thosmon ", 1);
}

static void OledDemo(void)
{
    osThreadAttr_t attr;
    attr.name = "OledmentTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096; /* 任务大小4096 */
    attr.priority = osPriorityNormal;

    if (osThreadNew(OledmentTask, NULL, &attr) == NULL) {
        printf("[OledDemo] Falied to create OledmentTask!\n");
    }
}

APP_FEATURE_INIT(OledDemo);