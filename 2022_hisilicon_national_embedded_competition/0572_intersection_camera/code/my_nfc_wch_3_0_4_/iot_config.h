/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef IOT_CONFIG_H
#define IOT_CONFIG_H

// <CONFIG THE LOG
/* if you need the iot log for the development , please enable it, else please comment it */
#define CONFIG_LINKLOG_ENABLE   1

// < CONFIG THE WIFI
/* Please modify the ssid and pwd for the own */
#define CONFIG_AP_SSID  "HUAWEI_Mate40" // WIFI SSID
#define CONFIG_AP_PWD   "1234589qwe" // WIFI PWD
/* Tencent iot Cloud user ID , password */
#define CONFIG_USER_ID    "K3U7LY549Zhi_3861_mqtt;12010126;38aee;1664726400"
#define CONFIG_USER_PWD   "01fbb8c028fa0bfdc527ffa11efd16920f367e5233e58440e116577326bb207c;hmacsha256"
#define CN_CLIENTID     "K3U7LY549Zhi_3861_mqtt" // Tencent cloud ClientID format: Product ID + device name

//tencent_product_name  hi_3861_1
//tencent_product_id  K3U7LY549Z

//tencent_device_name 	hi_3861_mqtt
//tencent_decive_miyao  uyLMPzvU+c8yY4PUbKHjAQ==

//MQTT三元组
//tencent_device_Client_ID  K3U7LY549Zhi_3861_mqtt
//mqtt_username  K3U7LY549Zhi_3861_mqtt;12010126;38aee;1664726400
//mqtt_password  01fbb8c028fa0bfdc527ffa11efd16920f367e5233e58440e116577326bb207c;hmacsha256


//wechat_app_appid  wx0d4fe3bb9fd9f322

/*
//tencent_yun
tencent_yun_appid  1306183331                               ?this for what?
tencent_yun_SecretId  AKIDf9NL2TdV6QTFkyvb0ECtJSJ0QZQ1KM7b
tencent_yun_SecretKey  EBPN5VebKmxrUqvjFVUCepjMina6LJ0Q

tencent_yun_enviroment_id  cloud1-8g7vzkft1d07fb8c
tencent_yun_enviroment_name  cloud1
*/

#endif