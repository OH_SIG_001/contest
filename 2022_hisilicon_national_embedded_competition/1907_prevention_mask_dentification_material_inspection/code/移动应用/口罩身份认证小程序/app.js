//app.js

const utils = require('./utils/util.js')

App({
  onLaunch: function () {
    // 展示本地存储能力
    wx.getSystemInfo({
      success: e => {
        this.globalData.StatusBar = e.statusBarHeight;
        let custom = wx.getMenuButtonBoundingClientRect();
        this.globalData.Custom = custom;  
        this.globalData.CustomBar = custom.bottom + custom.top - e.statusBarHeight;
      }
    })
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
  },
  onHide: function () {
    wx.stopBackgroundAudio()
  },
  globalData: {
    detailChartData: [], //图表数据
    person: null
  }
})