#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <stdint.h>
#include <string.h>
#include <hi_types_base.h>
#include <hi_early_debug.h>
#include <hi_stdlib.h>
#include <hi_uart.h>
#include <hi_task.h>
#include <hi_time.h>
#include <hi_adc.h>
#include <hi_watchdog.h>
#include <hi_pwm.h>
#include <hi_gpio.h>
#include <hi_types_base.h>
#include <app_demo_uart.h>
#include <iot_uart.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio_ex.h"
#include "hi_io.h"
#include "iot_gpio.h"
#include "iot_errno.h"
#include "hisignalling_protocol.h"
#include "oled_ssd1306.h"
#include "oled_fonts.h"
#include "servo_test.h"
#include "hx711_demo.h"

#define LED_TEST_GPIO 9
#define IOT_GPIO_SERVO_1 2
#define IOT_GPIO_SERVO_2 4
#define IOT_GPIO_MOTOR_1 0
#define IOT_GPIO_MOTOR_2 1
#define LED_INTERVAL_TIME_US_NEW 300000
#define GapValue 430 

int goodapple_cnt=0;
int badapple_cnt=0;
int goodorange_cnt=0;
int badorange_cnt=0;
int goodcarrot_cnt=0;
int badcarrot_cnt=0;

double base_data = 0;

int fruit_class_1 = 0;
int fruit_class_2 = 0;
int fruit_class_3 = 0;
int fruit_class = 0;

int direction_flag=0;
int uart_flag=0;
int weight_cnt=0;
double fruit_weight=0;
double weight_buf[10] = {0};

extern hi_u8   data;

hi_void *Classify_Task(char *param)
{
    
    hi_gpio_init();

    IoTGpioInit(IOT_GPIO_SERVO_1);
    IoSetFunc(IOT_GPIO_SERVO_1,0);
    IoTGpioSetDir(IOT_GPIO_SERVO_1,IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(IOT_GPIO_SERVO_1,0);

    IoTGpioInit(IOT_GPIO_SERVO_2);
    IoSetFunc(IOT_GPIO_SERVO_2,0);
    IoTGpioSetDir(IOT_GPIO_SERVO_2,IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(IOT_GPIO_SERVO_2,0);

    IoTGpioInit(IOT_GPIO_MOTOR_1);
    IoSetFunc(IOT_GPIO_MOTOR_1,0);
    IoTGpioSetDir(IOT_GPIO_MOTOR_1,IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(IOT_GPIO_MOTOR_1,0);

    IoTGpioInit(IOT_GPIO_MOTOR_2);
    IoSetFunc(IOT_GPIO_MOTOR_2,0);
    IoTGpioSetDir(IOT_GPIO_MOTOR_2,IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(IOT_GPIO_MOTOR_2,0);


	//初始化GPIO7为GPIO输入，为传感器DT引脚
    hi_io_set_func(HI_IO_NAME_GPIO_7, HI_IO_FUNC_GPIO_7_GPIO);
    hi_gpio_set_dir(HI_GPIO_IDX_7,HI_GPIO_DIR_IN);
	//初始化GPIO8为GPIO输出，为传感器SCK引脚
    hi_io_set_func(HI_IO_NAME_GPIO_8, HI_IO_FUNC_GPIO_8_GPIO);
    hi_gpio_set_dir(HI_GPIO_IDX_8,HI_GPIO_DIR_OUT);
    hi_gpio_set_ouput_val(HI_IO_NAME_GPIO_8,0);

	OledInit();
	OledFillScreen(0);
	IoTI2cInit(AHT20_I2C_IDX, AHT20_BAUDRATE);

    servo_2_0();
    servo_1_0();
    base_data = Get_Sensor_Read(); //获取基准值

    while(1){
          IoTGpioSetOutputVal(IOT_GPIO_MOTOR_1,0);
          IoTGpioSetOutputVal(IOT_GPIO_MOTOR_2,1);
          printf("start classify\n");
        
        
            fruit_weight = (Sensor_Read() - base_data) / GapValue; //获取重量        
            hi_sleep(10);              							//该延时用于舵机控制与显示重量的联动  
            hi_gpio_set_ouput_val(LED_TEST_GPIO,0);
	    

        if(fruit_weight > 0) // 大于0时显示
        {
            printf("weight : %.2f\r\n" ,fruit_weight); 
			OledShowString(10,4, " weight :       ",2);
            sprintf_s(weight_buf,10,"%.2f g ",fruit_weight);
            OledShowString(25,6, weight_buf,2);
        }
        
        

        if(fruit_weight > 8)
        {
	        weight_cnt=weight_cnt+1;
	
	        IoTGpioSetOutputVal(IOT_GPIO_MOTOR_1,0);
            IoTGpioSetOutputVal(IOT_GPIO_MOTOR_2,0);

            
        }
        else
        {
	        weight_cnt=0;
        }

        

    
    printf("weight_cnt=%d\n",weight_cnt);
    
    printf("Start Hisignalling successfully\n");

    if (weight_cnt==3) {

        /*unsigned char *recBuff = NULL;
        unsigned int fruit_table[] = {0,0,0,0,0,0};
        unsigned int fruit_table_a[] = {0,0,0,0,0,0};
        unsigned int fruit_kind = 0;
        unsigned int i = 0;*/


        servo_2_0();
        fruit_class_1=0;
        data=0;
        while(fruit_class_1==0)
        {
            TaskMsleep(HISGNALLING_FREE_TASK_TIME);
            fruit_class_1=data;
        }
        printf("fruit_class_1=%d\n",fruit_class_1);
		
		servo_2_45();
        fruit_class_2=0;
        data=0;
        while(fruit_class_2==0)
        {
            TaskMsleep(HISGNALLING_FREE_TASK_TIME);
            fruit_class_2=data;
        }
        printf("fruit_class_1=%d\n",fruit_class_2);
		
		servo_2_0();
        servo_2_inv45();
        fruit_class_3=0;
        data=0;
        while(fruit_class_3==0)
        {
            TaskMsleep(HISGNALLING_FREE_TASK_TIME);
            fruit_class_3=data;
        }
        printf("fruit_class_1=%d\n",fruit_class_3);
		
        if(fruit_class_1==fruit_class_2==fruit_class_3)
		{
			fruit_class=(fruit_class_1+fruit_class_2+fruit_class_3)/3;
		}
		else if(fruit_class_1==fruit_class_2)
		{
			fruit_class=(fruit_class_1+fruit_class_2)/2;
		}
		else if(fruit_class_1==fruit_class_3)
		{
			fruit_class=(fruit_class_1+fruit_class_3)/2;
		}
		else if(fruit_class_2==fruit_class_3)
		{
			fruit_class=(fruit_class_2+fruit_class_3)/2;
		}
		else
		{
			fruit_class=data;
            
		}
			
        printf("fruit_class=%d\n",fruit_class);

            /* 显示分类 */
            switch(fruit_class){
                case 1:
                    weight_cnt=0;
                    OledShowString(10,0, "class : ",2);
                    OledShowString(25,2, "Good Apple",2);
                    servo_2_inv45();
			        hi_udelay(1000000);
			        servo_1_inv45();
                    hi_udelay(1000000);
                break;
                case 2:
                    weight_cnt=0;
                    OledShowString(10,0, "class : ",2);
                    OledShowString(25,2, "Bad Apple",2);
                    servo_2_inv45();
                    servo_2_inv90();
                    hi_udelay(1000000);
                    servo_1_inv45();
                    hi_udelay(2000000);
                    servo_1_0();
                    servo_2_45();
			        hi_udelay(1000000);
                break;
                case 3:
                    weight_cnt=0;
                    OledShowString(10,0, "class : ",2);
                    OledShowString(25,2, "Good Orange",2);
                    servo_2_0();
			        hi_udelay(1000000);
			        servo_1_45();
                    hi_udelay(1000000);
                break;
                case 4:
                    weight_cnt=0;
                    OledShowString(10,0, "class : ",2);
                    OledShowString(25,2, "Bad Orange",2);
                    servo_2_inv45();
                    servo_2_inv90();
                    hi_udelay(1000000);
                    servo_1_inv45();
                    hi_udelay(2000000);
                    servo_1_0();
                    servo_2_45();
			        hi_udelay(1000000);
                break;
                case 5:
                    weight_cnt=0;
                    OledShowString(10,0, "class : ",2);
                    OledShowString(25,2, "Good Potato",2);
                    servo_2_45();
			        hi_udelay(1000000);
			        servo_1_45();
                    hi_udelay(1000000);
                break;
                case 6:
                    weight_cnt=0;
                    OledShowString(10,0, "class : ",2);
                    OledShowString(25,2, "Bad Potato",2);
                    servo_2_inv45();
                    servo_2_inv90();
                    hi_udelay(1000000);
                    servo_1_inv45();
                    hi_udelay(2000000);
                    servo_1_0();
                    servo_2_45();
			        hi_udelay(1000000);
                break;
                case 7:
                    weight_cnt=0;
                    OledShowString(10,0, "class : ",2);
                    OledShowString(25,2, "Good Melon",2);
                    //servo_2_45();
                    servo_2_0();
			        hi_udelay(1000000);
			        servo_1_inv45();
                    hi_udelay(1000000);
                break;
                case 8:
                    weight_cnt=0;
                    OledShowString(10,0, "class : ",2);
                    OledShowString(25,2, "Bad Melon",2);
                    servo_2_inv45();
                    servo_2_inv90();
                    hi_udelay(1000000);
                    servo_1_inv45();
                    hi_udelay(2000000);
                    servo_1_0();
                    servo_2_45();
			        hi_udelay(1000000);
                break;
                default:
                    OledShowString(25,2, "no fruit",2);  
                break;
            }
    servo_1_0();
    servo_2_0();
    OledFillScreen(0x00);


    } 
   TaskMsleep(HISGNALLING_FREE_TASK_TIME);

  }
}

hi_u32 HisignalingMsgTask(hi_void)
{
    hi_u32 ret = 0;
    IoTGpioInit(LED_TEST_GPIO);
    IoTGpioSetDir(LED_TEST_GPIO, IOT_GPIO_DIR_OUT);
    osThreadAttr_t hisignallingAttr = {0};

    hisignallingAttr.stack_size = HISIGNALLING_MSG_TASK_STACK_SIZE;
    hisignallingAttr.priority = 26;
    hisignallingAttr.name = (hi_char*)"Classify_Task";

    if (osThreadNew((osThreadFunc_t)Classify_Task, NULL, &hisignallingAttr) == NULL) {
        HISIGNALLING_LOG_ERROR("Failed to create hisignaling msg task\r\n");
        return HI_ERR_FAILURE;
    }
    return HI_ERR_SUCCESS;
}
SYS_RUN(HisignalingMsgTask);