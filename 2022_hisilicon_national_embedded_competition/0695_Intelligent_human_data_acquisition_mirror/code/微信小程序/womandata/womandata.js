// pages/mandata/mandata.js
Page({

  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },

  bindViewTap_1:function() {
    wx.navigateTo({     
      url: '/pages/womandata/duanxiu/duanxiu?data='+this.data.shengao,
    })
  },

  bindViewTap_2:function() {
    wx.navigateTo({
      url: '/pages/womandata/chenshan/chenshan?data='+this.data.shengao,
    })
  },

  bindViewTap_3:function() {
    wx.navigateTo({
      url: '/pages/womandata/duanqun/duanqun?data='+this.data.shengao,
    })
  },

  bindViewTap_4:function() {
    wx.navigateTo({
      url: '/pages/womandata/changqun/changqun?data='+this.data.shengao,
     
    })
  },

  
  data: {
    shengao:165.7,
    bichang:69.8,
    jiankuan:49.2,
    tuichang:98,
    yaowei:81.6
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})