/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "sample_comm_nnie.h"
#include "sample_media_ai.h"
#include "ai_infer_process.h"
#include "vgs_img.h"
#include "ive_img.h"
#include "misc_util.h"
#include "hisignalling.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#define STORE_FRM_WIDTH     640
#define STORE_FRM_HEIGHT    384
#define DETECT_OBJ_MAX     32
#define RET_NUM_MAX        4
#define DRAW_RETC_THICK    2    // Draw the width of the line
#define WIDTH_LIMIT        32
#define HEIGHT_LIMIT       32
#define IMAGE_WIDTH        224  // The resolution of the model IMAGE sent to the classification is 224*224
#define IMAGE_HEIGHT       224
#define MODEL_FILE    "/userdata/models/store_yolov3.wk" // darknet framework wk model
#define PIRIOD_NUM_MAX     49 // Logs are printed when the number of targets is detected
#define DETECT_OBJ_MAX     32 // detect max obj

static int biggestBoxIndex;
static IVE_IMAGE_S img;
static DetectObjInfo objs[DETECT_OBJ_MAX] = {0};
static RectBox boxs[DETECT_OBJ_MAX] = {0};
static RectBox objBoxs[DETECT_OBJ_MAX] = {0};
static RectBox remainingBoxs[DETECT_OBJ_MAX] = {0};
static RectBox cnnBoxs[DETECT_OBJ_MAX] = {0}; // Store the results of the classification network
static RecogNumInfo numInfo[RET_NUM_MAX] = {0};
static IVE_IMAGE_S imgIn;
static IVE_IMAGE_S imgDst;
static VIDEO_FRAME_INFO_S frmIn;
static VIDEO_FRAME_INFO_S frmDst;
int uartFd = 0;
static uintptr_t g_STOREModel = 0;

HI_S32 Yolo3STOREDetectLoad(uintptr_t* model)
{
    SAMPLE_SVP_NNIE_CFG_S *self = NULL;
    HI_S32 ret;

    ret = Yolo3Create(&self, MODEL_FILE);
    *model = ret < 0 ? 0 : (uintptr_t)self;
    SAMPLE_PRT("Load STORE detect claasify model success\n");
    /* uart open init */
    uartFd = UartOpenInit();
    
    if (uartFd < 0) {
        printf("uart1 open failed\r\n");
    } else {
        printf("uart1 open successed\r\n");
    }
    return ret;
}

HI_S32 Yolo3STOREDetectUnload(uintptr_t model)
{
    Yolo3Destroy((SAMPLE_SVP_NNIE_CFG_S*)model);
    SAMPLE_PRT("Unload STORE detect claasify model success\n");

    return 0;
}


static HI_S32 STOREDetect(uintptr_t model, IVE_IMAGE_S *srcYuv, DetectObjInfo boxs[])
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    int objNum;
    int ret = Yolo3CalImg(self, srcYuv, boxs, DETECT_OBJ_MAX, &objNum);
    if (ret < 0) {
        SAMPLE_PRT("STORE detect Yolo3CalImg FAIL, for cal FAIL, ret:%d\n", ret);
        return ret;
    }

    return objNum;
}

HI_S32 STOREDetectCal(IVE_IMAGE_S *srcYuv, DetectObjInfo resArr[])
{
    int ret = STOREDetect(g_STOREModel, srcYuv, resArr);
    return ret;
}

/* STORE recognition info */
/*static void STOREDetectFlag(const RecogNumInfo resBuf)
{
    HI_CHAR *objectName = NULL;
    switch (resBuf.num) {
        case 1u:
            objectName = "object Apple";
            UartSendRead(uartFd, 0); // 商品苹果
            SAMPLE_PRT("----object name----:%s\n", objectName);
            break;
        case 2u:
            objectName = "object Drinks";
            UartSendRead(uartFd, 1); // 商品饮料
            SAMPLE_PRT("----object name----:%s\n", objectName);
            break;
        case 3u:
            objectName = "object Cake";
            UartSendRead(uartFd, 2); // 商品蛋糕
            SAMPLE_PRT("----object name----:%s\n", objectName);
            break;
        
        default:
            objectName = "object others";
            UartSendRead(uartFd, 3); // 无效值
            SAMPLE_PRT("----object name----:%s\n", objectName);
            break;
    }
    SAMPLE_PRT("STORE object success\n");
}*/

HI_S32 Yolo3STOREDetectCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm)
{
    SAMPLE_PRT("begin yolo3detectClassifyCal\n");
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    IVE_IMAGE_S img;
    RecogNumInfo resBuf[RET_NUM_MAX] = {0};
    HI_S32 resLen = 0;
    int ret;

    ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);
    SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "STORE detect for YUV Frm to Img FAIL, ret=%#x\n", ret);
    
    ret = Yolo3CalImg(self, &img, resBuf, sizeof(resBuf) / sizeof((resBuf)[0]), &resLen); 
    SAMPLE_CHECK_EXPR_RET(ret < 0, "yolo3 cal FAIL, ret=%d\n", ret);
    HI_ASSERT(resLen <= sizeof(resBuf) / sizeof(resBuf[0]));

    return ret;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
