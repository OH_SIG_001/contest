/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <hi_task.h>
#include <string.h>
#include <hi_wifi_api.h>
#include <hi_mux.h>
#include <hi_io.h>
#include <hi_gpio.h>
#include "iot_config.h"
#include "iot_log.h"
#include "iot_main.h"
#include "iot_profile.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include <math.h>
#include <hi_early_debug.h>
#include <hi_time.h>
#include <hi_adc.h>
#include <hi_stdlib.h>
#include <hi_watchdog.h>
#include <ssd1306_oled.h>
#include <hi_pwm.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "cmsis_os2.h"
#include "iot_i2c.h"
#include "iot_gpio.h"
#include "iot_errno.h"
#include "oled_ssd1306.h"
#include <hisignalling_protocol.h>
#include <hi_uart.h>
#include <app_demo_uart.h>
#include <iot_uart.h>
#include "iot_gpio_ex.h"
#include <app_demo_hx711.h>
extern int classify;
extern double hx711_weight;

#define GapValue 482
UartDefConfig uartDefConfig1 = {0};
/* attribute initiative to report */
#define TAKE_THE_INITIATIVE_TO_REPORT
#define ONE_SECOND                          (1000)
/* oc request id */
#define CN_COMMADN_INDEX                    "commands/request_id="
#define WECHAT_SUBSCRIBE_LIGHT              "light"
#define WECHAT_SUBSCRIBE_LIGHT_ON_STATE     "1"
#define WECHAT_SUBSCRIBE_LIGHT_OFF_STATE    "0"

static void Uart1GpioCOnfig(void)
{
#ifdef ROBOT_BOARD
    IoSetFunc(HI_IO_NAME_GPIO_5, IOT_IO_FUNC_GPIO_5_UART1_RXD);
    IoSetFunc(HI_IO_NAME_GPIO_6, IOT_IO_FUNC_GPIO_6_UART1_TXD);
    /* IOT_BOARD */
#elif defined (EXPANSION_BOARD)
    IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
#endif
}






int g_ligthStatus = 1;
int weight = 0;
char thing0= 'a';
char thing1= 'c';
char thing2= 'd';
char thing3= 'b';
char thing4= 'e';
char thing5= 'f';
char thing6= 'g';
char thing7= 'h';
int price0=18;
int price1=28;
int price2=3;
int price3=4;
int price4=5;
int price5=6;
int price6=7;
int price7=8;

typedef void (*FnMsgCallBack)(hi_gpio_value val);

typedef struct FunctionCallback {
    hi_bool  stop;
    hi_u32 conLost;
    hi_u32 queueID;
    hi_u32 iotTaskID;
    FnMsgCallBack    msgCallBack;
}FunctionCallback;
FunctionCallback g_functinoCallback;

 //CPU Sleep time Set 
/*unsigned int TaskMsleep(unsigned int ms)
{
    if (ms <= 0) {
        return HI_ERR_FAILURE;
    }
    return hi_sleep((hi_u32)ms);
}
*/
static void DeviceConfigInit(hi_gpio_value val)
{
    hi_io_set_func(HI_IO_NAME_GPIO_9, HI_IO_FUNC_GPIO_9_GPIO);
    hi_gpio_set_dir(HI_GPIO_IDX_9, HI_GPIO_DIR_OUT);
    hi_gpio_set_ouput_val(HI_GPIO_IDX_9, val);
}

static int  DeviceMsgCallback(FnMsgCallBack msgCallBack)
{
    g_functinoCallback.msgCallBack = msgCallBack;
    return 0;
}

static void wechatControlDeviceMsg(hi_gpio_value val)
{
    DeviceConfigInit(val);
}

// < this is the callback function, set to the mqtt, and if any messages come, it will be called
// < The payload here is the json string
static void DemoMsgRcvCallBack(int qos, const char *topic, const char *payload)
{
    IOT_LOG_DEBUG("RCVMSG:QOS:%d TOPIC:%s PAYLOAD:%s\r\n", qos, topic, payload);
    /* 云端下发命令后，板端的操作处理 */
    if (strstr(payload, WECHAT_SUBSCRIBE_LIGHT) != NULL) {
        if (strstr(payload, WECHAT_SUBSCRIBE_LIGHT_OFF_STATE) != NULL) {
            wechatControlDeviceMsg(HI_GPIO_VALUE1);
            g_ligthStatus = HI_FALSE;
        } else {
            wechatControlDeviceMsg(HI_GPIO_VALUE0);
            g_ligthStatus = HI_TRUE;
        }
    }
    return HI_NULL;
}

/* publish sample */
hi_void IotPublishSample()
{
    /* reported attribute */
    WeChatProfile weChatProfile = {
        .subscribeType = "type",
        .status.subState = "state",
        .status.subReport = "reported",
        .status.reportVersion = "version",
        .status.Token = "clientToken",
        /* report motor */
        .reportAction.subDeviceActionMotor = "motor",
        .reportAction.motorActionStatus = 0, /* 0 : motor off */

    };

  if (classify==1){
        hi_sleep(500);
        weChatProfile.reportAction.subDeviceActionTemperature = "temperature",
        weChatProfile.reportAction.temperatureData = thing0; /* thing */
        weChatProfile.reportAction.subDeviceActionHumidity = "humidity",

        weight = hx711_weight;
        weChatProfile.reportAction.humidityActionData = weight; /* weight*/
        weChatProfile.reportAction.subDeviceActionLightIntensity = "light_intensity",
        weChatProfile.reportAction.lightIntensityActionData = price0; /* price */



}else if (classify==2){
        hi_sleep(500);
        weChatProfile.reportAction.subDeviceActionTemperature = "temperature",
        weChatProfile.reportAction.temperatureData = thing1; /* thing */
        weChatProfile.reportAction.subDeviceActionHumidity = "humidity",

        weight = hx711_weight;
        weChatProfile.reportAction.humidityActionData = weight; /* weight*/
        weChatProfile.reportAction.subDeviceActionLightIntensity = "light_intensity",
        weChatProfile.reportAction.lightIntensityActionData = price1;  /* price */



}else if (classify==3){
        hi_sleep(500);
        weChatProfile.reportAction.subDeviceActionTemperature = "temperature",
        weChatProfile.reportAction.temperatureData = thing2; /* thing */
        weChatProfile.reportAction.subDeviceActionHumidity = "humidity",
        weight = hx711_weight;
        weChatProfile.reportAction.humidityActionData = weight; /* weight*/
        weChatProfile.reportAction.subDeviceActionLightIntensity = "light_intensity",
        weChatProfile.reportAction.lightIntensityActionData = price2; /* price */

}else if (classify==4){
        hi_sleep(1000);
        weChatProfile.reportAction.subDeviceActionTemperature = "temperature",
        weChatProfile.reportAction.temperatureData = thing3; /* thing */
        weChatProfile.reportAction.subDeviceActionHumidity = "humidity",
        weight = hx711_weight;
        weChatProfile.reportAction.humidityActionData = weight; /* weight*/
        weChatProfile.reportAction.subDeviceActionLightIntensity = "light_intensity",
        weChatProfile.reportAction.lightIntensityActionData = price3; /* price */

}else if (classify==5){
        hi_sleep(1000);
        weChatProfile.reportAction.subDeviceActionTemperature = "temperature",
        weChatProfile.reportAction.temperatureData = thing4; /* thing */
        weChatProfile.reportAction.subDeviceActionHumidity = "humidity",
        weight = hx711_weight;
        weChatProfile.reportAction.humidityActionData = weight; /* weight*/
        weChatProfile.reportAction.subDeviceActionLightIntensity = "light_intensity",
        weChatProfile.reportAction.lightIntensityActionData = price4; /* price */

}else if (classify==6){
        hi_sleep(1000);
        weChatProfile.reportAction.subDeviceActionTemperature = "temperature",
        weChatProfile.reportAction.temperatureData = thing5; /* thing */
        weChatProfile.reportAction.subDeviceActionHumidity = "humidity",
        weight = hx711_weight;
        weChatProfile.reportAction.humidityActionData = weight; /* weight*/
        weChatProfile.reportAction.subDeviceActionLightIntensity = "light_intensity",
        weChatProfile.reportAction.lightIntensityActionData = price5; /* price */

}else if (classify==7){
        hi_sleep(1000);
        weChatProfile.reportAction.subDeviceActionTemperature = "temperature",
        weChatProfile.reportAction.temperatureData = thing6; /* thing */
        weChatProfile.reportAction.subDeviceActionHumidity = "humidity",
        weight = hx711_weight;
        weChatProfile.reportAction.humidityActionData = weight; /* weight*/
        weChatProfile.reportAction.subDeviceActionLightIntensity = "light_intensity",
        weChatProfile.reportAction.lightIntensityActionData = price6; /* price */

}else if (classify==8){
        hi_sleep(1000);
        weChatProfile.reportAction.subDeviceActionTemperature = "temperature",
        weChatProfile.reportAction.temperatureData = thing7; /* thing */
        weChatProfile.reportAction.subDeviceActionHumidity = "humidity",
        weight = hx711_weight;
        weChatProfile.reportAction.humidityActionData = weight; /* weight*/
        weChatProfile.reportAction.subDeviceActionLightIntensity = "light_intensity",
        weChatProfile.reportAction.lightIntensityActionData = price7; /* price */

}
    /* report light */
    if (g_ligthStatus == HI_TRUE) {
        weChatProfile.reportAction.subDeviceActionLight = "light";
        weChatProfile.reportAction.lightActionStatus = 1; /* 1: light on */
    } else if (g_ligthStatus == HI_FALSE) {
        weChatProfile.reportAction.subDeviceActionLight = "light";
        weChatProfile.reportAction.lightActionStatus = 0; /* 0: light off */
    } else {
        weChatProfile.reportAction.subDeviceActionLight = "light";
        weChatProfile.reportAction.lightActionStatus = 0; /* 0: light off */
    }



    /* profile report */
    IoTProfilePropertyReport(CONFIG_USER_ID, &weChatProfile);
}

// < this is the demo main task entry,here we will set the wifi/cjson/mqtt ready and
// < wait if any work to do in the while
static hi_void *DemoEntry(const char *arg)
{
    WifiStaReadyWait();
    cJsonInit();
    IoTMain();
    /* 云端下发回调 */
    IoTSetMsgCallback(DemoMsgRcvCallBack);
    /* 主动上报 */
#ifdef TAKE_THE_INITIATIVE_TO_REPORT
    while (1) {
        /* 用户可以在这调用发布函数进行发布，需要用户自己写调用函数 */
        IotPublishSample(); // 发布例程
#endif
        TaskMsleep(ONE_SECOND);
    }
    return NULL;
}

// < This is the demo entry, we create a task here,
// and all the works has been done in the demo_entry
#define CN_IOT_TASK_STACKSIZE  0x1000
#define CN_IOT_TASK_PRIOR 25
#define CN_IOT_TASK_NAME "IOTDEMO"

static void AppDemoIot(void)
{
    osThreadAttr_t attr;
    IoTWatchDogDisable();

    attr.name = "IOTDEMO";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = CN_IOT_TASK_STACKSIZE;
    attr.priority = CN_IOT_TASK_PRIOR;

    if (osThreadNew((osThreadFunc_t)DemoEntry, NULL, &attr) == NULL) {
        printf("[mqtt] Falied to create IOTDEMO!\n");
    }
}


SYS_RUN(AppDemoIot);