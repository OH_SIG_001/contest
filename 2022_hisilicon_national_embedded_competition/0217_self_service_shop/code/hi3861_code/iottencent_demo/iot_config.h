/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef IOT_CONFIG_H
#define IOT_CONFIG_H

// <CONFIG THE LOG
/* if you need the iot log for the development , please enable it, else please comment it */
#define CONFIG_LINKLOG_ENABLE   1

// < CONFIG THE WIFI
/* Please modify the ssid and pwd for the own */
#define CONFIG_AP_SSID  "zy" // WIFI SSID
#define CONFIG_AP_PWD   "zy123456" // WIFI PWD
/* Tencent iot Cloud user ID , password */
#define CONFIG_USER_ID    "JA4SVWGJZChi3861;12010126;d95b8;1664726400"
#define CONFIG_USER_PWD   "96d830e23349469081a4a84bce4a462dd52714a78b07439cfcb8e5a102d24d7b;hmacsha256"
#define CN_CLIENTID     "JA4SVWGJZChi3861" // Tencent cloud ClientID format: Product ID + device name
#endif