// pages/cart/cart.js
let action = '';
let moveY = 200;
let animation = animation = wx.createAnimation({
  transformOrigin: "50% 50%",
  duration: 400,
  timingFunction: "ease",
  delay: 0
});
animation.translateY(moveY + 'vh').step();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    backgroundVisible: false,
    animation: animation,
    bg: 'background',
    totalPrice: '68.00', // 总价
  
    commodityListData: [{
        src: "http://a1.qpic.cn/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/05RlWl8gsTOH*Z17MtCBzKphEXeOUFBWkP2Y09zWSavuVijgh8051i1EOLROofrCEiThNTxbAU40Tefu9sIzfA!!/c&ek=1&kp=1&pt=0&bo=3AGRAtwBkQIRADc!&tl=1&tm=1654257600&dis_t=1654259114&dis_k=f42cc6c8d7ea3e62f89a9655aaf6ff76&sce=0-12-12&rf=0-18",
        eName: "Silent Night ",
        zhName: "平安夜",
        weight: "908",
        lb: "2.0",
        price: "68.00",
        dinnerNum: "5",
        amount: 1
      },
    
      
    ], // 购物车商品数据
    adjunctsListData: [{
        src: "http://a1.qpic.cn/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/05RlWl8gsTOH*Z17MtCBzIzx42AdZDSzM2B0Ir2YDVyd5*RFpiLI96tZEbSq4WGDlOLWxb7d0X56edkXanLXXw!!/b&ek=1&kp=1&pt=0&bo=6ANxAugDcQIRADc!&tl=1&tm=1654308000&dis_t=1654308641&dis_k=0c1a7e85fb632b89b97a916aa8684b6a&sce=0-12-12&rf=0-18",
        name: "生日蜡烛",
        price: "10.00",
      },
      {
        src: "http://a1.qpic.cn/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/05RlWl8gsTOH*Z17MtCBzJ9Go*rS9Yc1pUvxHnMXih9Ur4k0Ul5970uoQpdar53ozAnlIufV*XL2r74lf73ilQ!!/b&ek=1&kp=1&pt=0&bo=DQP0AQ0D9AERADc!&tl=1&tm=1654308000&dis_t=1654308641&dis_k=7799c8aa2d0c49e75395ed2ae1ef4942&sce=0-12-12&rf=0-18",
        name: "手提袋",
        price: "2.00",
      },
    ], // 搭配商品数据
    recommendListData: [{
        src: "http://59.110.237.148/tsy/static_file/applets_21cake/images/58c4227141bbcf6efe30b5940267a1a9.jpg",
        name: "米道",
        price: "298.00",
        lb: "1.0",
        weight: "454"
      },
      {
        src: "http://59.110.237.148/tsy/static_file/applets_21cake/images/58c4227141bbcf6efe30b5940267a1a9.jpg",
        name: "米道",
        price: "298.00",
        lb: "1.0",
        weight: "454"
      },
      {
        src: "http://59.110.237.148/tsy/static_file/applets_21cake/images/58c4227141bbcf6efe30b5940267a1a9.jpg",
        name: "米道",
        price: "298.00",
        lb: "1.0",
        weight: "454"
      },
      {
        src: "http://59.110.237.148/tsy/static_file/applets_21cake/images/58c4227141bbcf6efe30b5940267a1a9.jpg",
        name: "米道",
        price: "298.00",
        lb: "1.0",
        weight: "454"
      },
    ], // 推荐商品
    
    specData: [{
      "id": "01",
      "size": "13x13cm",
      "suite_amount": "\u9002\u54083-4\u4eba\u5206\u4eab",
      "booking_hour_limited": "5",
      "cutlery_content": "\u542b5\u5957\u9910\u5177\uff08\u8721\u70db\u9700\u5355\u72ec\u8ba2\u8d2d\uff09",
      "price": "68.00",
      "mktprice": "68.00",
      "spec": "1.0\u78c5",
      "pound": "1.00",
      "has_spec_img": 1,
      "img_url": "\/themes\/wap\/img\/1.00P-full-13.00.jpg",
      "is_default": "true",
      "has_stock": 9997,
      "cat_id": "3",
      "bn": "201408-10",
      "enName": "Black Forest ",
      "simpleName": "\u9ed1\u68ee\u6797",
      "cakeGoodsId": "8",
      "weight": 454,
      "shipTime": {
        "date": "\u660e\u5929",
        "start_time": "09:30",
        "end_time": "21:30"
      }
    }, {
      "id": "239",
      "size": "17x17cm",
      "suite_amount": "\u9002\u54087-8\u4eba\u5206\u4eab",
      "booking_hour_limited": "5",
      "cutlery_content": "\u542b10\u5957\u9910\u5177\uff08\u8721\u70db\u9700\u5355\u72ec\u8ba2\u8d2d\uff09",
      "price": "398.00",
      "mktprice": "398.00",
      "spec": "2.0\u78c5",
      "pound": "2.00",
      "has_spec_img": 1,
      "img_url": "\/themes\/wap\/img\/2.00P-full-17.00.jpg",
      "is_default": "false",
      "has_stock": 9996,
      "cat_id": "3",
      "bn": "201408-20",
      "enName": "Black Forest ",
      "simpleName": "\u9ed1\u68ee\u6797",
      "cakeGoodsId": "8",
      "weight": 908,
      "shipTime": {
        "date": "\u660e\u5929",
        "start_time": "09:30",
        "end_time": "21:30"
      }
    }, {
      "id": "245",
      "size": "23x23cm",
      "suite_amount": "\u9002\u540811-12\u4eba\u5206\u4eab",
      "booking_hour_limited": "5",
      "cutlery_content": "\u542b15\u5957\u9910\u5177\uff08\u8721\u70db\u9700\u5355\u72ec\u8ba2\u8d2d\uff09",
      "price": "598.00",
      "mktprice": "598.00",
      "spec": "3.0\u78c5",
      "pound": "3.00",
      "has_spec_img": 1,
      "img_url": "\/themes\/wap\/img\/3.00P-full-23.00.jpg",
      "is_default": "false",
      "has_stock": 9999,
      "cat_id": "3",
      "bn": "201408-30",
      "enName": "Black Forest ",
      "simpleName": "\u9ed1\u68ee\u6797",
      "cakeGoodsId": "8",
      "weight": 1362,
      "shipTime": {
        "date": "\u660e\u5929",
        "start_time": "09:30",
        "end_time": "21:30"
      }
    }, {
      "id": "251",
      "size": "30x30cm",
      "suite_amount": "\u9002\u540815-20\u4eba\u5206\u4eab",
      "booking_hour_limited": "24",
      "cutlery_content": "\u542b20\u5957\u9910\u5177\uff08\u8721\u70db\u9700\u5355\u72ec\u8ba2\u8d2d\uff09",
      "price": "958.00",
      "mktprice": "958.00",
      "spec": "5.0\u78c5",
      "pound": "5.00",
      "has_spec_img": 1,
      "img_url": "\/themes\/wap\/img\/5.00P-full-30.00.jpg",
      "is_default": "false",
      "has_stock": 9997,
      "cat_id": "3",
      "bn": "201408-50",
      "enName": "Black Forest ",
      "simpleName": "\u9ed1\u68ee\u6797",
      "cakeGoodsId": "8",
      "weight": 2270,
      "shipTime": {
        "date": "\u660e\u5929",
        "start_time": "09:30",
        "end_time": "21:30"
      }
    }], // 规格数据
    specCurrent: 1, // 选中规格
  },

  // 底部详情弹出模态框 规格确认事件
  confirm: function() {
    this.hidden();
  },

  // 底部详情弹出模态框 规格点击选中
  onSpecActive: function(e) {
    this.setData({
      specCurrent: e.currentTarget.dataset.index
    });
  },

  // 点击失效
  /*clickInvalid: function() {
    return false;
  },*/

  //移动按钮点击事件
  showPop: function(e) {
    moveY = 0;
    action = 'show';
    animationEvents(this, moveY, action);
  },
  //隐藏弹窗浮层
  hidden: function(e) {
    moveY = 200;
    action = 'hide';
    animationEvents(this, moveY, action);
  },

  // 全部删除
  onAllDel: function() {
    this.setData({
      commodityListData: [],
    });
  },


  // 监听子组件删除商品事件
  onDel: function(e) {
    let commodityListData = this.data.commodityListData;
    commodityListData.splice(e.detail.index, 1);
    this.setData({
      commodityListData: commodityListData
    });
  },

  // 监听子组件减少商品事件
  onLess: function(e) {
    let param = {};
    let string = "commodityListData[" + e.detail.index + "].amount";
    param[string] = --this.data.commodityListData[e.detail.index].amount;
    this.setData(param);
  },

  // 监听子组件添加商品事件
  onAdd: function(e) {
    let param = {};
    let string = "commodityListData[" + e.detail.index + "].amount";
    param[string] = ++this.data.commodityListData[e.detail.index].amount;
    this.setData(param);
  },

  // 去购物 跳转分类页面
  onGoShopping: function() {
    wx.switchTab({
      url: '/pages/classify/classify',
    })
  },

     // 支付页面
     handlePay: function() {
      wx.navigateTo({
        url: '/pages/pay/index',
      })
    },
   
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
      return {
        title: res.target.dataset.shareinfo.title,
        path: res.target.dataset.shareinfo.path,
        imageUrl: res.target.dataset.shareinfo.imageUrl
      }
    } else {
      return {
        title: '全新品种蛋糕，等你来尝哦！',
        path: 'pages/home/home',
        imageUrl: "http://59.110.237.148/tsy/static_file/applets_21cake/images/banner2.jpg"
      }
    }
  }

  
})
//动画事件 底部的弹出，背景层通过切换不同的class，添加一个transition的效果，使之有一个渐变的感觉。
function animationEvents(that, moveY, action) {
  that.animation = wx.createAnimation({
    transformOrigin: "50% 50%",
    duration: 400,
    timingFunction: "ease",
    delay: 0
  })
  that.animation.translateY(moveY + 'vh').step()
  if (action == 'show') {
    that.setData({
      animation: that.animation.export(),
      show: true,
      backgroundVisible: true,
      bg: 'bg',
      disableScroll: 'disableScroll'
    });
  } else if (action == 'hide') {
    that.setData({
      animation: that.animation.export(),
      show: false,
      backgroundVisible: false,
      bg: 'background',
      disableScroll: ''
    });
  }
}