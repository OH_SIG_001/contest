/* pages/cart/cart.wxss */

.cart {
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  background: #f6f6f6;
}

.main {
  flex: 1;
  display: flex;
  flex-direction: column;
}

.commodityList {
  flex: 1;
  overflow-y: scroll;
}

.commodityList .tipsInfo {
  height: 80rpx;
  line-height: 80rpx;
  text-align: center;
  font-size: 24rpx;
  color: #442818;
  margin-bottom: 20rpx;
}

.settleColumn {
  height: 88rpx;
  overflow: hidden;
  background: #fff;
  width: 100%;
  box-shadow: 0rpx -4rpx 8rpx 0rpx rgba(0, 0, 0, 0.10);
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.settleColumn image {
  display: block;
  height: 35rpx;
  width: 35rpx;
  margin-left: 25rpx;
}

.settleColumn .dec {
  display: flex;
}

.settleColumn .dec .settleBtn {
  display: block;
  height: 88rpx;
  width: 200rpx;
  line-height: 88rpx;
  text-align: center;
  color: #fff;
  background: #442818;
  font-size: 24rpx;
}

.settleColumn .dec .total {
  margin-right: 24rpx;
}

.settleColumn .dec .total .totalPrice {
  margin-top: 10rpx;
  display: block;
  font-size: 32rpx;
  font-weight: bold;
  line-height: 32rpx;
  color: #442818;
  text-align: right;
}

.settleColumn .dec .total text {
  display: block;
  color: #707070;
  font-size: 20rpx;
  line-height: 32rpx;
  text-align: right;
}

.empty {
  flex: 1;
  overflow-y: scroll;
  position: relative;
}

.empty .content {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

.empty .content image {
  height: 300rpx;
  width: 300rpx;
  display: block;
  margin: 0 auto;
}

.empty .content text {
  display: block;
}

.empty .content .dec {
  margin: 20rpx 0;
  font-size: 28rpx;
  color: #d5bfa7;
  text-align: center;
}

.empty .content .goShopping {
  color: #442818;
  width: 240rpx;
  margin: 0 auto;
  display: block;
  text-align: center;
  font-size: 24rpx;
}

.adjunctsList {
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding: 0 10rpx;
}

.adjunctsItem {
  margin-bottom: 40rpx;
}

.cartTopTip {
  height: 80rpx;
  line-height: 80rpx;
  text-align: center;
  font-size: 24rpx;
  color: #442818;
  margin-bottom: 20rpx;
}

.suggestgoodsList {
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding: 0 10rpx;
}

.suggestgoodsList .recommendItem {
  margin-bottom: 40rpx;
}

/* 底部弹出模态框代码 */

.element-wrapper {
  display: flex;
  position: fixed;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  overflow: hidden;
}

.background {
  background-color: rgba(0, 0, 0, 0);
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: 0;
}

.bg {
  background-color: rgba(0, 0, 0, 0.4);
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: 0;
  transition-property: background-color;
  transition-timing-function: ease;
  transition-duration: 1s;
  transition-delay: 0;
}

.element {
  position: fixed;
  width: 100%;
  bottom: 0;
  background-color: rgba(255, 255, 255, 1);
  box-shadow: 0rpx -4rpx 8rpx 0rpx rgba(0, 0, 0, 0.1);
}

.element .picker_header>image {
  height: 30rpx;
  width: 30rpx;
  padding: 30rpx;
  position: absolute;
  top: 0;
  right: 0;
}

.element .left-bt {
  left: 5%;
  font-size: 34rpx;
  color: #888;
  line-height: 34rpx;
}

.element .right-bt {
  right: 5%;
  font-size: 34rpx;
  color: #888;
  text-align: right;
  line-height: 34rpx;
}

.element .line {
  display: block;
  position: fixed;
  height: 1px;
  width: 100%;
  margin-top: 89rpx;
  background-color: #eee;
}

.pickerContent {
  padding: 35rpx 48rpx 20rpx 48rpx;
  font-size: 24rpx;
  color: #442818;
}

.pickerContent .detailsPrice {
  font-size: 30rpx;
  line-height: 44rpx;
  margin-bottom: 30rpx;
  font-weight: bold;
  color: #442818;
}

.pickerContent .detailsDecBox {
  margin-bottom: 20rpx;
  display: flex;
  padding-top: 20rpx;
}

.pickerContent .detailsDecBox>image {
  display: block;
  min-width: 50%;
  height: 200rpx;
}

.pickerContent .detailsDecBox .detailsList {
  display: flex;
  flex-direction: column;
  margin-left: 30rpx;
  min-width: 50%;
}

.pickerContent .detailsDecBox .detailsList .item {
  display: flex;
  margin-bottom: 8rpx;
  align-items: center;
}

.pickerContent .detailsDecBox .detailsList .item image {
  display: block;
  width: 30rpx;
  height: 30rpx;
  margin-right: 20rpx;
}

.pickerContent .detailsDecBox .detailsList .item text {
  display: block;
  font-size: 22rpx;
  color: #442818;
}

.box .title {
  margin-top: 20rpx;
}

.box .list {
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin-top: 10rpx;
}

.box .list .item {
  box-sizing: border-box;
  width: 23%;
  display: flex;
  flex-wrap: wrap;
  line-height: 28rpx;
  align-items: center;
  justify-content: center;
  background-color: #f7f7f7;
  height: 64rpx;
  text-align: center;
  margin-left: 2%;
  margin-bottom: 20rpx;
  padding: 0 1%;
  position: relative;
  border: 2rpx solid transparent;
}

.box .list .item.active {
  border: 2rpx solid #442818;
  background-color: #fff;
}

.box .list .item .weigth {
  width: 100%;
  text-align: center;
}

.box .list .item .lb {
  width: 100%;
  text-align: center;
}

.box .list .item .taste {
  width: 100%;
  text-align: center;
}

.box .list .item image {
  position: absolute;
  top: 0;
  left: 0;
  height: 24rpx;
  width: 24rpx;
  opacity: 0;
}

.box .list .item image.active {
  opacity: 1;
}

.poundupBtn {
  display: flex;
  align-items: center;
  height: 100rpx;
  width: 100%;
  text-align: center;
  font-size: 30rpx;
  box-shadow: 0rpx -4rpx 8rpx 0rpx rgba(0, 0, 0, 0.1);
}

.poundupBtn>text {
  width: 50%;
  display: block;
  height: 100rpx;
  line-height: 100rpx;
}

.poundupBtn .cancel {
  color: #442818;
  background-color: #fff;
}

.poundupBtn .confirm {
  color: #fff;
  background-color: #442818;
}
