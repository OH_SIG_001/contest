# 强化学习驱动的基于面部追踪的远程会议系统

## 项目介绍

​	当今的世界，依旧没有摆脱疫情对各行各业的冲击，其影响之一就是线上会议这一形式被越来越多的公司或团队采用。 而在视频会中，如何使用摄像头来对与会者的人脸进行全面的摄录，一直都是影响线上会议质量的关键因素。

​	我们针对这一痛点，基于海思 Taurus & Pegasus 套件与 OpenHarmony 嵌入式系统，设计了一套融合深度强化学习的 6 轴机械臂姿态解算算法的线上会议人脸追焦辅助系统。在这套系统的帮助下，无论与会者采用何种设备参加会议，都能实现全自动的人脸追焦，从而让与会者的面容始终清晰端正地保持在画面中央，显著提升视频会议的通话质量与入会体验。


## 仓库结构

- *[文件夹]* **taurus_meeting**:
  - 摄像头录制视频抽帧、检测网推理、识别信息上传代码
- *[文件夹]* **pegasus_meeting**:
  - 姿态解算与下位机控制代码
- *[文件夹]* **RL**:
  - DDPG 算法实现代码
  - 作为对照组的状态机控制代码
  - 使用 DDPG 的深度强化学习控制代码
  - 基于 SOCKET 包装的智能体交互环境代码
- *[文件夹]* **power_supply**:
  - PCB 原理图
  - PCB 板图
- **humanface_detect_inst.wk**: 训练好的人脸识别模型 .wk 文件


## 运行配置

- Taurus
  - Taurus 使用 IEEE 802.3 协议进行有线入网，无需修改网络配置，默认采用 DHCP 即可
  - TCP SOCKET 配置了边缘服务器的 IP 与 `3516` 端口
  - 该目录下的代码应该被放入 <code>hi3516/device/soc/hisilicon/hi3516dv300/sdk_linux/sample/taurus</code> 下，并对 <code>hi3516/device/soc/hisilicon/hi3516dv300/sdk_linux</code> 下的 <code>BUILD.gn</code> 进行如下修改：
  
  ```bash
  # Copyright (C) 2021 Hisilicon (Shanghai) Technologies Co., Ltd. All rights reserved.
  group("hispark_taurus_sdk") {
    if (defined(ohos_lite)) {
      deps = [
        ":sdk_linux_lite_libs",
        ":sdk_make",
        "//kernel/linux/build:linux_kernel",
        "sample/taurus/taurus_meeting:hi3516dv300_ai_sample",
      ]
    } else {
      deps = [
        ":sdk_linux_modules",
        ":sdk_make",
        "out/lib:sdk_linux_mpp_group",
        "//kernel/linux/build:linux_kernel",
        "sample/taurus/taurus_meeting:hi3516dv300_ai_sample",
      ]
    }
  }

  # 其他保持不变
  ```
  - 编译完成后，在 <code>hi3516/out/hispark_taurus/ipcamera_hispark_taurus_linux/bin</code> 下找到 <code>ohos_camera_ai_demo</code> 二进制运行文件，使用 SD 卡拷贝到 Taurus 板卡中进行运行。

- Pegasus
  - Pegasus 使用 IEEE 802.11 进行无线入网，需要手动修改配置的 SSID 与密码
  - TCP SOCKET 配置了边缘服务器的 IP 与 `3861` 端口
  - 该目录应该被放入 <code>hi3861/applications/sample/wifi-iot/app</code> 下，并且修改同目录下的 <code>BUILD.gn</code> 为如下形式:

  ```bash
  import("//build/lite/config/component/lite_component.gni")

  lite_component("app") {
    features = [ "pegasus_meeting:netDemo", ]
  }
  ```
  - 编译完成后，直接烧录至 Hi3861 板子上即可


- 边缘服务器

  - 边际网关的 IP 配置为 `192.168.0.1/16`

  - 边缘服务器端 IP 配置为 `192.168.10.178/16`

  - 边缘服务器确保 `3861` 与 `3516` 端口为空闲状态

  - 软件运行环境：

    ```
    python									3.8
    gym                     0.24.1
    numpy                   1.22.3
    pandas                  1.4.3
    PySocks                 1.7.1
    tensorboard             2.9.1
    tensorboardX            2.2
    torch                   1.11.0
    torchvision             0.12.0
    ```

## 程序启动方法

<div align="center"><img src="https://raw.githubusercontent.com/coolmoon327/picBed/master/pictures20220714141605.png" style="zoom: 67%;"></div>

- 在接入网络正常运行的条件下，首先在边缘服务器上使用 `python main.py` 启动决策服务

- 在 Taurus 板端挂载 .wk 文件的存储介质，使用命令启动服务：

  - ```bash
    # 安装内核模块
    cd /ko
    insmod ./hi_mipi_tx.ko
    
    # 启动主程序 
    cd /userdata
    ./ohos_camera_ai_demo 1
    ```

- 按下 Pegasus 主控板上的 reset 按键重启服务