本作品基于 Hi3516DV300 平台，通过摄像头拍
摄分拣区域的实时图像，并利用板端部署的神经网络算法对其进行检测，最终得出其中医疗垃圾的种类和具体位置。之后，通过一系列坐标变化算法，系统可发出相关控制指令，利用机械臂对相应的医疗垃圾进行抓取并分拣至指定区域，最终完成分类医疗垃圾的目的。