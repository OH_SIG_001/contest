#ifndef READ_KIT_DATA_H
#define READ_KIT_DATA_H

#include "sample_comm.h"

#ifdef __cplusplus
extern "C" {
#endif

#define Users_Num_MAX 10
#define Drug_Num_MAX  10

typedef struct PeopleInf{
   int  ID;                    //People ID
   char Name[100];             //Name
   int  Empty;                 //Enabled or not               
   int  DrugNum;               //Number of medication types taken
   int  DrugID[10];            //Drug ID
   int  Times[10];             //Number of doses per day
   int  TimeOut[10];           //Alarm time(min)
   int  DosageTime[10][10];    //Drug ID:Dosing time(0~23h)
} PeopleInf;

typedef struct DrugInf{
   int  ID;                   //Drug ID
   char Name[100];            //Name
   int  Empty;                //Enabled or not               
   int  DrugTotal;            //Total number of drugs
} DrugInf;

#ifdef __cplusplus
}
#endif

#endif // READ_KIT_DATA_H
