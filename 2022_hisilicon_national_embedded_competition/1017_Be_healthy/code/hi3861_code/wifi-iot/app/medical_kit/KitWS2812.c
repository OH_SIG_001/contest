#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"

#include <app_demo_spi.h>
#include "wifiiot_gpio.h"
#include "wifiiot_spi.h"
#include "wifiiot_gpio_ex.h"
#include "KitWS2812.h"

#define RED_INDEX   0
#define GREEN_INDEX 1
#define BLUE_INDEX  2

int first_wakeup_flag = 0;
unsigned char wakeup_color[3]={0,0,255};
//色带
unsigned char color[8][3]=
{
    {255,255,255},//白
    {255,255, 0 },
    {255, 0 ,255},
    { 0 ,255,255},
    { 0 , 0 ,255},//蓝
    { 0 ,255, 0 },//绿
    {255, 0 , 0 },//红
    { 0 , 0 , 0 }
};
void Kit_LED_init(void)
{   
    //初始化GPIO
    IoTGpioInit(HI_IO_NAME_GPIO_9);
    
    hi_spi_deinit(HI_SPI_ID_0); /* if wake_up from deep sleep, should deinit first */
    hi_spi_cfg_basic_info spi_cfg_basic_info;
    spi_cfg_basic_info.cpha = 0;
    spi_cfg_basic_info.cpol = 0;
    spi_cfg_basic_info.data_width = HI_SPI_CFG_DATA_WIDTH_E_8BIT;
    spi_cfg_basic_info.endian = 0;
    spi_cfg_basic_info.fram_mode = 0;
    spi_cfg_basic_info.freq = 8*900000; 
    hi_spi_cfg_init_param spi_init_param = {0};
    spi_init_param.is_slave = 0;
    hi_spi_init(HI_SPI_ID_0, spi_init_param, &spi_cfg_basic_info); //基本参数配置
    hi_spi_set_loop_back_mode(HI_SPI_ID_0, 1);
    hi_io_set_func(HI_IO_NAME_GPIO_9, HI_IO_FUNC_GPIO_9_SPI0_TXD);
    IoTGpioSetDir(HI_IO_NAME_GPIO_9, WIFI_IOT_GPIO_DIR_OUT);
    hi_spi_set_irq_mode(HI_SPI_ID_0, 0);
    hi_spi_set_dma_mode(HI_SPI_ID_0, 1);
}

//功能：把 3BYTE 的RGB数据转换成 24BYTE SPI数据
static void WS2812_send(unsigned char *rgb)
{
    unsigned char data[24];
    unsigned char i, bit;
    unsigned char index = 0;
        for(i = 0; i < 8; i++) // GREEN  按位发送
        {
            bit = ((rgb[GREEN_INDEX] << i) & 0x80);
            data[index] = (bit == 0x80) ? 0xfc : 0xe0;//spi发送0xfc 为ws812 1码 spi发送0xe0 为ws812 0码
            index++;
        } 
        for(i = 0; i < 8; i++) // RED
        {
            bit = ((rgb[RED_INDEX]<<i) & 0x80);
            data[index] = (bit==0x80) ? 0xfc : 0xe0;
            index++;
        }
        for(i=0; i<8; i++) // BLUE
        {
            bit = ((rgb[BLUE_INDEX]<<i) & 0x80);
            data[index] = (bit==0x80) ? 0xfc : 0xe0;
            index++;
        }
        hi_spi_host_write(WIFI_IOT_SPI_ID_0, data, 24);
}
void Kit_LED_State(int *state)
{
    unsigned char data[50];
    unsigned char rgb[3];
    memset(data, 0, sizeof(data));
    if(*state == LED_WAKE_UP)
    {
        
        if(!first_wakeup_flag)
        {    
            
            for (unsigned int j = 0; j < 22; j ++)//亮度
            {   
                hi_spi_host_write(WIFI_IOT_SPI_ID_0, data, sizeof(data));//前50byte为低电平break
                for(int i=0;i<22;i++)
                {
                    
                    if(i > j)
                    {
                        rgb[0] = wakeup_color[0]&0;//色带
                        rgb[1] = wakeup_color[1]&0;
                        rgb[2] = wakeup_color[2]&0;
                    }
                    else 
                    {
                        rgb[0] = wakeup_color[0]&255;//色带
                        rgb[1] = wakeup_color[1]&255;
                        rgb[2] = wakeup_color[2]&255;
                    }
                    WS2812_send(rgb);
                }
                for(int i=0;i<22;i++)
                {
                    if(i < (22-j))
                    {
                        rgb[0] = wakeup_color[0]&0;//色带
                        rgb[1] = wakeup_color[1]&0;
                        rgb[2] = wakeup_color[2]&0;
                    }
                    else 
                    {
                        rgb[0] = wakeup_color[0]&255;//色带
                        rgb[1] = wakeup_color[1]&255;
                        rgb[2] = wakeup_color[2]&255;
                    }
                    WS2812_send(rgb);
                }
                usleep(50 * 1000);
            } 
            first_wakeup_flag = 1;
        }   
        for (unsigned int j = 255; j > 0; j --)//亮度
        {   
            hi_spi_host_write(WIFI_IOT_SPI_ID_0, data, sizeof(data));//前50byte为低电平break
            rgb[0] = wakeup_color[0]&j;//色带
            rgb[1] = wakeup_color[1]&j;
            rgb[2] = wakeup_color[2]&j;
            if(*state == LED_WAKE_UP)
            {    for (unsigned int k = 0; k < 44; k ++)//灯数
                    WS2812_send(rgb);
                usleep(5 * 1000);
            }
            // if(*state != LED_WAKE_UP)
            // {
            //     j=0;
            // }
        }

        for (unsigned int j = 0; j < 256; j ++)//亮度
        {
            hi_spi_host_write(WIFI_IOT_SPI_ID_0, data, sizeof(data));//前50byte为低电平break
            rgb[0] = wakeup_color[0]&j;//色带
            rgb[1] = wakeup_color[1]&j;
            rgb[2] = wakeup_color[2]&j;
            if(*state == LED_WAKE_UP)   
           { 
               for (unsigned int k = 0; k < 44; k ++)//灯数
                WS2812_send(rgb);
            usleep(5 * 1000);
           }
            // if(*state != LED_WAKE_UP)
            // {
            //     j=256;
            // }
        }
        // if(*state == LED_WAKE_UP)
        //     usleep(5000000);
    }
    else if(*state == LED_BREATH)
    {
             
        for (int i = 0; i < 256; i ++)                  //几种颜色渐变   呼吸灯
        {
            for (unsigned int j = 0; j < 256; j ++)//亮度
            {
                if(*state == LED_BREATH)
                {
                    hi_spi_host_write(WIFI_IOT_SPI_ID_0, data, sizeof(data));//前50byte为低电平break
                    rgb[0] = color[i%7][0]&j;//色带
                    rgb[1] = color[i%7][1]&j;
                    rgb[2] = color[i%7][2]&j;
                    for (unsigned int k = 0; k < 44; k ++)//控制30个灯
                        WS2812_send(rgb);
                    usleep(10 * 1000);
                }
            }  
            for (unsigned int j = 255; j > 0; j --)//亮度
            {
                if(*state == LED_BREATH)
                {
                    hi_spi_host_write(WIFI_IOT_SPI_ID_0, data, sizeof(data));//前50byte为低电平break
                    rgb[0] = color[i%7][0]&j;//色带
                    rgb[1] = color[i%7][1]&j;
                    rgb[2] = color[i%7][2]&j;
                    for (unsigned int k = 0; k < 44; k ++)//控制30个灯
                        WS2812_send(rgb);
                    usleep(10 * 1000);
                }
            }  
        }
    }
    else if(*state == LED_EXIT)
    {   
        hi_spi_host_write(WIFI_IOT_SPI_ID_0, data, sizeof(data));//前50byte为低电平break
        rgb[0] = wakeup_color[0]&0;//色带
        rgb[1] = wakeup_color[1]&0;
        rgb[2] = wakeup_color[2]&0;

        for (unsigned int k = 0; k < 44; k ++)//灯数
            WS2812_send(rgb);

        usleep(5 * 1000);
    }
    
}