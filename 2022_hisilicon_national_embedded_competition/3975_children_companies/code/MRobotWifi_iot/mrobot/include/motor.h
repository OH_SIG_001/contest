/**
 * @file motor.h
 * @author hellokun(www.hellokun.cn)
 * @brief 
 * @version 0.1
 * @date 2022-03-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once 
#ifndef __MORTOR_H__
#define __MORTOR_H__
#define IN1 5
#define IN2 6  
#define IN3 7
#define IN4 8   //IN1-IN4 motor_driver control IO

void Motor_Init();
void Forward(unsigned int circle_time);
void Backward(unsigned int circle_time);
void Turn_left(unsigned int circle_time);
void Turn_right(unsigned int circle_time);
void Circle(unsigned int circle_time);
void Stop_motor();

#endif