

#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"

#include "iot_gpio.h"
#include "iot_pwm.h"
#include "hi_io.h"     //==hi_io_set_func()、hi_io_set_pull()
#include "hi_adc.h"    //==hi_adc_read()
#include "hi_i2c.h"

#include "oled_ssd1306.h"


/**
 //oled测试

static float ConvertToVoltage(unsigned short data)
{
    return (float)data * 1.8 * 4 / 4096;
}


static void OledTask(void *arg)
{
    (void)arg;

    //IoTGpioInit(I2C1_SDA_IO13);
    // IoTGpioInit(I2C1_SCL_IO14);

    OledInit();

    OledFillScreen(0x00);
    OledShowString(25, 2, "Hello", 2); //==位置、内容、大小1、2可选
    sleep(2);
    OledFillScreen(0x00);
    OledShowString(30,2, "Kun", 2); //==位置、内容、大小1、2可选
    sleep(1);
    OledFillScreen(0x00);
    OledShowString(25, 2, "HarmonyOS", 2); //==位置、内容、大小1、2可选
    sleep(2); 
    OledFillScreen(0x00); 

    OledShowChinese(0,0,0);
    OledShowChinese(18,0,1);
    OledShowChinese(36,0,2);
    OledShowChinese(54,0,3);  //你好鸿蒙 
    OledShowChinese(63,0,4);  //
    sleep(3);
   
    // static const char digits[]= "0123456789";
    for (int i = 0; i < 3; i++) {
        OledFillScreen(0x00);
        for (int y = 0; y < 8; y++) {
            static const char text[] = "ABCDEFGHIJKLMNOP"; // QRSTUVWXYZ
            OledShowString(0, y, text, 1);
            OledShowString(0, y, "Hello,HarmonyOS", 2); //==位置、内容、大小1、2可选
        }
        sleep(1);
    }

    OledFillScreen(0x00);
    while (1) {
        static char text[128] = {0};
        unsigned short data = 0;
        //gpio5 read adc
        hi_adc_read( HI_ADC_CHANNEL_2, &data, HI_ADC_EQU_MODEL_4, HI_ADC_CUR_BAIS_DEFAULT, 0);
        float voltage = ConvertToVoltage(data);
        snprintf(text, sizeof(text), "vol: %.3f!", voltage);

        OledShowString(0, 1, text, 2);
        usleep(30*1000);
    }
}

static void OledDemo(void)
{
    osThreadAttr_t attr;

    attr.name = "OledTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096;
    attr.priority = osPriorityNormal;

    if (osThreadNew(OledTask, NULL, &attr) == NULL) {
        printf("[OledDemo] Falied to create OledTask!\n");
    }
}

APP_FEATURE_INIT(OledDemo);


*/