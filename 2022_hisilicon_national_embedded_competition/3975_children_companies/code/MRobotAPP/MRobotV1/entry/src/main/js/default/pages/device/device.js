import app from '@system.app';
import router from '@system.router';
import {getApp} from '../../common.js';

export default {
    data: {
        deviceName: '',
        deviceImg: '',
//        productName: 'FAN',
        productName:'MRobot',
        sessionId: ''
    },
//    JS根据productName显示对应的设备图标
    onInit() {
        this.deviceName = this.$t('strings.device-name');
        this.deviceImg = '/common/img/' + this.productName + ".png";
        getApp(this).Product.productName = this.productName;
        getApp(this).Product.productId = this.productId;
        getApp(this).ConfigParams.sessionId = this.sessionId;
    },
    cancel() {
        app.terminate();
    },
    configNet() {
        router.push({
          uri: 'pages/netconfig/netconfig'

        });
    },
    //去云端控制界面
    web_mqtt()
    {
        router.push({
            uri: 'pages/web_mqtt/web_mqtt'
        })
    }
}