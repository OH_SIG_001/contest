#include <stdio.h>

#include "stm32f10x_gpio.h"
#include "delay.h"
#include "sys.h"

#include "usart.h"
#include "ESP.h"

//以下变量用于处理ESP32
u8 num;
char str[100];

//以下变量用于处理海思
//储存20个手势
char str_hand[20];
//接收到的手势数量
u8   num_flag = 0;
//暂时存储数据的数组
char data[2];
//八种手势的数量
char num_hand[9];

//ESP联网标志
char ESP_flag;

void ESP32_Reset_Config(void)
{
      GPIO_InitTypeDef GPIO_InitStructure;
        
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13|GPIO_Pin_14;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
        
        GPIO_ResetBits(GPIOC, GPIO_Pin_13);
}

int main(void)
{
    SystemInit();
    delay_init();
    uart1_init(115200);
    uart2_init(115200);
        uart3_init(115200);
            
        ESP32_Reset_Config();
        //复位ESP
        GPIO_ResetBits(GPIOC, GPIO_Pin_14);
        delay_ms(200);
        GPIO_SetBits(GPIOC, GPIO_Pin_14);
        
    ESPInit();

        ESPConnect();
        
        while(1);

}


//串口中断处理
void USART1_IRQHandler(void)
{
    USART_ClearFlag(USART1,USART_FLAG_TC);
    if(USART_GetITStatus(USART1,USART_IT_RXNE)!=Bit_RESET)//检查指定的USART中断发生与否
    {
        u8  k=USART_ReceiveData(USART1);
        uart1_send_char(k);
    }
}

void USART2_IRQHandler(void)
{
    USART_ClearFlag(USART2,USART_FLAG_TC);
    if(USART_GetITStatus(USART2,USART_IT_RXNE)!=Bit_RESET)//检查指定的USART中断发生与否
    {
        u8  k=USART_ReceiveData(USART2);
        uart1_send_char(k);
                
                str[num] = k;
        num++;
    }
}

void USART3_IRQHandler(void)
{
    USART_ClearFlag(USART3,USART_FLAG_TC);
    if(USART_GetITStatus(USART3,USART_IT_RXNE)!=Bit_RESET)//检查指定的USART中断发生与否
    {
        u8  k=USART_ReceiveData(USART3);

                //更新存储数组
                data[0] = data[1];
                data[1] = k;
                
                //判断停止位是否到来
                if(k == 0xFF)
                {
                        str_hand[num_flag] = data[0];
                        num_flag++;
                }
                
                //判断是否收集够了20个手势
                if(num_flag == 20)
                {
                        //统计收集的手势数量
                        for(int m=0; m<9; m++)
                        {
                                for(int n=0; n<20; n++)
                                {
                                        if(str_hand[n] == m)
                                        {
                                                num_hand[m]++;
                                        }
                                }
                                
                        }
                        
                        //得出数量最多的手势
                        u8 max_hand = num_hand[0];
                        u8 max_hand_label = 0;
                        
                        for(int m=1; m<9; m++)
                        {
                                if(max_hand < num_hand[m])
                                {
                                        max_hand = num_hand[m];
                                        max_hand_label = m;
                                }
                        }
                        
                        //发送数量最多的手势
                        uart1_send_char(max_hand_label);
                        
                        uart2_send_buff("AT+CIPSEND=1\r\n",14);
                        delay_ms(200);
                        uart2_send_char(max_hand_label+47);
                        
                        num_flag = 0;
                }
                
        
    }
}
