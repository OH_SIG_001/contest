
# 将数据集中的视频 逐帧提取保存

import os
import cv2
import time

# 数据集路径
path_video = 'D:\Works\Hisi_codes\SLRDataset_isolate\color'     # 原视频目录
path_image = 'D:\Works\Hisi_codes\SLRDataset_isolate\color_img' # 保存目录
# # 比赛官方服务器中数据集存放的路径
# path_video = '/home/hs18352379891/datasets/SLR_Dataset_videos'     # 原视频目录
# path_image = '/home/hs18352379891/datasets/SLR_Dataset_images' # 保存目录



dir_list = os.listdir(path_video) # 目录内的子目录列表
dir_list = sorted(dir_list)

print(dir_list)
print('dir nums = ', len(dir_list))
t1 = time.time()
for i in range(len(dir_list)):
    dir = dir_list[i] # 文件夹名
    dir_path = os.path.join(path_video, dir) # 文件夹完整路径
    if not os.path.isdir(dir_path): # 创建文件夹
        os.makedirs(dir_path)
    videos_list = os.listdir(dir_path) # 读取视频文件列表
    videos_list = sorted(videos_list)
    # print(len(videos_list))
    # print(videos_list[5])    
    print("\ndir: ", dir, "\n")

    # if i==1:
    #     break

    for j in range(len(videos_list)): # 遍历当前文件夹内视频
        if i==0 and j<=237:
            continue
        video_name = videos_list[j] # 读取的视频的文件名
        file_path = os.path.join(dir_path, videos_list[j]) # 读取视频的完整路径
        cap = cv2.VideoCapture(file_path)
        
        if (j+1)%10==0:
            print("\rvideo_name: ", video_name)

        # 创建读取视频的同名文件夹
        sub_dir = os.path.join(path_image, dir, video_name[0:-11]) # 去掉后缀名.mp4 和 名称中的 ._color
        os.makedirs(sub_dir)

        count = 0
        while True: # 逐帧抽取保存
            flag, frame = cap.read()
            if not flag:
                break
            # save_path = os.path.join(path_image, dir, video_name[0:-4]+"_%d.jpg"%count)
            # save_path = os.path.join(sub_dir, "%d.jpg"%count)
            save_path = os.path.join(sub_dir, "{:03d}.jpg".format(count))
            cv2.imwrite(save_path, frame)
            count = count + 1
        cap.release()


t2 = time.time()
print("time: ", t2-t1)



