
ResNet50 + LSTM + FC

res50lstm_epoch026.pth	训练的原始权重
res50lstm_epoch026-res.pth	原始权重分类出的Res50部分网络的权重
res50lstm_epoch026-lstm.pth	原始权重分类出的lstm部分网络的权重
res50lstm_epoch026-fc.pth	原始权重分类出的全连接FC部分网络的权重


lstm_with_linner.pth		将 res50lstm_epoch026-lstm.pth 转换为自己用pytorch搭建的lstm