﻿#include "sys.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <ohos_init.h>
#include <cmsis_os2.h>
#include <iot_i2c.h>
#include <iot_gpio.h>
#include <iot_errno.h>

#include "max30102_func.h"
#include "orientation.h"
#include "task_start.h"

#define Vl6_INTERVAL_TIME_US 300000
#define Vl6_TASK_STACK_SIZE 4096
#define Vl6_TASK_PRIO 25
#define Vl6_TEST_GPIO 9 // for hispark_pegasus

u8 ex_Range = 0;
static void *MutiTask(const char *arg)
{
    (void)arg;
    int i=0;
    GPS_init();
    printf("\r\nOrientation Initialization Succeed!\r\n");
    
    Max30102Init();
    printf("\r\nMax30102 Initialization Succeed!\r\n");

	if(VL6180X_Init() == 0)	printf("\r\nVL6180X  Initialization Succeed!\r\n");
    
	delay_us(2000000);
	/*LOOP*/
    Dete_signalRang();
	while(1)
	{
		ex_Range = VL6180X_Read_Range();
        printf("\r\n Current Range:%d mm\r\n",ex_Range);
        hr_bo_measure();
        GPS_Read();
		parseGpsBuffer();
		printGpsBuffer();
		delay_us(100000);//100ms
        /*
        i++;
       if(i==10)
        {
            Max30102Init();
            Dete_signalRang();
            i=0;
        }
        */

    }
    return NULL;
}

static void TaskEntry(void)
{
    osThreadAttr_t attr;
   
    attr.name = "MutiTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = Vl6_TASK_STACK_SIZE;
    attr.priority = Vl6_TASK_PRIO;

    if (osThreadNew((osThreadFunc_t)MutiTask, NULL, &attr) == NULL) {
        printf("[Task] Falied to create Vl6Task!\n");
    }
}

SYS_RUN(TaskEntry);