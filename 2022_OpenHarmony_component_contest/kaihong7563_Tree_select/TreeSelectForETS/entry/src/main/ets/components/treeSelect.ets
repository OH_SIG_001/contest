import { itemProp, itemChildrenProp } from '../utils/consts'

@Component
export default struct TreeSelect {
  private multiple: boolean = false // 是否支持多选
  private items: itemProp [] // 分类显示所需的数据
  @State itemChildren: itemChildrenProp[] = [] // 左侧选择项对应导航的数据
  @Link mainActiveIndex: number // 左侧选中项的索引
  @Link activeId: string [] // 右侧选中项的 id，数组格式
  private itemClick: () => void // 点击右侧选择项时触发函数

  aboutToAppear() {
    this.itemChildren = this.items[this.mainActiveIndex].children
  }

  /**
   * 点击左侧导航时触发
   * @param index - 被点击的导航的索引
   */
  private navClick = (index: number) => {
    console.log(index.toString())
    this.itemChildren = this.items[index].children
    this.mainActiveIndex = index
  }

  /**
   * 判断该选择项是否已经被选中
   * @param id - 被点击的选择项的ID
   */
  hasActiveId(id: string) {
    let index = this.activeId.indexOf(id);
    return index != -1
  }

  build() {
    Row() {
      Flex({ direction: FlexDirection.Column }) {

        // 组件左侧导航
        ForEach(this.items, (item: itemProp, index: number) => {
          Flex({ alignItems: ItemAlign.Center }) {
            Text() {
            }.height('100%').width(6).backgroundColor(index === this.mainActiveIndex ? '#F10A24' : '#eee')

            Text(item.text).fontSize(25).margin({ left: 10 })
            Text(item.info)
              .visibility(item.info ? Visibility.Visible : Visibility.None)
              .backgroundColor('#F10A24')
              .fontSize(14)
              .fontColor('#fff')
              .width(18)
              .height(18)
              .borderRadius(10)
              .textAlign(TextAlign.Center)
            Text()
              .visibility(item.dot ? Visibility.Visible : Visibility.None)
              .backgroundColor('#F10A24')
              .fontSize(21)
              .fontColor('#fff')
              .width(10)
              .height(10)
              .borderRadius(5)
              .textAlign(TextAlign.Center)
          }
          .height(50)
          .width('100%')
          .backgroundColor(index === this.mainActiveIndex ? "#fff" : '')
          .onClick(this.navClick.bind(this, index))
        }, (item: itemProp) => item.text)
      }
      .width('30%')

      // 组件右侧选择项
      Flex({ direction: FlexDirection.Column }) {
        ForEach(this.itemChildren, (item: itemChildrenProp) => {
          Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.SpaceBetween }) {
            Text(item.text).fontSize(22).margin({ left: 10 }).flexGrow(1).fontWeight(FontWeight.Bold)
            Image($r('app.media.check'))
              .height(50)
              .width(50)
              .visibility(this.hasActiveId(item.id) ? Visibility.Visible : Visibility.None)
          }
          .height(50)
          .width('100%')
          .onClick(() => {
            if (this.multiple) {
              if (this.hasActiveId(item.id)) {
                let index = this.activeId.indexOf(item.id);
                this.activeId.splice(index, 1);
              } else {
                this.activeId.push(item.id)
              }
            } else {
              this.activeId.splice(0, 1, item.id)
            }
            if (this.itemClick) {
              this.itemClick()
            }
          })
        }, (item: itemChildrenProp) => item.id)
      }
      .width('70%')
      .height('100%')
      .backgroundColor('#fff')
    }
    .backgroundColor('#eee')
    .height(200)
  }
}