import prompt from '@system.prompt';
import { items, dotItems } from '../../common/utils/consts.js';


export default {
    data: {
        // 支持单选关联的数据
        single: {
            items, // item为分类显示所需的数据
            mainActiveIndex: 0, // main-active-index表示左侧高亮选项的索引
            activeId: 1 // active-id表示右侧高亮选项的 id
        },

        // 支持单选关联的数据
        multiple: {
            items, // item为分类显示所需的数据
            mainActiveIndex: 1, // main-active-index表示左侧高亮选项的索引
            activeId: [4, 5] // active-id为数组格式时，可以选中多个右侧选项
        },

        // 支持提示信息，设置dot属性后，会在图标右上角展示一个小红点。设置info属性后，会在图标右上角展示相应的徽标
        dotItems
    },
    onInit() {

    },

    /**
     * 点击左侧导航时触发
     * @param index - 被点击的导航的索引
     */
    changeNav1(index) {
        this.single.mainActiveIndex = index.detail;
        console.log(JSON.stringify(index.detail));
    },

    /**
     * 点击左侧导航时触发
     * @param index - 被点击的导航的索引
     */
    changeNav2(index) {
        this.multiple.mainActiveIndex = index.detail;
        console.log(JSON.stringify(index.detail));
    },


    /**
     * 点击右侧选择项时触发
     * @param index - 该点击项的数据
     */
    getChecked1(data) {
        this.single.activeId = data.detail;
        prompt.showToast({
            message: JSON.stringify(data.detail)
        });
        console.log(JSON.stringify(data.detail));
    },

    /**
     * 点击右侧选择项时触发
     * @param index - 该点击项的数据
     */
    getChecked2(data) {
        this.multiple.activeId = data.detail;
        prompt.showToast({
            message: JSON.stringify(data.detail)
        });
        console.log(JSON.stringify(data.detail));
    }
}
