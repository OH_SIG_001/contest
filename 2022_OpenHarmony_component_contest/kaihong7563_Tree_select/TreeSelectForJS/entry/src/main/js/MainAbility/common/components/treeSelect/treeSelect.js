export default {
    props: {
        // 分类显示所需的数据
        items: {
            default: []
        },

        // 左侧选中项的索引
        mainActiveIndex: {
            default: 0
        },

        //右侧选中项的 id，支持传入数组
        activeId: {
            default: ""
        }
    },
    data() {
        let itemChildren = this.items[this.mainActiveIndex].children || [];
        let type = typeof this.activeId;
        return {
            itemChildren: itemChildren,
            type: type
        };
    },

    /**
     * 点击左侧导航时触发
     * @param index - 被点击的导航的索引
     */
    clickNav(index) {
        this.mainActiveIndex = index;
        this.itemChildren = this.items[index].children || [];
        this.$emit("clickNav", index);
    },

    /**
     * 点击右侧选择项时触发
     * @param item - 点击的选择项
     * @param checked - 点击的选择项被选中的状态
     */
    clickItem(item, checked) {
        if (this.type === "object") {
            if (checked) {
                let index = this.activeId.indexOf(item.id);
                this.activeId.splice(index, 1);
            } else {
                this.activeId.push(item.id);
            }
        } else {
            this.activeId = item.id;
        }
        this.$emit("clickItem", this.activeId);
    },

    /**
     * 判断该选择项是否已经被选中
     * @param id - 被点击的选择项的ID
     */
    hasActiveId(id) {
        if (this.type === "object") {
            let index = this.activeId.indexOf(id);
            if (index != -1) {
                return true;
            }
        } else {
            return this.activeId == id;
        }
        return false;
    }
}